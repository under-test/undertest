# Running UnderTest in .NET Framework

Some projects still require .NET Framework to run.

To enable projects running on .NET Framework, UnderTest supports `netstandard2.0`. 

UnderTest.Templates defaults to using .NET Core.  If you look at a `csproj` created using the template you will see:

```xml
  <PropertyGroup>
    <OutputType>Exe</OutputType>
    <TargetFramework>netcoreapp2.2</TargetFramework>
    <NoWarn>CS1998;CS0169</NoWarn>
  </PropertyGroup>
```

to update your project to run on .NET Framework update the TargetFramework to `net472`

```xml
  <PropertyGroup>
    <OutputType>Exe</OutputType>
    <TargetFramework>net472</TargetFramework>
    <NoWarn>CS1998;CS0169</NoWarn>
  </PropertyGroup>
```

Then recompile and enjoy! 
