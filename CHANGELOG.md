# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased
### added
- adds support for `Inconclusive` on `FeatureHandlers` #341
- adds alias for `PropertyInject` of `Inject` #340
- adds a method `Noop` to `FeatureHandler` #349
- adds an exception `UnknownGherkinValueException` for unknown gherkin values #336
- adds `ExpectedException` handling #259 (see UnderTest.Examples.ExpectedExceptions)
- adds `ScenarioAttribute` execution #351 (see UnderTest.Examples.ScenarioExecution)
- adds support for behaviors to Before/After event hooks #343
- adds support for tag inheritance on Features/Scenarios and Example tables #357
- adds output of scenario descriptions in the log output #358
### changed
- update handling of tags on multiple Example tables, see UnderTest.Examples.MultipleExampleTableTagHandling #348
### breaking
- renamed `LogExecution` behavior to `LogStepExecution` #343
- renamed `LogStepExecutionTime` behavior to `LogExecution` #343
- renamed `StepBehaviorAttribute` to `BehaviorAttribute` #343

## 0.8.0 - 2020-10-04
### added
- output log is written to a text file in the report folder #260
- adds `GherkinDataTableStepParamAttribute` to manage GherkinDataTable parameters #262
- adds property injection on our container #272
- adds `IDisposable` to TestRunSettings #273
- adds `ProjectDescription` to `TestProjectDetails` #278
- adds commandline parameters to set `ReportOutputFolder` and `ReportOutputFilename` #281 
- adds `Message` to InconclusiveAttribute #280
- adds `CollectionStepParam` that returns a collection of given type #284
- adds an example using UnderTest.Strategies.Selenium #289
- adds a BehaviorResults to ScenarioStepResult #292
- adds HasPublicTestSetting #296
- adds FeatureToggle in the box #305
- adds a `ScenarioTimeout` to `ExecutionSettings` #313
- adds parameter support for `Guid` #316
- adds an `ExecutionMode` to `TestRunSettings` #315
- adds an initial Config Story (see: UnderTest.Examples.Configuration) #307
- adds `BaseTypeRequiredAttribute` to `HandlesFeatureAttribute` #321
- adds improved error message when step bindings do not match step method calls #320
- adds global event hook setup.  See `UnderTest.Examples.TestRunEventHandlers` #323
- adds improved `enum` binding for step methods #216
- adds step behaviors #328
- adds summary of features+scenarios which have failures/inconclusive are repeated after the run #290
- adds the ability to repeat inconclusive and failures post run #290
### changed
- improve console output for GherkinDataTable parameters #261
- correct issue where `TriggerBackgroundAfterEvent` was calling `BeforeBackgroundKeywordGrouping` #268
- improve error message when FeatureHandler is missing #269
- improve default value for `ReportOutputFolder` #276
- update fields on `TestRunScreenshot` #277
- update the format of GherkinDataTable and DocStrings in the output log to look more like input #282
- Step("step text") will now match all of Given/When/Then scenario steps #285
- FeatureHandlerServices Executes calling structure added #286
- corrected issue where features files with upper case letters in extensions were ignored #293
- fix issue with `RunOnlyTheseTags` when the tags only on scenarios #301 
- corrected message when scenarios are filtered #309 
### breaking
- removed SetOutputSettings in favor of ConfigureOutputSettings #281
- FeatureHandler instances are now scoped to the scenario vs run #215
- Because of the change from `UnderTestSettings` to `IUnderTestSettings` means that binaries are not compatible pre/post UT 0.8.0-rc023

## 0.7.0 - 2020-01-08
### added
- improved handling of invalid gherkin #229
- add support for `@future-version` on example tables #228
- add step binding logic to match on value-replace steps in scenario outlines #222
- included links to examples in the README #233
- add `ILogger` to our DI container #239
- add access to the Header from within a GherkinDataTable #130 
- add logging when passing DocString, DataTable when calling steps #243
- culture of CSV file parsing is based on the feature file's language #251
- UnderTestRunResult now has a `UniqueRunId` property #256
- add access to `TestRunSettings` within our container #247
- add additional debug/verbose logging output for run results #246
- add `SetProjectVersionFromAssembly` to `TestProjectDetails` #254 
### changed
- defaults report folder to `reports/{now}` vs `reports` #225
- adds `MeansImplicitUseAttribute` to Given, When, Then and FeatureHandler attributes #227
- updates handling of invalid feature files when `SetFailRunOnFirstError` #231
- fixed an issue where string regex matched parameters were being forced to lowercase #235
- corrected an issue where in some cases failures in BeforeFeature would pass #238
- features where all scenario are skipped will no longer be counted as passed #237
- renamed `UnderTextExitCodes` to `UnderTestExitCodes` - #244
- tags on `BeforeAfterScenarioContext` are now the combined from feature/scenario #241
- filtering by tags is now case sensitivity by default #249
- corrected exception flow in `BeforeScenario` failure cases #252
- the default scope for the container to Scoped #298

## 0.6.0 - 2019-10-10
### added
- add support for `IDisposable` feature handlers #207
- add support for ExecutionSetting for FailRunOnFirstError #208
### changed
- improve the error message when `HandlesFeatureAttribute` files are not found
- improve the duration display at the end of a run #211
- no longer warns about abstract FeatureHandlers without HandlesFeatureAttribute #210
- corrected issue where step methods for other keywords would match #214
- corrected issue where scenarios with failures in BeforeScenario would be reported as passed #209
- improve the error message when `GherkinDataTable` is a partial column match #213
- add log output when cli based tag filters #217
- updated `wip` features/scenario still pass #220
- improved cases for only run these tests filter #218
### upgrade path
- rename any calls to `ConfigureExecutionSettings` to `WithExecutionSettings` 

## 0.5.0 - 2019-09-02
### added
- add a Summary Report to give scenario result summary #191
- add `FeatureDescription` to the output on `TestRunFeatureResult` #196
- add step parameters to ScenarioStepResult in the output json #197
- add the ability to configure built in tag names #90
- add a pre-flight check for invalid file paths #179
- add an interface `IFeatureHandler` for `FeatureHandler` #201
- add new methods to filter which test are executed `OnlyRunTheseTags` and `DoNotRunTheseTags` #169
- add the ability to parse command line args to configure test runs #187
### changed
- improved the exception output when multiple steps match #199
### upgrade path
- all UnderTest `Program.cs` Main methods should be updated with `.WithCommandLineArgs(args)` is as seen in [our README.md](./README.md)
- update any calls to `AddTestFilter` in `Program.cs` to use `OnlyRunTheseTags` and `DoNotRunTheseTags`

## 0.4.1 - 2019-08-23
### added
- added a custom exception output for when a TestFilter throws an exception #194
### changed
- corrected an issue with @ignore and @wip on scenarios #192

## 0.4.0 - 2019-08-18 
### added
- build notifications are now announced in Slack #152
- add support for IncompleteScenarioException #155
- build notifications are now announced in Slack #152
- support for IncompleteScenarioException #155
- support to set the `ILogger` instance #162
- support to set the order features are executed #146
- adds support for exit codes from UnderTestRunResult #151
- adds support to example tables for external data sources #10
- adds initial support for StepParams #78
- adds initial support for hooks #150 
### changed
- marked `IGlobalHandler` as obsolete. #154
- improved console output #168 #166
- *BREAKING* - renamed `incomplete` to `wip` #171
- improves support for step parameters #180
- after events are guaranteed to be called for `BeforeAfterKeywordGroupingContext` #181
### upgrade path
- update any features with tags with `incomplete` to `wip`
- update any usage of `IncompleteScenarioException` to `WorkInProgressScenarioException` 
- Add support for exit codes from UnderTestRunResult #151 Steps:
    1. update `Program.cs` to have the `Main` method return an int
    1. update the call to `return` and call `.ToExitCode()`. This should look something like this (depending on your customization)     
    ```
    return new UnderTestRunner()
        .WithProjectDetails(x => x
          .SetProjectName("YOUR PROJECT NAME Test Suite"))
        .WithTestSettings(x => x
          .AddAssembly(typeof(Program).Assembly))
        .Execute()
          .ToExitCode();
    ``` 
1. update any code using `IGlobalHandler` to not.  We see global handlers as an anti-pattern.  Our recommended approach is to move your shared steps into base feature handlers that you inherit from.

## 0.3.4 - 2019-08-01 
### changed
- corrected issue when `but` keyword is used #172

## 0.3.3 - 2019-07-31 
### changed
- corrected issue where steps are being partially matched #170

## 0.3.2 - 2019-07-16 
### changed
- removed duplicate call to `BeforeScenarioKeywordGrouping` added call to `AfterScenarioKeywordGrouping` #160

## 0.3.1 - 2019-07-15
### changed
- corrected issue where abstract FeatureHandlers were attempted to be created #156
- corrected issue where FeatureHandlers declared with open generic types throw an exception #157

## 0.3.0 - 2019-07-10
### added
- enabled StepAttribute to be used on all step types #112
- added support for GherkinDataTable.GetInstance and GherkinDataTable.GetList #118
- added support for `DebugRunThis` and `DebugIgnoreThis` attributes which change which features are executed #120
- improved the visual separation of features in the console output #127
- improved error of duplicate `HandlesFeature` attributes.  #135
- added support for `BeforeBackgroundKeywordGrouping` / `AfterBackgroundKeywordGrouping` and `BeforeScenarioKeywordGrouping` / `AfterScenarioKeywordGrouping` #134
- added `InconclusiveAttribute` #133
- added support for SimpleInjector in our pipeline #100
- added support for async step methods #138
- added support for nullable types in gherkin DataTables #139
- added support for two column gherkin data tables #129
- added support for ignored and incomplete scenarios #144
### changed
- GherkinDataTable now has a `Header` property #117
- improved error message when #119 is hit - scenario with example table #126
- [breaking] Move `BasicInvokeBinder` to `UnderTest.Infrastructure.Binding.UnderTestInvokeBinder`. See #118
- corrected issue `AfterScenario` exceptions were not handled #136
- failures in `AfterFeature` will now result in features failing #137

## 0.2.2 - 2019-06-17
### added
- improved exception output handling for exceptions with InnerExceptions #124
### changed
- corrected issue where handlers with inherited methods would not be located #123
- corrected issue where examples tables still processed if not a Scenario Outline #119

## 0.2.1 - 2019-06-11
### added
### changed
- corrected NRE When Using Step Definitions With Parameters and no example tables #114
- corrected behavior when no `TestFilters` are active #113

## 0.2.0 - 2019-06-10
### added
- `UnderTestRunner` which is our test run wrapper
- `Strategy` support to enable multiple test types
- report capture capabilities
- `FeatureHandler` support
- new ExampleTestSuite project to display new approaches
- Newtonsoft.Json as a nuspec dependency
- Colorful.Console as a nuspec dependency
- improved output of console context #55
- improve Serilog-based logging setup #73
- add support for Scenario Outlines #72
- add support for test filters #80
- add support for DataTables #79
- add support for DataTables #84
### changed
- removed the dependency SpecFlow, Pickles and Lamar
- general project approach update
- project is now a .net standard library

## 0.1.3 - 2019-02-06
### added
- adds NUnit via nuspec file

## 0.1.2 - 2019-02-06
### added
- adds JetBrains.Annotations via nuspec file

## 0.1.1 - 2019-01-23
### added
- lamar now pulls via the nuspec file

## 0.1.0 - 2019-01-22
### added
- initial version
