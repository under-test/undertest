using FluentAssertions;
using Serilog;
using UnderTest.Attributes;

namespace UnderTest.ExampleTestSuite.FeatureHandlers
{
  [HandlesFeature("SimpleFeatureWithDIHandler.feature")]
  public class WithDependencyInjectionFeatureHandler : FeatureHandler
  {
    [Inject] public ISomething Something { get; set; }

    [Given("Something exists")]
    public void SomethingExists()
    {
      Log.Information("    Something exists");
      Something.Should().NotBeNull();
    }

    [When("Something happens")]
    public void SomethingHappens() => Noop();

    [Then("This should now be true")]
    public void ThisExists() => Noop();

    public interface ISomething
    { }

    public class SomethingImplementation : ISomething
    {
      public SomethingImplementation(ILogger logger)
      {
        logger.Information("DI injected this logger");
      }
    }
  }
}
