using System;
using UnderTest.Attributes;

namespace UnderTest.ExampleTestSuite.FeatureHandlers
{
  [HandlesFeature("Outlines/ScenarioOutlineMatchingBasedOnValue.feature")]
  public class ScenarioOutlineMatchingBasedOnValueFeatureHandler: FeatureHandler
  {
    [Given(@"the applicant (has|has not) completed a (Medicare|Social Security) application")]
    public void GivenTheApplicantHasCompletedAApplication([HasHasNotToBooleanStepParam]bool isCompletedP, string applicationTypeP)
    {
      Console.WriteLine($"{isCompletedP}::{applicationTypeP}");
    }

    public class HasHasNotToBooleanStepParam : StepParamAttribute
    {
      public override object Transform(object input)
      {
        if (input == null)
        {
          return false;
        }

        return "has" == input.ToString();
      }
    }
  }
}
