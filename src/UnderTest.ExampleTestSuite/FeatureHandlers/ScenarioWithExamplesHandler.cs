using System.Diagnostics.CodeAnalysis;
using JetBrains.Annotations;
using UnderTest.Attributes;

namespace UnderTest.ExampleTestSuite.FeatureHandlers
{
  [HandlesFeature("Outlines/ScenarioWithExamples.feature")]
  public class ScenarioWithExamplesHandler : FeatureHandler
  {
    [Given("the number <number> from this step")]
    public void TheNumberXFromThisStep()
    {
      Log.Information("    the number <number> from this step");
    }

    [When("the number <number-to-add> is added")]
    public void TheNumberXIsAdded()
    {
      Log.Information("    the number <number-to-add> is added");
    }

    [Then("the result should be <result>")]
    public void TheResultShouldBeResult()
    {
      Log.Information("    the result should be <result>");
    }
  }
}
