using System;
using System.Diagnostics.CodeAnalysis;
using JetBrains.Annotations;
using UnderTest.Attributes;

namespace UnderTest.ExampleTestSuite.FeatureHandlers
{
  [HandlesFeature("Background.feature")]
  public class BackgroundFeatureHandler : FeatureHandler, IDisposable
  {
    [Given("something happens in the background")]
    public void BackgroundSomething()
    {
      Log.Information($"    something happens in the background");
    }

    [Given("Something exists")]
    public void SomethingExists()
    {
      Log.Information($"    Something exists.");
    }

    [When("Something happens")]
    public void SomethingHappens()
    {
      Log.Information("    something happens");
    }

    [Then("This should now be true")]
    public void ThisExists()
    {
      Log.Information("    this is true");
    }

    [Then("not this")]
    public void NotThis()
    {
      Log.Information("  not this");
    }

    public void Dispose()
    { }
  }
}
