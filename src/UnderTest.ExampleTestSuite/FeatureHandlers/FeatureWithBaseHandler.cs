using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using JetBrains.Annotations;
using UnderTest.Attributes;

namespace UnderTest.ExampleTestSuite.FeatureHandlers
{
  [HandlesFeature("FeatureWithBaseHandler.feature")]
  public class FeatureWithBaseHandler : BaseHandler
  {
    private int _number;

    [Given(@"the number <number> from this step")]
    public void GivenTheNumberFromThisStep(int numberP)
    {
      _number = numberP;
    }

    [When(@"the number <number-to-add> is added")]
    public void WhenTheNumberIsAdded(int numberToAdd)
    {
      _result = _number + numberToAdd;
    }
  }

  public class BaseHandler : FeatureHandler
  {
    protected int _result;

    [Then(@"the result should be <result>")]
    public void ThenTheResultShouldBe(int result)
    {
      _result.Should().Be(result);
    }
  }
}
