using System.Linq;
using FluentAssertions;
using UnderTest.Attributes;

namespace UnderTest.ExampleTestSuite.FeatureHandlers
{
  [HandlesFeature("Tagging/TagsOnFeatureAndScenario.feature")]
  public class TagsOnFeatureAndScenarioFeatureHandler : FeatureHandler
  {
    public override void BeforeScenario(BeforeAfterScenarioContext context)
    {
      base.BeforeScenario(context);

      context.Scenario.Tags.Count().Should().Be(2);
    }

    [Given("something")]
    public void Something() => Noop();
  }
}
