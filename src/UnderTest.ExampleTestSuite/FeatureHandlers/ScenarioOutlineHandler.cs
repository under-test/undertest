using System;
using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using JetBrains.Annotations;
using UnderTest.Attributes;

namespace UnderTest.ExampleTestSuite.FeatureHandlers
{
  [HandlesFeature("Outlines/ScenarioOutline.feature")]
  public class ScenarioOutlineHandler : FeatureHandler
  {
    private int number;
    private int result;

    [Given("Something exists for <name>")]
    public void GivenSomethingExistsForName(string name)
    {
      Log.Information($"    Given name {name}");
    }

    [When("Something happens")]
    public void SomethingHappens() => Noop();

    [Then("This should now be true")]
    public void ThisShouldNowBeTrue() => Noop();

    [Given(@"the number (\d+) from this step")]
    public void GivenTheNumberFromThisStep(int numberP)
    {
      number = numberP;
    }

    [When(@"the number (\d+) is added")]
    public void WhenTheNUmberIsAdded(int numberToAdd)
    {
      result = number + numberToAdd;
    }

    [Then(@"the result should be (\d+)")]
    public void ThenTheResultShouldBe(int result)
    {
      result.Should().Be(result);
    }
  }
}
