using UnderTest.Attributes;

namespace UnderTest.ExampleTestSuite.FeatureHandlers
{
  [HandlesFeature(@"Tagging/IgnoredScenario.feature")]
  public class IgnoredScenarioHandler : FeatureHandler
  { }
}
