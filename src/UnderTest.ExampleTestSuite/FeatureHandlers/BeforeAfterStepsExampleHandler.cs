using System.Diagnostics.CodeAnalysis;
using JetBrains.Annotations;
using UnderTest.Attributes;

namespace UnderTest.ExampleTestSuite.FeatureHandlers
{
  [HandlesFeature("Hooks/BeforeAfterStepsExample.feature")]
  public class BeforeAfterStepsExampleHandler : FeatureHandler
  {
    public override void BeforeStep(BeforeAfterStepContext context)
    {
      Log.Information($"In BeforeStep for `{context?.Feature?.Document?.Feature?.Name}` on step {context.Step.Keyword}{context.Step.Text}");
    }

    public override void AfterStep(BeforeAfterStepContext context)
    {
      Log.Information($"In AfterStep for `{context?.Feature?.Document?.Feature?.Name}` on step {context.Step.Keyword}{context.Step.Text}");
    }

    public override void BeforeScenarioKeywordGrouping(BeforeAfterKeywordGroupingContext context)
    {
      Log.Information($"In BeforeScenarioKeywordGrouping for `{context?.Feature?.Document?.Feature?.Name}` on step {context.KeywordGrouping.Keyword}");
    }

    public override void AfterScenarioKeywordGrouping(BeforeAfterKeywordGroupingContext context)
    {
      Log.Information($"In AfterScenarioKeywordGrouping for `{context?.Feature?.Document?.Feature?.Name}` on step {context.KeywordGrouping.Keyword}");
    }

    [Given("Something is true")]
    public void SomethingIsTrue() => Noop();

    [When("Something happens")]
    public void SomethingHappens() => Noop();

    [Then("This should now be true")]
    public void ThisExists() => Noop();
  }
}
