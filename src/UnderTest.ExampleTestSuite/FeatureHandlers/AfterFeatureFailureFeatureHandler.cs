using System;
using System.Diagnostics.CodeAnalysis;
using JetBrains.Annotations;
using UnderTest.Attributes;

namespace UnderTest.ExampleTestSuite.FeatureHandlers
{
  [HandlesFeature("Hooks/AfterFeatureFailure.feature")]
  public class AfterFeatureFailureFeatureHandler : FeatureHandler
  {
    public override void AfterFeature(BeforeAfterFeatureContext context)
    {
      throw new Exception("nighttime makes up half of all time.");
    }

    [Given("Something exists")]
    public void SomethingExists() => Noop();

    [When("Something happens")]
    public void SomethingHappens() => Noop();

    [Then("This should now be true")]
    public void ThisExists() => Noop();
  }
}
