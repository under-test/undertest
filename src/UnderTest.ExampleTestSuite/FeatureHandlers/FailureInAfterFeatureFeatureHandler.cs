using System;
using System.Diagnostics.CodeAnalysis;
using JetBrains.Annotations;
using UnderTest.Attributes;

namespace UnderTest.ExampleTestSuite.FeatureHandlers
{
  [HandlesFeature("Hooks/FailureInAfterFeature.feature")]
  public class FailureInAfterFeatureFeatureHandler : FeatureHandler
  {
    public override void AfterFeature(BeforeAfterFeatureContext context)
    {
      throw new Exception("nighttime makes up half of all time.");
    }

    [Given("Something exists")]
    public void SomethingExists() => Noop();

    [When("I do the thing")]
    public void SomethingHappens() => Noop();

    [Then("the thing happens")]
    public void ThisExists() => Noop();
  }
}
