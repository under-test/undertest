using UnderTest.Attributes;

namespace UnderTest.ExampleTestSuite.FeatureHandlers
{
  public class GenericBaseFeatureHandler<T> : FeatureHandler
  {
    protected T Value { get; set; }
  }
}
