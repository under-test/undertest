﻿using System.Diagnostics.CodeAnalysis;
using UnderTest.ExampleTestSuite.FeatureHandlers;

[assembly: ExcludeFromCodeCoverageAttribute]

namespace UnderTest.ExampleTestSuite
{
  class Program
  {
    static int Main(string[] args)
    {
      return new UnderTestRunner()
        .WithCommandLineArgs(args)
        .WithProjectDetails(x => x
          .SetProjectName("UnderTest Example Test Suite")
          .SetProjectVersionFromAssembly(typeof(UnderTestRunner).Assembly))
        .WithTestSettings(settings => settings
          .AddAssembly(typeof(Program).Assembly)
          .ConfigureContainer(x => x.Register<WithDependencyInjectionFeatureHandler.ISomething, WithDependencyInjectionFeatureHandler.SomethingImplementation>())
          .WithExecutionSettings(x => x.SetFailRunOnFirstError(false)))
        .Execute()
          .ToExitCode();
    }
  }
}
