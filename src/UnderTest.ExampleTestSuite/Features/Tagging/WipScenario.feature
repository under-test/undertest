Feature: Example showing when gherkin may be a work-in-progress but within the project

  @wip
  Scenario: Wip Scenario marked as such
    Given A scenario we don't know all of the details of yet
    When details are not complete
    Then then with UnderTest we can mark the feature as `wip`
