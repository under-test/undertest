Feature: Scenario Outline Matching Based On Value

  Scenario: Option 1
  Given the applicant has completed a Medicare application

  Scenario Outline: Option 2
    Given the applicant has <completed?> a <application-type> application

    Examples:
      | completed?    | application-type |
      | completed     | Medicare         |
      | not completed | Social Security  |

  Scenario Outline: Option 3
    Given the applicant <has?> completed a <application-type> application

    Examples:
      | has?    | application-type |
      | has     | Medicare         |
      | has not | Social Security  |

  Scenario Outline: Option 4
    Given the applicant <has-completed?> a <application-type> application

    Examples:
      | has-completed?    | application-type |
      | has completed     | Medicare         |
      | has not completed | Social Security  |

