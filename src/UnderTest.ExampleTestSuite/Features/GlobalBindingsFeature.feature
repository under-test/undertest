Feature: Global binding feature

  # note - in a future version Global Handlers will be removed.
  Scenario: Scenario where the bindings are bound globally
    Given Something bound globally
    When Something that is bound globally happens
    Then This should now be true based on a global binding
