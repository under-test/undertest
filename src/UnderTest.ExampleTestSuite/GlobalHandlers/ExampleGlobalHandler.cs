using Serilog;
using UnderTest.Attributes;

namespace UnderTest.ExampleTestSuite.GlobalHandlers
{
  public class ExampleGlobalHandler: IGlobalHandler
  {
    public ILogger Log => TestSettings.Logger;

    [PropertyInjected]
    public IUnderTestSettings TestSettings { get; set; }

    [Step("Something bound globally")]
    public void SomethingBoundGlobally()
    {
      Log.Information($"  Running globally bound `Something bound globally`");
    }

    [Step("Something that is bound globally happens")]
    public void SomethingThatIsBoundGloballyHappens()
    {
      Log.Information($"  Running globally bound `Something that is bound globally happens`");
    }

    [Step("This should now be true based on a global binding")]
    public void ThisShouldNowBeTrueBasedOnAGlobalBinding()
    {
      Log.Information($"  Running globally bound `This should now be true based on a global binding`");
    }
  }
}
