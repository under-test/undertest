using UnderTest.Attributes;

namespace UnderTest.Examples.FilterFeatures.FeatureHandlers
{
  [HandlesFeature("Example.feature")]
  public class ExampleFeatureHandler : FeatureHandler
  {
    [Given("I have logged (.*)")]
    public void Given(string amILoggedI)
    {
      // some fancy test code
    }

    [Then("I am logged in")]
    [Then("I am not logged in")]
    public void When() => Noop();
  }
}
