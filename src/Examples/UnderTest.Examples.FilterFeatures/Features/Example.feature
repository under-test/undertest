Feature: Example Feature

  This is an example feature where one scenario is marked with the tag `@RunThis`

  @RunThis
  Scenario: First Scenario
    Given I have logged in
    Then I am logged in

  Scenario: Second Scenario
    Given I have logged out
    Then I not am logged in
