using System.Diagnostics.CodeAnalysis;

[assembly: ExcludeFromCodeCoverageAttribute]

namespace UnderTest.Examples.FilterFeatures
{
  class Program
  {
    static int Main(string[] args)
    {
      return new UnderTestRunner()
        .WithCommandLineArgs(args)
        .WithProjectDetails(x => x
          .SetProjectName("Example Project showing tag filtering").
          SetProjectVersionFromAssembly(typeof(FeatureHandler).Assembly))
        .WithTestSettings(settings => settings
          .AddAssembly(typeof(Program).Assembly)
          .OnlyRunTheseTags("RunThis")
          // todo - play with these to see the different behaviors
          //.DoNotRunTheseTags("RunThis")
          //.DoNotRunTheseTags("SomethingOnNoFeatures")
          //.OnlyRunTheseTags("SomethingOnNoFeatures")
        )
        .Execute()
          .ToExitCode();
    }
  }
}
