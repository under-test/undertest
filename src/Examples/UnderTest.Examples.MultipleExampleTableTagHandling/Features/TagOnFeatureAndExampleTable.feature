﻿Feature: Selectively run based on tags and scenarios and example tables
  
  As a developer using UnderTest
  I want to limit which scenarios are run based on tags
  So that can focus my run execution

  Scenario Outline: Scenario outline with no tags 
    note: should not be run when running with `OnlyRunTheseTags("@smoke")`
    Given there is an initial value of '<initial-value>'
    When the initial value is doubled
    Then the result should be '<result-value>'

    Examples: Should not be run
      | initial-value | result-value |
      | 1             | 2            |

  @smoke
  Scenario Outline: Scenario outline with tag on the scenario outline
    note: all examples should be run when running with `OnlyRunTheseTags("@smoke")`
    Given there is an initial value of '<initial-value>'
    When the initial value is doubled
    Then the result should be '<result-value>'

    Examples: Initial and result values
      | initial-value | result-value |
      | 1             | 2            |
      | 2             | 4            |

  Scenario Outline: Scenario outline with tag on the examples table
    note: the first example table should be run when running with `OnlyRunTheseTags("@smoke")`
    Given there is an initial value of '<initial-value>'
    When the initial value is doubled
    Then the result should be '<result-value>'
    @smoke
    Examples: Initial and result values
     note: should be run when running with `OnlyRunTheseTags("@smoke")`
      | initial-value | result-value |
      | 1             | 2            |

    Examples: non-smoke tagged examples
      note: should not be run when running with `OnlyRunTheseTags("@smoke")`
      | initial-value | result-value |
      | 2             | 4            |

  Scenario: Scenario without examples or tags
    note: should not be run when running with `OnlyRunTheseTags("@smoke")`
    Given there is an initial value of '50'
    When the initial value is doubled
    Then the result should be '100'
