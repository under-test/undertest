using System;
using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using UnderTest.Reporting;

[assembly: ExcludeFromCodeCoverageAttribute]

namespace UnderTest.Examples.MultipleExampleTableTagHandling
{
  class Program
  {
    static int Main(string[] args)
    {
      var assembly = typeof(Program).Assembly;
      var results = new UnderTestRunner<TestSuiteConfiguration>()
        .WithCommandLineArgs(args)
        .WithProjectDetails(x => x
          .SetProjectName("UnderTest.Examples.MultipleExampleTableTagHandling")
          .SetProjectVersionFromAssembly(assembly))
        .WithTestSettings(settings => settings
          .AddAssembly(assembly)
          .OnlyRunTheseTags("@smoke"))
        .Execute();
    
      return HandleExpectedResultForSuccessfulSuite(results);
    }

    private static int HandleExpectedResultForSuccessfulSuite(UnderTestRunResult result)
    {
      if (result.HasAnyFailedScenarios())
        return (int)UnderTestExitCodes.Failure;

      try
      {
        // we have expected non-passing results from this test suite
        // handle that tracking here.
        result.Features.Passed.Should().Be(2);
        result.Scenarios.Passed.Should().Be(4);
        result.Scenarios.Skipped.Should().Be(6);

        return result.ToExitCode();
      }
      catch (Exception)
      {
        return (int)UnderTestExitCodes.Failure;
      }
    }
  }
}
