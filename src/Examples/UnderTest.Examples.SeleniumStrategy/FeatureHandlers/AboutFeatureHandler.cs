using FluentAssertions;
using OpenQA.Selenium;
using UnderTest.Attributes;
using UnderTest.Strategy.Selenium;

namespace UnderTest.Examples.SeleniumStrategy.FeatureHandlers
{
  [HandlesFeature("About.feature")]
  public class AboutFeatureHandler : SeleniumFeatureHandler
  {
    const string ThinkingBigAboutPageUrl = "https://thinkingbig.net/about/";

    [When("I visit the About page")]
    public void VisitTheAboutPage()
    {
      CurrentDriver.Navigate().GoToUrl(ThinkingBigAboutPageUrl);
    }

    [Then("the current page should be the About page")]
    public void TheCurrentPageIsTheAboutPage()
    {
      CurrentDriver.Url.Should().Be(ThinkingBigAboutPageUrl);
    }

    [Then("I should see the Thinking Big Executives")]
    public void ThenIShouldSeeTbExecs()
    {
      CurrentDriver.FindElements(By.ClassName("executive")).Should().HaveCountGreaterThan(0);
    }

    [Then("I should see the Thinking Big Team")]
    public void ThenIShouldSeeTbTeam()
    {
      CurrentDriver.FindElements(By.ClassName("team-member")).Should().HaveCountGreaterThan(0);
    }

    [Then("I should see the Thinking Big Contact Information")]
    public void ThenIShouldSeeTbContactInfo()
    {
      new PageElement(By.CssSelector("#contact_footer"), this).Should().NotBeNull();
    }
  }
}
