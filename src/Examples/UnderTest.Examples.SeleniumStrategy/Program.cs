using System;
using System.Diagnostics.CodeAnalysis;
using OpenQA.Selenium.Chrome;
using UnderTest.Strategy.Selenium;

[assembly: ExcludeFromCodeCoverageAttribute]

namespace UnderTest.Examples.SeleniumStrategy
{
  class Program
  {
    static int Main(string[] args)
    {
      return new UnderTestRunner()
        .WithCommandLineArgs(args)
        .WithProjectDetails(x => x
          .SetProjectName("UnderTest Example of testing web browsers using UnderTest.Strategies.Selenium")
          .SetProjectVersionFromAssembly(typeof(UnderTestRunner).Assembly))
        .WithTestSettings(settings => settings
          .AddAssembly(typeof(Program).Assembly)
          .AddSeleniumStrategy(strategy => strategy
                    .WithFeatureHandlersFrom(typeof(Program).Assembly)
                    .SetDriverCreationFunc(() => new ChromeDriver(AppDomain.CurrentDomain.BaseDirectory))
                    .SetDriverLifeCycle(DriverLifecycle.EntireRun)
                    .WithWebsiteComponentsFrom(typeof(Program).Assembly))
                    .AttachBehaviors()
                      .CaptureScreenshots()
                      .FinishAttaching())
        .Execute()
        .ToExitCode();
    }
  }
}
