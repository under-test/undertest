﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using FluentAssertions;
using UnderTest.Attributes;
using UnderTest.Examples.FeatureService.Services;

namespace UnderTest.Examples.FeatureService.FeatureHandlers
{
  [HandlesFeature("ExecutesOnFeatureHandler.feature")]
  public class ExecutesFeatureHandler : FeatureHandler<ExampleUnderTestService>
  {
    private string something;

    [Given("Something exists")]
    public Task SomethingExists() => ExecutesAsync(async () =>
      Console.WriteLine("Something exists"));

    [When("Something happens")]
    public Task SomethingHappens() => ExecutesAsync( async () =>
      something = "shoes");

    [Then("This should now be true")]
    public void ThisExists() => ExecutesSync(() =>
      something.Should().Be("shoes"));
  }
}
