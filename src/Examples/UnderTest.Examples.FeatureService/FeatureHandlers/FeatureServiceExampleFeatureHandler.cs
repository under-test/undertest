using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using UnderTest.Attributes;
using UnderTest.Examples.FeatureService.Services;

namespace UnderTest.Examples.FeatureService.FeatureHandlers
{
  [HandlesFeature("ExampleFeatureService.feature")]
  public class FeatureServiceExampleFeatureHandler : FeatureHandler<ExampleUnderTestService>
  {
    [Given("Something exists")]
    public Task SomethingExists() => ExecutesAsync(x => x
      .SetupTheFactThatSomethingExists()
    );

    [When("Something happens")]
    public Task SomethingHappens() => ExecutesAsync( x => x
      .ExecuteSomething());

    [Then("This should now be true")]
    public void ThisExists() => ExecutesSync(x => x
      .VerifySomethingIs());
  }
}
