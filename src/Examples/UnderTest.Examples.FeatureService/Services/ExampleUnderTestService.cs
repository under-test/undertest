using System;
using System.Net;
using System.Threading.Tasks;
using FluentAssertions;
using JetBrains.Annotations;

namespace UnderTest.Examples.FeatureService.Services
{
  [UsedImplicitly]
  public class ExampleUnderTestService: FeatureHandlerServiceBase
  {
    private int value;

    private readonly Guid id = Guid.NewGuid();

    public async Task<ExampleUnderTestService> SetupTheFactThatSomethingExists()
    {
      Console.WriteLine($"id => {id}");
      // this can:
      //   - call another library to setup some state
      //   - use your application screens to setup some state
      //   - call an API
      //   - modify a database
      //   - whatever you application needs to
      //
      // the goal here is to prep the state

      // in this case, we will hit a url
      var request = WebRequest.Create("https://undertest.dev");
      var response = (HttpWebResponse) await Task.Factory
        .FromAsync<WebResponse>(request.BeginGetResponse,
          request.EndGetResponse,
          null);

      response.StatusCode.Should().Be(HttpStatusCode.OK);

      return this;
    }

    public Task<ExampleUnderTestService> ExecuteSomething()
    {
      Console.WriteLine($"id => {id}");
      // this can:
      //  - ideally uses your application to trigger an event

      // in our example we will set our value to 2
      value = 2;

      return Task.FromResult(this);
    }

    // note: this method is not setup for async/await and called with ExecuteSync
    public ExampleUnderTestService VerifySomethingIs()
    {
      Console.WriteLine($"id => {id}");
      // this can:
      // - look up some state via app, api, etc
      // - verify that state

      // in our example, we will verify that value is 2
      value.Should().Be(2);

      return this;
    }
  }
}
