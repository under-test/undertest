using System.Diagnostics.CodeAnalysis;
using FluentAssertions;

[assembly: ExcludeFromCodeCoverageAttribute]

namespace UnderTest.Examples.ExpectedExceptions
{
  class Program
  {
    static int Main(string[] args)
    {
      var asm = typeof(Program).Assembly;
      var result = new UnderTestRunner()
        .WithCommandLineArgs(args)
        .WithProjectDetails(x => x
          .SetProjectName(asm.GetName().Name)
          .SetProjectVersionFromAssembly(asm))
        .WithTestSettings(settings => settings
          .AddAssembly(asm))
        .Execute();
        
      // verify expected exceptions
      const int expectedFailureCount = 9;
      result.Features.ExpectedFailure.Should().Be(expectedFailureCount);
      result.Scenarios.Failed.Should().Be(expectedFailureCount);

      return result.ToExitCode() == (int)UnderTestExitCodes.Failure ? 0 : 1;
    }
  }
}
