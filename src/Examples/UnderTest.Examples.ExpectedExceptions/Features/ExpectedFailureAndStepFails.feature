Feature: Feature that is expected to fail and a step fails

  Scenario: Simple scenario
    Given something exists
    When something happens
    Then this should now be true
