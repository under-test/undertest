Feature: Feature that is expected to fail in the AfterScenario

  Scenario: Simple scenario
    Given something exists
    When something happens
    Then this should now be true
