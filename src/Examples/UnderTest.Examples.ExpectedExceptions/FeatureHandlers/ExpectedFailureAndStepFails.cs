using System;
using UnderTest.Attributes;

namespace UnderTest.Examples.ExpectedExceptions.FeatureHandlers
{
  [ExpectedFailure]
  [HandlesFeature("ExpectedFailureAndStepFails.feature")]
  public class ExpectedFailureAndStepFailsFeatureHandler : FeatureHandler
  {
    [Given("Something exists")]
    public void SomethingExists() => throw new Exception("Step failed");

    [When("Something happens")]
    public void SomethingHappens() => Noop();

    [Then("This should now be true")]
    public void ThisExists() => Noop();
  }
}
