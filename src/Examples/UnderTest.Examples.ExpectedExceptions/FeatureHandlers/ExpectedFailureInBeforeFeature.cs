using System;
using UnderTest.Attributes;

namespace UnderTest.Examples.ExpectedExceptions.FeatureHandlers
{
  [ExpectedFailure]
  [HandlesFeature("ExpectedFailureInBeforeFeature.feature")]
  public class ExpectedFailureInBeforeFeature : FeatureHandler
  {
    public override void BeforeFeature(BeforeAfterFeatureContext context) => throw new Exception("before feature failed");

    [Given("Something exists")]
    public void SomethingExists() => Noop();

    [When("Something happens")]
    public void SomethingHappens() => Noop();

    [Then("This should now be true")]
    public void ThisExists() => Noop();
  }
}
