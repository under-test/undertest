﻿using System;
using UnderTest.Attributes;

namespace UnderTest.Examples.ExpectedExceptions.FeatureHandlers
{
  [ExpectedFailure]
  [HandlesFeature("ExpectedFailureInAfterStep.feature")]
  public class ExpectedFailureInAfterStep : FeatureHandler
  {
    public override void AfterStep(BeforeAfterStepContext context) => throw new Exception("after step failed");

    [Given("Something exists")]
    public void SomethingExists() => Noop();

    [When("Something happens")]
    public void SomethingHappens() => Noop();

    [Then("This should now be true")]
    public void ThisExists() => Noop();
  }
}
