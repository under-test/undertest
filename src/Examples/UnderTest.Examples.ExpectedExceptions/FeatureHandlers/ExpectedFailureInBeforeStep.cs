using System;
using UnderTest.Attributes;

namespace UnderTest.Examples.ExpectedExceptions.FeatureHandlers
{
  [ExpectedFailure]
  [HandlesFeature("ExpectedFailureInBeforeStep.feature")]
  public class ExpectedFailureInBeforeStepFeatureHandler : FeatureHandler
  {
    public override void BeforeStep(BeforeAfterStepContext context) => throw new Exception("before step failed");

    [Given("Something exists")]
    public void SomethingExists() => Noop();

    [When("Something happens")]
    public void SomethingHappens() => Noop();

    [Then("This should now be true")]
    public void ThisExists() => Noop();
  }
}
