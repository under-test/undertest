using System.Diagnostics.CodeAnalysis;
using UnderTest;
using UnderTest.Configuration;

[assembly: ExcludeFromCodeCoverageAttribute]

namespace UnderTest.Examples.ConfigureOutputSettings
{
  class Program
  {
    /// <summary>
    /// note: in addition to configuring the values below command line parameters can be passed to set these values
    /// see <see cref="UnderTestCommandLineOptions"/> for more information.
    /// </summary>
    static int Main(string[] args)
    {
      return new UnderTestRunner()
        .WithCommandLineArgs(args)
        .WithProjectDetails(x => x
          .SetProjectName("Example Configuring Output Settings")
          .SetProjectVersion("0.1.0")
          .SetProjectDescription("This example customizes the project's output settings."))
        .WithTestSettings(settings => settings
          .AddAssembly(typeof(Program).Assembly)
          .ConfigureOutputSettings(output => output
            .SetReportOutputFolder(@"c:\temp\my-project")
            .SetReportOutputFileName("our-custom-filename.json")))
        .Execute()
          .ToExitCode();
    }
  }
}
