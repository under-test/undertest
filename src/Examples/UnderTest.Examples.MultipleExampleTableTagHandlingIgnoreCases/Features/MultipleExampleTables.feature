Feature: Multiple example tables filtering

  As an UnderTest developer
  I want to allow for selective example tables to be executed via tags
  So that I can selectively execute my scenarios
  
  Scenario Outline: Multiple example tables with varying tags
    
    Given <something> exists
    When Something happens
    Then This should now be true

    @smoke
    Examples: This should be executed
      | something |
      | 2         |

    Examples: This has no tags and should be ignored when run with `OnlyRunTheseTags("@smoke")`
      | something |
      | 1         |

    @ignore
    Examples: This should be ignored
      | something |
      | 1         |
    
    @future-version
    Examples: This should be marked as future-version and skipped
      | something |
      | 3         |
