using System;
using UnderTest.Attributes.StepParams;
using UnderTest.Exceptions;

namespace UnderTest.Examples.Parameters.StepParams
{
  public class ParameterizedGherkinDataTableStepParamAttribute : GherkinDataTableStepParamAttribute
  {
    public override string TransformCellValue(string heading, string cellText, int rowIndex, int colIndex)
    {
      return cellText switch
      {
        "`10 years ago`" => DateTime.Now.AddYears(-10).ToString("yyyy-MM-dd"),
        "`5 years hence`" => DateTime.Now.AddYears(5).ToString("yyyy-MM-dd"),
        _ => throw new UnknownGherkinValueException($"Argument does not match any expected case `{cellText}`", cellText)
      };
    }
  }
}
