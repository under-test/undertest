﻿using UnderTest.Attributes;

namespace UnderTest.Examples.Parameters.StepParams
{
  public class InjectedIntoStepParam : StepParamAttribute
  {
    [PropertyInjected]
    public BarService BarService { get; set;}

    public override object Transform(object input)
    {
      return BarService.AddShoes((string) input);
    }
  }

  public class BarService
  {
    public object AddShoes(string input)
    {
      return $"{input}Shoes";
    }
  }
}
