﻿using FluentAssertions;
using UnderTest.Attributes;
using UnderTest.Support;

namespace UnderTest.Examples.Parameters.FeatureHandlers
{
  [HandlesFeature("Parameters.EnumAliases.feature")]
  public class ParameterWithEnumAliases : FeatureHandler
  {
    private Basic basicValue;
    private WithAliases withAliases;

    [Given(@"the enum value is (.*)")]
    public void GivenTheNumberFromThisStep(Basic basic)
    {
      basicValue = basic;
    }

    [When(@"the enum value is (.*)")]
    public void WhenTheNUmberIsAdded(WithAliases withAliases)
    {
      this.withAliases = withAliases;
    }

    [Then(@"the enum values should be set")]
    public void ThenTheResultShouldBe()
    {
      basicValue.Should().Be(Basic.Foo);
      withAliases.Should().Be(WithAliases.Bar);
    }

    public enum Basic
    {
      Foo
    }

    public enum WithAliases
    {
      [EnumAlias("Something")]
      [EnumAlias("Something else")]
      Bar
    }
  }
}
