using System.Diagnostics.CodeAnalysis;
using System.Linq;
using FluentAssertions;
using UnderTest.Arguments;
using UnderTest.Attributes;
using UnderTest.Examples.Parameters.Core;

namespace UnderTest.Examples.Parameters.FeatureHandlers
{
  [HandlesFeature("Parameters.feature")]
  public class ParametersFeatureHandler : FeatureHandler
  {
    private GherkinDataTable table;

    private GherkinDocString docString;

    [Given(@"the following data table:")]
    public void GivenTheFollowingDataTable(GherkinDataTable tableP)
    {
      table = tableP;
    }

    [When(@"the doc string is pulled in:")]
    public void WhenTheTableIsPulledIn(GherkinDocString docStringP)
    {
      docString = docStringP;
    }

    [When("we are passed <value1> and <value2>")]
    public void WeArePassedValue1AndValue2(int value1, int value2)
    {
      Log.Information($"Value 1 is {value1}");
      Log.Information($"Value 2 is {value2}");
    }

    [When("the value of (.*)")]
    public void TheValueOfParam(string value)
    {
      value.Should().Be("SHOES");
    }

    [When("the user has a number of (ABC[0-9]+)")]
    public void GivenTheUSerHasAnIdOfNumber(string numberP)
    {
      numberP.Should().Be("ABC123456");
    }

    [When("the user also has an enum of <enum>")]
    public void TheUserAlsoHasAnEnum(Foo foo)
    {
      foo.Should().Be(Foo.Bar);
    }

    [Then(@"the table data and docString are available")]
    public void ThenTheDataTableIsAvailableInATableObject()
    {
      table.Should().NotBeNull();
      table.Rows.Count().Should().Be(1);

      docString.Content.Should().NotBeNull();
    }
  }
}
