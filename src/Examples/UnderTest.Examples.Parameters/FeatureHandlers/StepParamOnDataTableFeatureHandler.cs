using System;
using System.Linq;
using FluentAssertions;
using UnderTest.Arguments;
using UnderTest.Attributes;
using UnderTest.Examples.Parameters.StepParams;

namespace UnderTest.Examples.Parameters.FeatureHandlers
{
  [HandlesFeature("StepParamOnDataTable.feature")]
  public class StepParamOnDataTableFeatureHandler : FeatureHandler
  {
    [Given(@"the following dates:")]
    public void GivenTheFollowingDates([ParameterizedGherkinDataTableStepParam] GherkinDataTable table)
    {
      var firstRow = DateTime.ParseExact(table.Rows.First()["Date"].ToString(), "yyyy-MM-dd", null);
      var lastRow = DateTime.ParseExact(table.Rows.Last()["Date"].ToString(), "yyyy-MM-dd", null);

      firstRow.Year.Should().Be(DateTime.Now.AddYears(-10).Year);
      lastRow.Year.Should().Be(DateTime.Now.AddYears(5).Year);
    }
  }
}
