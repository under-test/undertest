﻿using System;
using System.Collections.Generic;
using FluentAssertions;
using UnderTest.Arguments;
using UnderTest.Attributes;

namespace UnderTest.Examples.Parameters.FeatureHandlers
{
  [HandlesFeature("Parameters.Guid.feature")]
  public class ParameterWithGuid : FeatureHandler
  {
    private GherkinDataTable table;
    private AppropriateType appropriateType;
    private IList<AppropriateType> appropriateTypeCollection;

    [Given("I have the following table:")]
    public void HaveTheFollowingTable(GherkinDataTable table) => this.table = table;

    [When("I call GenerateInstance on the table with an appropriate type")]
    public void ICallGenerateInstanceOnTheTableWithAnAppropriateType() =>
      appropriateType = table.GenerateInstance<AppropriateType>();

    [When("I call GenerateList on the table with an appropriate type")]
    public void ICallGenerateListOnTheTableWithAnAppropriateType() =>
      appropriateTypeCollection = table.GenerateList<AppropriateType>();

    [Then("I should get an instance of the appropriate type")]
    public void ShouldGetAnInstanceOfTheAppropriateTypeWithTheValues() =>
      appropriateType.Should().NotBeNull();

    [Then(@"I should get a collection of (\d+) items of the appropriate type")]
    public void ShouldGetAnListOfTheAppropriateTypeWithTheValues(int itemCount) =>
      appropriateTypeCollection.Should().NotBeNullOrEmpty()
        .And.HaveCount(itemCount);

    private class AppropriateType
    {
      public Guid Id { get; set; }
      public string Description { get; set; }
    }
  }
}
