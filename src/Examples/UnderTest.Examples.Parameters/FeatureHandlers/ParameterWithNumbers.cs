using FluentAssertions;
using UnderTest.Attributes;

namespace UnderTest.Examples.Parameters.FeatureHandlers
{
  [HandlesFeature("Parameters.Numbers.feature")]
  public class ParameterWithNumbers : FeatureHandler
  {
    private int number;
    private int result;

    [Given(@"the number (\d+) from this step")]
    public void GivenTheNumberFromThisStep(int numberP)
    {
      number = numberP;
    }

    [When(@"the number (\d+) is added")]
    public void WhenTheNUmberIsAdded(int numberToAdd)
    {
      result = number + numberToAdd;
    }

    [Then(@"the result should be (\d+)")]
    public void ThenTheResultShouldBe(int result)
    {
      this.result.Should().Be(result);
    }
  }
}
