using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using JetBrains.Annotations;
using UnderTest.Arguments;
using UnderTest.Attributes;

namespace UnderTest.Examples.Parameters.FeatureHandlers
{
  [HandlesFeature("GenerateInstancesOfObjectsUsingDataTables.feature")]
  public class GenerateInstancesOfObjectsUsingDataTablesFeatureHandler : FeatureHandler
  {
    private NestedClasses.UserProfile userProfile;

    private IEnumerable<NestedClasses.UserProfile> userProfiles;

    [Given(@"a user exists with profile information:")]
    public void GivenAUserExistsWithProfileInformation(GherkinDataTable userProfileTable)
    {
      userProfile = userProfileTable.GenerateInstance<NestedClasses.UserProfile>();
    }

    [Then("the profile surname should be (.*)")]
    public void ThenTheProfileSurnameShouldBeWildcard(string surname)
    {
      userProfile.Surname.Should().Be(surname);
    }

    [Given(@"users exist with profile information:")]
    public void GivenAUsersExistsWithProfileInformation(GherkinDataTable userProfilesTable)
    {
      userProfiles = userProfilesTable.GenerateList<NestedClasses.UserProfile>();
    }

    [Then("the last profile surname should be (.*)")]
    public void ThenTheSurnameShouldBeWildcard(string surname)
    {
      userProfiles.Last().Surname.Should().Be(surname);
    }

    [UsedImplicitly]
    public class NestedClasses
    {
      public class UserProfile
      {
        public string Email { get; set; }
        public string GivenName { get; set; }
        public string Surname { get; set; }
        public Language LanguagePreference { get; set; }
        public AccountType AccountType { get; set; }
      }

      public enum Language
      {
        English,
        French
      }

      public enum AccountType
      {
        User,
        Moderator,
        Admin,
        Developer
      }
    }
  }
}
