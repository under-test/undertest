using System.Diagnostics.CodeAnalysis;
using UnderTest.Examples.Parameters.StepParams;

[assembly: ExcludeFromCodeCoverageAttribute]

namespace UnderTest.Examples.Parameters
{
  class Program
  {
    static int Main(string[] args)
    {
      return new UnderTestRunner()
        .WithCommandLineArgs(args)
        .WithProjectDetails(x => x
          .SetProjectName("Set of examples showing off how to pass parameters to steps")
          .SetProjectVersionFromAssembly(typeof(Program).Assembly))
        .WithTestSettings(settings => settings
          .AddAssembly(typeof(Program).Assembly)
          .ConfigureContainer(x => x.Register<BarService>()))
        .Execute()
        .ToExitCode();
    }
  }
}
