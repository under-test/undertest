﻿Feature: Parse GUIDs from Data Tables

  Scenario: Calling GenerateInstance on a Data Table With a GUID
    Given I have the following table:
      | Id                                   | Description |
      | 00000000-0000-0000-0000-000000000000 | A GUID      |
    When I call GenerateInstance on the table with an appropriate type
    Then I should get an instance of the appropriate type

  Scenario: Calling GenerateList on a Data Table With a GUID
    Given I have the following table:
      | Id                                   | Description |
      | 00000000-0000-0000-0000-000000000000 | First GUID  |
      | 11111111-1111-1111-1111-111111111111 | Second GUID |
    When I call GenerateList on the table with an appropriate type
    Then I should get a collection of 2 items of the appropriate type
