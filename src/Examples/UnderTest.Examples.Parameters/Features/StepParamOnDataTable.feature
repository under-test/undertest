Feature: Parameterized Gherkin Data Table Example
  Scenario:
    Given the following dates:
      | Date            |
      | `10 years ago`  |
      | `5 years hence` |
