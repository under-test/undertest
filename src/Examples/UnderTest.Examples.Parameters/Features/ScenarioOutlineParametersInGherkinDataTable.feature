Feature: Another Demo

  Scenario Outline: Data table with Examples
    Given the following two numbers:
      | First Number   | Second Number   |
      | <first-number> | <second-number> |
    When the two numbers are added together
    Then the result should be <result>

    Examples: Numbers and results
      | first-number | second-number | result |
      | 0            | 0             | 0      |
      | 1            | 0             | 1      |
      | 0            | 1             | 1      |
      | 5            | 10            | 15     |
