Feature: Data table pulled into step definition

  Scenario Outline: Pull data table into step definition
    Given the following data table:
      | FirstProperty | SecondProperty |
      | first-value   | second value   |
    When we are passed <value1> and <value2>
      And the doc string is pulled in:
      """
      Something with basic formatting
      ===============
      Here is the first paragraph of my blog post. Lorem ipsum dolor sit amet,
      consectetur adipiscing elit.
      """
      And the value of <value3>
      And the user has a number of <number>
      And the user also has an enum of <enum>
    Then the table data and docString are available
    Examples:
      | value1 | value2 | value3 | number    | enum   |
      | 1      | 2      | SHOES  | ABC123456 | Bar    |
