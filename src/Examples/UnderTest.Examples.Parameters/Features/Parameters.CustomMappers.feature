Feature: Custom parameter parsing

  Scenario Outline: Sample scenario with a custom parsed step
    Given a user
    And the user's birth date is <relative-time>
    When the value '<relative-time>' is passed to a DateTime argument
    Then the argument receives the appropriate relative DateTime value
      And shoes is added to "something"

    Examples: Relative date and time
      | relative-time |
      | today         |
      | tomorrow      |
      | yesterday     |

    Scenario: Sample passing a collection of items
      Given the pet store has a cat, dog, bird
      Then the answers will be 1 - 54 - 887
