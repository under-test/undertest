Feature: Number parameters

  Scenario: scenarios using numbers as parameters
    Given the number 5 from this step
    When the number 10 is added
    Then the result should be 15
