﻿Feature: Enum alias parameters

  Scenario: scenarios using enums that are aliased as parameters
    Given the enum value is Foo
    When the enum value is Something
    Then the enum values should be set
