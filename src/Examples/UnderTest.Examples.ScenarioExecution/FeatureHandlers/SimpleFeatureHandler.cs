using System.Threading.Tasks;
using UnderTest.Attributes;

namespace UnderTest.Examples.ScenarioExecution.FeatureHandlers
{
  [HandlesFeature("SimpleFeature.feature")]
  public class SimpleFeatureHandler : FeatureHandler
  {
    [Scenario("simple scenario")]
    public async Task SimpleScenario()
    {
      Log.Information("    executing simple solution stuff");
    }

    [Scenario("simple scenario outline")]
    public async Task SimpleScenarioOutline(ScenarioExecutionContext scenarioContext)
    {
      Log.Information($"    executing simple scenario outline `{scenarioContext.ExampleTableRow}` stuff");
    }
  }
}
