Feature: Simple feature

  Scenario: simple scenario
    Given Something exists
    When Something happens
    Then This should now be true

  Scenario Outline: simple scenario outline
    Given Something exists
    When Something happens
    Then <something> should now be true
    Examples: 
    | something |
    | 1         |