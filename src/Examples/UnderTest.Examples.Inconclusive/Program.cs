using System.Diagnostics.CodeAnalysis;

[assembly: ExcludeFromCodeCoverageAttribute]

namespace UnderTest.Examples.InconclusiveAttribute
{
  class Program
  {
    static int Main(string[] args)
    {
      var result = new UnderTestRunner()
        .WithCommandLineArgs(args)
        .WithProjectDetails(x => x
          .SetProjectName("Example showing the Inconclusive attribute")
          .SetProjectVersion("0.1.0"))
        .WithTestSettings(settings => settings
          .AddAssembly(typeof(Program).Assembly))
        .Execute()
          .ToExitCode();

      return BuildExitCode(result);
    }

    private static int BuildExitCode(int result)
    {
      return (int) (result == (int) UnderTestExitCodes.Inconclusive
        ? UnderTestExitCodes.Pass
        : UnderTestExitCodes.Failure);
    }
  }
}
