﻿using UnderTest.Attributes;

namespace UnderTest.Examples.InconclusiveAttribute.FeatureHandlers
{
  [Attributes.Inconclusive(Message = "This feature handler hasn't been started yet.")]
  [HandlesFeature("InconclusiveFeature.feature")]
  public class InconclusiveFeatureHandler : FeatureHandler
  { }
}
