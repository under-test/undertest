using System.Diagnostics.CodeAnalysis;
using UnderTest;

[assembly: ExcludeFromCodeCoverageAttribute]

namespace UnderTest.Examples.Configuration
{
  class Program
  {
    static int Main(string[] args)
    {
      return new UnderTestRunner<TestSuiteConfiguration>()
        .WithCommandLineArgs(args)
        .WithProjectDetails(x => x
          .SetProjectName("UnderTest Configuration Example")
          .SetProjectVersionFromAssembly(typeof(Program).Assembly))
        .WithTestSettings(settings => settings
          .AddAssembly(typeof(Program).Assembly))
        .Execute()
          .ToExitCode();
    }
  }
}
