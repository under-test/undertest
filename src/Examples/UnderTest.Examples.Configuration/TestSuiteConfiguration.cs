using System;
using CommandLine;
using JetBrains.Annotations;
using UnderTest.Configuration;

namespace UnderTest.Examples.Configuration
{
  [UsedImplicitly]
  public class TestSuiteConfiguration: UnderTestCommandLineOptions
  {
    [Option('e', "ExampleValue")]
    public string ExampleValue { get; set; } = Environment.GetEnvironmentVariable("TestSuiteName.ExampleValue") ?? "this is the default value to use when the env var `SuiteName.ExampleValue` is not set and `ExampleValue` is not passed through the commandline";
  }
}
