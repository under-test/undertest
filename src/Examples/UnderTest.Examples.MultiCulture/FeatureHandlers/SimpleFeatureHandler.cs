using System.Globalization;
using FluentAssertions;
using UnderTest.Attributes;
using UnderTest.Examples.MultiCulture.assets.Language;
using UnderTest.Examples.MultiCulture.Config;
using UnderTest.Examples.MultiCulture.SimulatedProjectThatIsUnderTest;

namespace UnderTest.Examples.MultiCulture.FeatureHandlers
{
  [HandlesFeature("SimpleFeature.feature")]
  public class SimpleFeatureHandler : FeatureHandler
  {
    [PropertyInjected] TestSuiteConfiguration Config { get; set; }

    private string result;
    private string who;

    [Given("we know how to greet people")]
    public void SomethingExists()
      => Log.Information($"    Current culture: {Config.Culture}");

    [When("we greet (.*)")]
    public void SomethingHappens(string whoP)
    {
      who = whoP;
      result = new HelloWorldGenerator(Config.Culture).SayIt(whoP);
    }

    // note: if you wanted to check values without the resources, you could use values specific to the tests
    [Then("they should have been greed")]
    public void ThisExists()
      => result.Should().Be($"{Hello.HelloWorldGenerator_helloText} {who}");
  }
}
