﻿using System;
using JetBrains.Annotations;
using UnderTest.Examples.MultiCulture.assets.Language;

namespace UnderTest.Examples.MultiCulture.SimulatedProjectThatIsUnderTest
{
  // This class is simulating an external project which we are testing using UnderTest.
  // If you have feedback on this class, please create an issue, we are always looking to improve our examples.
  // This is intended to be a tiny bit of logic where language is a key parameter, forcing our tests to be multi-language.
  // For more on tooling to help with this approach, see: https://www.jetbrains.com/help/rider/Resources__LocalizationManager.html
  public class HelloWorldGenerator
  {
    public HelloWorldGenerator(string lang)
    {
      this.lang = lang;
    }

    private readonly string lang;

    [LocalizationRequired(true)]
    private readonly string helloText = Hello.HelloWorldGenerator_helloText;

    public string SayIt(string toWhom) =>
      lang switch
      {
        "en" => $"{helloText} {toWhom}",
        "fr" => $"{helloText} {toWhom}",
        _ => throw new Exception("Unknown value for language: " + lang)
      };
  }
}
