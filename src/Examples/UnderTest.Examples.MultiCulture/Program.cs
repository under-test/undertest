using System.Diagnostics.CodeAnalysis;
using UnderTest.Examples.MultiCulture.Config;

[assembly: ExcludeFromCodeCoverageAttribute]

namespace UnderTest.Examples.MultiCulture
{
  class Program
  {
    static int Main(string[] args)
    {
      return new UnderTestRunner<TestSuiteConfiguration>()
        .WithCommandLineArgs(args)
        .WithProjectDetails(x => x
          .SetProjectName("UnderTest multi-culture example")
          .SetProjectVersion("0.1.0"))
        .WithTestSettings(settings => settings
          .AddAssembly(typeof(Program).Assembly))
        .Execute()
          .ToExitCode();
    }
  }
}
