using UnderTest.Attributes;

namespace UnderTest.Examples.TestRunEventHandlers.FeatureHandlers
{
  [HandlesFeature("SimpleFeature.feature")]
  public class SimpleFeatureHandler : FeatureHandler
  {
    [Given("Something exists")]
    public void SomethingExists() => Noop();

    [When("Something happens")]
    public void SomethingHappens() => Noop();

    [Then("This should now be true")]
    public void ThisExists() => Noop();
  }
}
