﻿using System.Reflection;
using UnderTest.Attributes;

namespace UnderTest.Examples.TestRunEventHandlers
{
  [TestRunEventHandler]
  public class TestRunHooks : TestRunEventHandlerBase
  {
    public override void BeforeTestRun(BeforeAfterTestRunContext context)
      => TestSettings.Logger.Information($"In {MethodBase.GetCurrentMethod().Name}");

    public override void BeforeFeature(BeforeAfterFeatureContext context)
      => TestSettings.Logger.Information($"In {MethodBase.GetCurrentMethod().Name}");

    public override void BeforeScenario(BeforeAfterScenarioContext context)
      => TestSettings.Logger.Information($"In {MethodBase.GetCurrentMethod().Name}");

    public override void BeforeBackgroundKeywordGrouping(BeforeAfterKeywordGroupingContext context)
      => TestSettings.Logger.Information($"In {MethodBase.GetCurrentMethod().Name}");

    public override void BeforeScenarioKeywordGrouping(BeforeAfterKeywordGroupingContext context)
      => TestSettings.Logger.Information($"In {MethodBase.GetCurrentMethod().Name}");

    public override  void BeforeStep(BeforeAfterStepContext context)
      => TestSettings.Logger.Information($"In {MethodBase.GetCurrentMethod().Name}");

    public override void AfterFeature(BeforeAfterFeatureContext context)
      => TestSettings.Logger.Information($"In {MethodBase.GetCurrentMethod().Name}");

    public override void AfterScenario(BeforeAfterScenarioContext context)
      => TestSettings.Logger.Information($"In {MethodBase.GetCurrentMethod().Name}");

    public override void AfterScenarioKeywordGrouping(BeforeAfterKeywordGroupingContext context)
      => TestSettings.Logger.Information($"In {MethodBase.GetCurrentMethod().Name}");

    public override void AfterBackgroundKeywordGrouping(BeforeAfterKeywordGroupingContext context)
      => TestSettings.Logger.Information($"In {MethodBase.GetCurrentMethod().Name}");

    public override void AfterStep(BeforeAfterStepContext context)
      => TestSettings.Logger.Information($"In {MethodBase.GetCurrentMethod().Name}");

    public override void AfterTestRun(BeforeAfterTestRunContext context)
      => TestSettings.Logger.Information($"In {MethodBase.GetCurrentMethod().Name}");
  }
}
