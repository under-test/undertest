using System.Threading;
using JetBrains.Annotations;
using UnderTest.Attributes;

namespace UnderTest.Examples.ExecutionSettings.FeatureHandlers
{
  [HandlesFeature("ScenarioTimeoutExample.feature")]
  public class SimpleFeatureHandler : FeatureHandler
  {
    [Given("Something exists")]
    public void SomethingExists() => Noop();

    [When("something happens that will timeout - this will fail")]
    public void SomethingHappens()
    {
      // simulate some longer running process
      Log.Information("    something longer running happens is about to happen");
      Thread.Sleep(2000);
    }

    [Then("This should now be true")]
    public void ThisExists() => Noop();
  }
}
