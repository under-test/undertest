using System.Diagnostics.CodeAnalysis;
using UnderTest.Infrastructure;
using UnderTest.Locators;

namespace UnderTest.Tests.TestSupportingClasses.TestRunSettingsTypes
{
  public class NullStepLocator : IStepLocator
  {
    public NullStepLocator(TestRunSettings settings)
    {
      TestSettings = settings;
    }

    public string Name { get; } = "Null Step Locator";

    public TestRunSettings TestSettings { get; }

    public StepLocatorMatch LocateStep(StepForProcessing step, IFeatureHandler handler)
    {
      return null;
    }
  }
}
