using UnderTest.Attributes;

namespace UnderTest.Tests.TestSupportingClasses.ExampleFeatureHandlers
{
  public class ValueBasedMatcherExampleFeatureHandler : FeatureHandler
  {
    [Given("I click (two) times")]
    public void IAmAStepThatMatchesBasedOnRegex(string times)
    {
      Log.Information("I am a step that matches based on regex - called");
    }
  }
}
