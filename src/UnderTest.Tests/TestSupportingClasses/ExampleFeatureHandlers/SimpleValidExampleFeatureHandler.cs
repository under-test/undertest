using System.Diagnostics.CodeAnalysis;
using UnderTest.Attributes;

namespace UnderTest.Tests.TestSupportingClasses.ExampleFeatureHandlers
{
  [HandlesFeature("IDontExist.feature")]
  public class SimpleValidExampleFeatureHandler : FeatureHandler
  {
    [Given("I am a step that matches")]
    public void IAmAStepThatMatches()
    {
      Log.Information("I am a step that matches - called");
    }

    [Given("Partial (match)")]
    public void PartialMatch()
    {
      Log.Information("Partial match");
    }

    [Given("I click (.*) times")]
    public void IAmAStepThatMatchesBasedOnRegex(string times)
    {
      Log.Information("I am a step that matches based on regex - called");
    }
  }
}
