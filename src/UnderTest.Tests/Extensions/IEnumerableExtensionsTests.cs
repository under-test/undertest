using System.Collections.Generic;
using FluentAssertions;
using Gherkin.Ast;
using NUnit.Framework;

namespace UnderTest.Tests.Extensions
{
  public class IEnumerableExtensionsTests
  {
    [Test]
    public void ToStepForProcessingList_PassedNull_ReturnsEmptyList()
    {
      List<Step> instance = null;

      var result = instance.ToStepsForProcessingList();

      result.Count.Should().Be(0);
    }

    [Test]
    public void ToStepForProcessingList_PassedList_Returns()
    {
      var instance = new List<Step> {new Step(null, "test", "test", null)};

      var result = instance.ToStepsForProcessingList();

      result.Count.Should().Be(1);
    }
  }
}
