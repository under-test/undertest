using System;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.Infrastructure;
using UnderTest.Reporting;

namespace UnderTest.Tests.Extensions
{
  public class UnderTestRunResultExtensionsTests
  {
    [Test]
    public void ToExitCode_WhenNull_ThrowsNRE()
    {
      UnderTestRunResult instance = null;
      Action act = () => instance.ToExitCode();

      act.Should().Throw<NullReferenceException>();
    }

    [Test]
    public void ToExitCode_WhenSuccessFull_ReturnsZero()
    {
      var instance = new UnderTestRunResult(new TestProjectDetails()) {OverallResult = TestRunOverallResult.Pass};

      var result = instance.ToExitCode();

      result.Should().Be((int)UnderTestExitCodes.Pass);
    }

    [Test]
    public void ToExitCode_WhenInconclusive_ReturnsZero()
    {
      var instance = new UnderTestRunResult(new TestProjectDetails()) { OverallResult = TestRunOverallResult.Inconclusive };

      var result = instance.ToExitCode();

      result.Should().Be((int)UnderTestExitCodes.Inconclusive);
    }

    [Test]
    public void ToExitCode_WhenFailed_ReturnsZero()
    {
      var instance = new UnderTestRunResult(new TestProjectDetails()) { OverallResult = TestRunOverallResult.Failure };

      var result = instance.ToExitCode();

      result.Should().Be((int)UnderTestExitCodes.Failure);
    }
  }
}
