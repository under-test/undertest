using FluentAssertions;
using Gherkin.Ast;
using NUnit.Framework;

namespace UnderTest.Tests.Extensions
{
  public class ScenarioExtensionsTests
  {
    [Test]
    public void IsScenarioOutline_PassedNull_ReturnsFalse()
    {
      Scenario instance = null;

      var result = instance.IsScenarioOutline();

      result.Should().BeFalse();
    }

    [Test]
    public void IsScenarioOutline_PassedScenarioOutlineKeyword_ReturnsTrue()
    {
      var instance = new Scenario(null, null, "Scenario Outline", "test", string.Empty, null, null);

      var result = instance.IsScenarioOutline();

      result.Should().BeTrue();
    }

    [Test]
    public void IsScenarioOutline_PassedScenarioTemplateKeyword_ReturnsTrue()
    {
      var instance = new Scenario(null, null, "Scenario Template", "test", string.Empty, null, null);

      var result = instance.IsScenarioOutline();

      result.Should().BeTrue();
    }

    [Test]
    public void IsScenarioOutline_PassedScenarioKeyword_ReturnsTrue()
    {
      var instance = new Scenario(null, null, "Scenario", "test", string.Empty, null, null);

      var result = instance.IsScenarioOutline();

      result.Should().BeFalse();
    }
  }
}
