using FluentAssertions;
using NUnit.Framework;
using UnderTest.Filtering;
using UnderTest.Tests.TestSupportingClasses.FeatureContextRelated;

namespace UnderTest.Tests.Extensions
{
  public class TestRunSettingsExtensionsTests
  {
    private FeatureContextFactory featureContextFactory;

    [SetUp]
    public void Setup()
    {
      featureContextFactory = new FeatureContextFactory();
    }

    [Test]
    public void OnlyRunTheseTags_WhenPassedATag_FilterAdded()
    {
      var instance = new TestRunSettings();

      instance.OnlyRunTheseTags("@smoke");

      instance.TestFilters.Count.Should().Be(1);
    }

    [Test]
    public void DoNotRunTheseTags_WhenPassedATag_FilterAdded()
    {
      var instance = new TestRunSettings();

      instance.DoNotRunTheseTags("@smoke");

      instance.TestFilters.Count.Should().Be(1);
    }

    [Test]
    public void DoNotRunTheseTags_WhenPassedATagThatDoesntMatchCase_FilterAddedAndMatches()
    {
      // arrange
      const string tagName = "@smoke";
      var instance = new TestRunSettings();
      var featureContext = featureContextFactory.GenerateFeatureContextWithNumberOfTags(1, () => tagName);

      // act
      instance.DoNotRunTheseTags(tagName.ToUpper());
      var filterResult = featureContext.ShouldBeFiltered(instance.TestFilters, new TestFilterContext{ FeatureContext = featureContext});

      // assert
      instance.TestFilters.Count.Should().Be(1);
      filterResult.Should().BeTrue();
    }

    [Test]
    public void DoNotRunTheseTags_WhenPassedATagThatDoesntMatch_FilterAddedAndDoesNotMatches()
    {
      // arrange
      const string tagName = "@smoke";
      var instance = new TestRunSettings();
      var featureContext = featureContextFactory.GenerateFeatureContextWithNumberOfTags(1, () => tagName);

      // act
      instance.DoNotRunTheseTags("NotSmoke");
      var filterResult = featureContext.ShouldBeFiltered(instance.TestFilters, new TestFilterContext{ FeatureContext = featureContext});

      // assert
      instance.TestFilters.Count.Should().Be(1);
      filterResult.Should().BeFalse();
    }
  }
}
