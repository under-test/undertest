using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.Exceptions;
using UnderTest.Infrastructure;
using UnderTest.Strategies;
using UnderTest.Tests.TestSupportingClasses.TestRunSettingsTypes;

namespace UnderTest.Tests
{
  public class TestRunSettingsTests
  {
    [Test]
    public void Constructor_WhenCalled_InitializesVariables()
    {
      var instance = new TestRunSettings();

      instance.Assemblies.Should().NotBeNull();
      instance.FeatureHandlerLocator.Should().NotBeNull();
      instance.Logger.Should().NotBeNull();
      instance.OutputSettings.Should().NotBeNull();
      instance.ReportOutputWriter.Should().NotBeNull();
      instance.StepLocator.Should().NotBeNull();
      instance.Strategies.Should().NotBeNull();
      instance.TestFilters.Should().NotBeNull();
      instance.TypesCache.Should().NotBeNull();
      instance.ExecutionMode.Should().Be(ExecutionMode.Normal);
    }

    [Test]
    public void AddAssembly_PassedNull_ThrowsArgumentNullException()
    {
      var instance = new TestRunSettings();

      // ReSharper disable once AssignNullToNotNullAttribute
      Action act = () => instance.AddAssembly(null);

      act.Should().Throw<ArgumentNullException>();
    }

    [Test]
    public void AddAssembly_PassedValidAssembly_AddsToAssemblies()
    {
      var instance = new TestRunSettings();

      instance.AddAssembly(typeof(TestRunSettingsTests).Assembly);

      instance.Assemblies.Count().Should().Be(1);
    }

    [Test]
    public void AddAssemblies_PassedNull_ThrowsArgumentNullException()
    {
      var instance = new TestRunSettings();

      // ReSharper disable once AssignNullToNotNullAttribute
      Action act = () => instance.AddAssemblies(null);

      act.Should().Throw<ArgumentNullException>();
    }

    [Test]
    public void AddAssemblies_PassedValidAssemblies_AddsToAssemblies()
    {
      var instance = new TestRunSettings();

      instance.AddAssemblies(new List<Assembly> { typeof(TestRunSettingsTests).Assembly, typeof(TestRunSettings).Assembly });

      instance.Assemblies.Count().Should().Be(2);
    }

    [Test]
    public void AddPostRunVerification_PassedNull_ThrowsArgumentNullException()
    {
      var instance = new TestRunSettings();

      // ReSharper disable once AssignNullToNotNullAttribute
      Action act = () => instance.AddPostRunVerification(null);

      act.Should().Throw<ArgumentNullException>();
    }

    [Test]
    public void AddPostRunVerification_PassedValidDelegate_AddsToPostRunVerification()
    {
      var instance = new TestRunSettings();

      instance.AddPostRunVerification(result => result);

      instance.PostRunVerification.Count().Should().Be(1);
    }

    [Test]
    public void AddTestFilter_PassedNull_ThrowsArgumentNullException()
    {
      var instance = new TestRunSettings();

      // ReSharper disable once AssignNullToNotNullAttribute
      Action act = () => instance.AddTestFilter(null);

      act.Should().Throw<ArgumentNullException>();
    }

    [Test]
    public void AddTestFilter_PassedTestFilter_AddsToTestFilters()
    {
      var instance = new TestRunSettings();

      instance.AddTestFilter(context => true);

      instance.TestFilters.Count().Should().Be(1);
    }

    [Test]
    public void AddStrategy_PassedNull_ThrowsArgumentNullException()
    {
      var instance = new TestRunSettings();

      // ReSharper disable once AssignNullToNotNullAttribute
      Action act = () => instance.AddStrategy(null);

      act.Should().Throw<ArgumentNullException>();
    }

    [Test]
    public void AddStrategy_PassedStrategy_AddsToStrategies()
    {
      var instance = new TestRunSettings();

      instance.AddStrategy(new DefaultGlobalStrategyTestStrategy());

      instance.Strategies.Count().Should().Be(1);
    }

    [Test]
    public void FeatureDirectory_WhenNoAssembliesConfigured_ThrowsNoFeaturesAssembliesConfiguredException()
    {
      var instance = new TestRunSettings();

      // ReSharper disable once RedundantToStringCall
      // ReSharper disable once ReturnValueOfPureMethodIsNotUsed
      Action act = () => instance.FeatureDirectory.ToString();

      act.Should().Throw<NoFeatureAssembliesConfiguredException>();
    }

    [Test]
    public void FeatureDirectory_WhenAssemblyAdded_ReturnsDirectory()
    {
      var instance = new TestRunSettings();
      instance.AddAssembly(typeof(TestRunSettingsTests).Assembly);

      var result = instance.FeatureDirectory;

      result.Should().NotBeNull();
    }

    [Test]
    public void Init_WhenCalledWithoutAssembly_ReturnsInstance()
    {
      var instance = new TestRunSettings();

      var result = instance.Init();

      result.Should().BeTrue();
    }

    [Test]
    public void Init_WhenCalledWithAnAssembly_ReturnsInstance()
    {
      var instance = new TestRunSettings();
      instance.AddAssembly(typeof(TestRunSettingsTests).Assembly);

      var result = instance.Init();

      result.Should().BeTrue();
    }

    [Test]
    public void SetContainerLocator_PassedNull_ThrowsArgumentNullException()
    {
      var instance = new TestRunSettings();

      // ReSharper disable once AssignNullToNotNullAttribute
      Action act = () => instance.SetFeatureHandlerLocator(null);

      act.Should().Throw<ArgumentNullException>();
    }

    [Test]
    public void SetContainerLocator_NewInstance_SetsFeatureHandlerLocator()
    {
      var instance = new TestRunSettings();

      instance.SetFeatureHandlerLocator(new NullFeatureHandlerLocator(instance));

      instance.FeatureHandlerLocator.Should().BeOfType<NullFeatureHandlerLocator>();
    }

    [Test]
    public void SetGherkinLoader_PassedNull_ThrowsArgumentNullException()
    {
      var instance = new TestRunSettings();

      // ReSharper disable once AssignNullToNotNullAttribute
      Action act = () => instance.SetGherkinLoader(null);

      act.Should().Throw<ArgumentNullException>();
    }

    [Test]
    public void SetGherkinLoader_NewInstance_SetsGherkinLoader()
    {
      var instance = new TestRunSettings();

      instance.SetGherkinLoader(new NullGherkinLoader(instance));

      instance.GherkinLoader.Should().BeOfType<NullGherkinLoader>();
    }

    [Test]
    public void SetStepLocator_PassedNull_ThrowsArgumentNullException()
    {
      var instance = new TestRunSettings();

      // ReSharper disable once AssignNullToNotNullAttribute
      Action act = () => instance.SetStepLocator(null);

      act.Should().Throw<ArgumentNullException>();
    }

    [Test]
    public void SetStepLocator_NewInstance_SetsStepLocator()
    {
      var instance = new TestRunSettings();

      instance.SetStepLocator(new NullStepLocator(instance));

      instance.StepLocator.Should().BeOfType<NullStepLocator>();
    }

    [Test]
    public void SetReportOutputWriter_PassedNull_ThrowsArgumentNullException()
    {
      var instance = new TestRunSettings();

      // ReSharper disable once AssignNullToNotNullAttribute
      Action act = () => instance.SetReportOutputWriter(null);

      act.Should().Throw<ArgumentNullException>();
    }

    [Test]
    public void SetReportOutputWriter_NewInstance_SetsReportOutputWriter()
    {
      var instance = new TestRunSettings();

      instance.SetReportOutputWriter(new NullReportOutputWriter());

      instance.ReportOutputWriter.Should().BeOfType<NullReportOutputWriter>();
    }
  }
}
