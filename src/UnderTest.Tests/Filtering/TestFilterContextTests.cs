using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Gherkin.Ast;
using NUnit.Framework;
using UnderTest.Filtering;

namespace UnderTest.Tests.Filtering
{
  public class TestFilterContextTests
  {
    [Test]
    public void Tags_WhenScenarioAndFeatureTagsAreNull_EmptyListReturned()
    {
      var instance = new TestFilterContext
      {
        Scenario =  null,
        FeatureContext = BuildFeatureContextWithTags(null)
      };

      var result = instance.Tags;

      result.Should().BeEmpty();
    }

    [Test]
    public void Tags_WhenScenarioIsNull_FeatureTagsAreReturned()
    {
      const string expected = "shoes";
      var tags = new List<Tag>{ new Tag(null, expected)};
      var instance = new TestFilterContext
      {
        Scenario =  null,
        FeatureContext = BuildFeatureContextWithTags(tags)
      };

      var result = instance.Tags;

      result.Count().Should().Be(1);
      result.First().Name.Should().Be(expected);
    }

    [Test]
    public void Tags_WhenFeatureAndScenarioAreSet_FeatureTagsAreReturned()
    {
      const string expected = "shoes";
      var tags = new List<Tag>{ new Tag(null, expected)};
      var instance = new TestFilterContext
      {
        Scenario = BuildScenarioWithTags(tags),
        FeatureContext = BuildFeatureContextWithTags(tags)
      };

      var result = instance.Tags;

      result.Count().Should().Be(2);
      result.First().Name.Should().Be(expected);
      result.ToArray()[1].Name.Should().Be(expected);
    }
    
    [Test]
    public void Tags_WhenFeatureScenarioAndSpecificExampleAreSet_FeatureTagsAreReturned()
    {
      const string scenarioExpected = "shoes";
      const string exampleTag = "specific-tag";
      var scenarioTags = new List<Tag>{ new Tag(null, scenarioExpected)};
      var exampleTags = new List<Tag>{ new Tag(null, exampleTag)};
      var instance = new TestFilterContext
      {
        Scenario = BuildScenarioWithTagsAndExampleWithTags(scenarioTags, exampleTags),
        FeatureContext = BuildFeatureContextWithTags(scenarioTags)
      };

      var result = instance.Tags;

      result.Count().Should().Be(3);
      result.First().Name.Should().Be(scenarioExpected);
      result.ToArray()[1].Name.Should().Be(scenarioExpected);
      result.Last().Name.Should().Be(exampleTag);
    }

    [Test]
    public void Tags_WhenFeatureNullAndScenarioAreSet_FeatureTagsAreReturned()
    {
      const string expected = "shoes";
      var tags = new List<Tag>{ new Tag(null, expected)};
      var instance = new TestFilterContext
      {
        Scenario = BuildScenarioWithTags(tags),
        FeatureContext = null
      };

      var result = instance.Tags;

      result.Count().Should().Be(1);
      result.First().Name.Should().Be(expected);
    }

    private static Scenario BuildScenarioWithTags(List<Tag> tags)
    {
      return new Scenario(tags.ToArray(), null, "Scenario", "Test scenario", "", null, null);
    }

    private static Scenario BuildScenarioWithTagsAndExampleWithTags(List<Tag> tags, List<Tag> exampleTags)
    {
      return new Scenario(tags.ToArray(), null, "Scenario", "Test scenario", "", null, new []{ new Examples(exampleTags?.ToArray(), null, "Examples", string.Empty, null, null, null)});
    }

    private static FeatureContext BuildFeatureContextWithTags(List<Tag> tags)
    {
      return new FeatureContext
      {
        Document = BuildBasicGherkinDocumentWithTags(tags)
      };
    }

    private static GherkinDocument BuildBasicGherkinDocumentWithTags(List<Tag> tags)
    {
      return new GherkinDocument(new Feature(tags?.ToArray(), null, "en", "Feature", "test", null, null), null);
    }
  }
}
