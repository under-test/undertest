using FluentAssertions;
using NUnit.Framework;
using UnderTest.Exceptions;

namespace UnderTest.Tests.Exceptions
{
  public class NoFeatureAssembliesConfiguredExceptionTests
  {
    [Test]
    public void DefaultConstructor_PassedMessage_MessageIsSet()
    {
      const string message = "shoes";

      var instance = new NoFeatureAssembliesConfiguredException(message);

      instance.Message.Should().Be(message);
      instance.InnerException.Should().BeNull();
    }
  }
}