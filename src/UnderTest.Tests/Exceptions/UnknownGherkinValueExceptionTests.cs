﻿using System;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.Exceptions;

namespace UnderTest.Tests.Exceptions
{
  public class UnknownGherkinValueExceptionTests
  {
    [Test]
    public void DefaultConstructor_PassedMessage_MessageIsSet()
    {
      const string message = "shoes";

      var instance = new UnknownGherkinValueException(message);

      instance.Message.Should().Be(message);
      instance.InnerException.Should().BeNull();
    }
    
    [Test]
    public void Class_Serialization_Deserializes()
    {
      const string message = "shoes";
      const string value = "test-value";

      var inner = new Exception("inner");
      var instance = new UnknownGherkinValueException(message, inner, value);

      var result = instance.Clone();

      result.Value.Should().Be(instance.Value);
      result.InnerException.Message.Should().Be(instance.InnerException.Message);
    }
  }
}
