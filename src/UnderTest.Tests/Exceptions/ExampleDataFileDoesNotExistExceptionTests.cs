using System;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.Exceptions;

namespace UnderTest.Tests.Exceptions
{
  public class ExampleDataFileDoesNotExistExceptionTests
  {
    [Test]
    public void DefaultConstructor_PassedMessage_MessageIsSet()
    {
      const string message = "shoes";

      var instance = new ExampleDataFileDoesNotExistException(message);

      instance.Message.Should().Be(message);
      instance.InnerException.Should().BeNull();
    }

    [Test]
    public void Class_Serialization_Deserializes()
    {
      const string message = "shoes";
      const string filename = "test.file";

      var inner = new Exception("inner");
      var instance = new ExampleDataFileDoesNotExistException(message, inner)
      {
        Filename = filename
      };

      var result = instance.Clone();

      result.Filename.Should().Be(instance.Filename);
      result.InnerException.Message.Should().Be(instance.InnerException.Message);
    }
  }
}