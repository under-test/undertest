using System;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.Exceptions;

namespace UnderTest.Tests.Exceptions
{
  public class WorkInProgressScenarioExceptionTests
  {
    [Test]
    public void DefaultConstructor_PassedMessage_MessageIsSet()
    {
      const string message = "shoes";

      var instance = new WorkInProgressScenarioException(message);

      instance.Message.Should().Be(message);
      instance.InnerException.Should().BeNull();
    }

    [Test]
    public void Class_Serialization_Deserializes()
    {
      const string message = "shoes";
      const string stepKeyword = "given";
      const string stepText = "something is true";

      var inner = new Exception("inner");
      var instance = new WorkInProgressScenarioException(message, inner)
      {
        StepKeyword = stepKeyword,
        StepText = stepText
      };

      var result = instance.Clone();

      result.StepKeyword.Should().Be(instance.StepKeyword);
      result.StepText.Should().Be(instance.StepText);
      result.InnerException.Message.Should().Be(instance.InnerException.Message);
    }
  }
}