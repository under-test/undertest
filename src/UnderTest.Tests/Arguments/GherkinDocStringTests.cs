using FluentAssertions;
using NUnit.Framework;
using UnderTest.Arguments;

namespace UnderTest.Tests.Arguments
{
  public class GherkinDocStringTests
  {
    [Test]
    public void Constructor_WhenPassedValues_ContentAndContentTypeSet()
    {
      var expectedContentType = "text/x.cucumber.gherkin+plain";
      var expectedContent = "shoes";

      var result = new GherkinDocString(expectedContentType, expectedContent);

      result.ContentType.Should().Be(expectedContentType);
      result.Content.Should().Be(expectedContent);
    }
  }
}
