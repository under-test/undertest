using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Gherkin.Ast;
using NUnit.Framework;
using UnderTest.Arguments;

namespace UnderTest.Tests.Arguments
{
  public class GherkinDataTableTests
  {
    [Test]
    public void Constructor_PassedDataTable_HeaderAndRowsPropSet()
    {
      const string expectedHeader = "header";
      const string expectedValue = "shoes";
      var dataTable = new DataTable(new List<TableRow>
      {
        new TableRow(null, GenerateTableCells(expectedHeader)),
        new TableRow(null, GenerateTableCells(expectedValue))
      }.ToArray());
      var result = new GherkinDataTable(dataTable);

      result.Header.Cells.First().Value.Should().Be(expectedHeader);
      result.Rows.Count().Should().Be(1);
      var row = result.Rows.First();
      row.Cells.First().Value.Should().Be(expectedValue);
      row[expectedHeader].Value.Should().Be(expectedValue);
      row[0].Value.Should().Be(expectedValue);
    }

    [Test]
    public void GenerateInstance_WhenNoHeader_ThrowsException()
    {
      const string heading = "shoes";
      var stepRow1 = new TableRow(null, GenerateTableCells(heading));
      var stepDataTable = new DataTable(new [] { stepRow1 });
      var table = new GherkinDataTable(stepDataTable);

      Action act = () => table.GenerateInstance<NestedTestTypes.GenerateInstanceSingleProperty>();

      // todo - make this a custom type
      act.Should().Throw<Exception>();
    }

    [Test]
    public void GenerateInstance_WhenMultipleRows_ThrowsException()
    {
      const string heading = "shoes";
      const string value = "test";
      var stepRow1 = new TableRow(null, GenerateTableCells(heading));
      var stepRow2 = new TableRow(null, GenerateTableCells(value));
      var stepRow3 = new TableRow(null, GenerateTableCells(value));
      var stepDataTable = new DataTable(new [] { stepRow1, stepRow2, stepRow3 });
      var table = new GherkinDataTable(stepDataTable);

      Action act = () => table.GenerateInstance<NestedTestTypes.GenerateInstanceSingleProperty>();

      // todo - make this a custom type
      act.Should().Throw<Exception>();
    }

    [Test]
    public void GenerateInstance_WhenPassedOneRowOnBasicType_ReturnsInstance()
    {
      const string heading = "shoes";
      const string value = "test";
      var stepRow1 = new TableRow(null, GenerateTableCells(heading));
      var stepRow2 = new TableRow(null, GenerateTableCells(value));
      var stepDataTable = new DataTable(new [] { stepRow1, stepRow2 });
      var table = new GherkinDataTable(stepDataTable);

      var result = table.GenerateInstance<NestedTestTypes.GenerateInstanceSingleProperty>();

      result.Shoes.Should().Be(value);
    }

    [Test]
    public void GenerateInstance_WhenPassedOneRowOnMultiColumnType_ReturnsInstance()
    {
      const string heading1 = "shoes";
      const string heading2 = "NotShoes";
      const string value1 = "test1";
      const string value2 = "test2";
      var stepRow1 = new TableRow(null, GenerateTableCells(heading1, heading2));
      var stepRow2 = new TableRow(null, GenerateTableCells(value1, value2));
      var stepDataTable = new DataTable(new [] { stepRow1, stepRow2 });
      var table = new GherkinDataTable(stepDataTable);

      var result = table.GenerateInstance<NestedTestTypes.GenerateInstanceMultipleProperty>();

      result.Shoes.Should().Be(value1);
      result.NotShoes.Should().Be(value2);
    }

    [Test]
    public void GenerateInstance_WhenPassedOneRowOnBasicTypeWithNonString_ReturnsInstance()
    {
      const string heading = "shoes";
      const int value = 1;
      var stepRow1 = new TableRow(null, GenerateTableCells(heading));
      var stepRow2 = new TableRow(null, GenerateTableCells(value.ToString()));
      var stepDataTable = new DataTable(new[] {stepRow1, stepRow2});
      var table = new GherkinDataTable(stepDataTable);

      var result = table.GenerateInstance<NestedTestTypes.GenerateInstanceSingleNonStringProperty>();

      result.Shoes.Should().Be(value);
    }

    [Test]
    public void GenerateInstance_WhenPassedStringOfCharactersNull_ReturnsNull()
    {
      const string heading = "shoes";
      const string value = null;
      var stepRow1 = new TableRow(null, GenerateTableCells(heading));
      var stepRow2 = new TableRow(null, GenerateTableCells("null"));
      var stepDataTable = new DataTable(new[] {stepRow1, stepRow2});
      var table = new GherkinDataTable(stepDataTable);

      var result = table.GenerateInstance<NestedTestTypes.GenerateInstanceSingleNullStringProperty>();

      result.Shoes.Should().Be(value);
    }

    [Test]
    public void GenerateInstance_WhenPassedTypeThatContainsAndEnum_TheEnumIsSet()
    {
      const string heading = "shoes";
      const NestedTestTypes.AccountType value = NestedTestTypes.AccountType.Moderator;
      var stepRow1 = new TableRow(null, GenerateTableCells(heading));
      var stepRow2 = new TableRow(null, GenerateTableCells(value.ToString()));
      var stepDataTable = new DataTable(new[] {stepRow1, stepRow2});
      var table = new GherkinDataTable(stepDataTable);

      var result = table.GenerateInstance<NestedTestTypes.GenerateInstanceEnumProperty>();

      result.Shoes.Should().Be(value);
    }

    [Test]
    public void GenerateInstance_WhenPassedHeadingThatContainsAString_TheEnumIsSet()
    {
      const string heading = "Name With Spaces";
      const string value = "something";
      var stepRow1 = new TableRow(null, GenerateTableCells(heading));
      var stepRow2 = new TableRow(null, GenerateTableCells(value));
      var stepDataTable = new DataTable(new[] {stepRow1, stepRow2});
      var table = new GherkinDataTable(stepDataTable);

      var result = table.GenerateInstance<NestedTestTypes.GenerateInstanceNameWithSpaces>();

      result.NameWithSpaces.Should().Be(value);
    }

    [Test]
    public void GenerateInstance_WhenPassedTypeThatSeveralFieldsAndEnum_TheFieldsAre()
    {
      const string heading1 = "Email";
      const string heading2 = "Given Name";
      const string heading3 = "Surname";
      const string heading4 = "Language Preference";
      const string heading5 = "Account Type";
      const string heading6 = "DateOfBirth";
      const string heading7 = "Date of Death";

      const string email = "charlie@example.com";
      const string givenName = "Charlie";
      const string surname = "Smith";
      const NestedTestTypes.Language language = NestedTestTypes.Language.French;
      const NestedTestTypes.AccountType accountType = NestedTestTypes.AccountType.Admin;
      var dob = DateTime.Now.Date;
      DateTime? dod = DateTime.Now.Date;

      var stepRow1 = new TableRow(null, GenerateTableCells(heading1, heading2, heading3, heading4, heading5, heading6, heading7));
      var stepRow2 = new TableRow(null, GenerateTableCells(email, givenName, surname, language.ToString(), accountType.ToString(), dob.ToString(), dod.ToString()));
      var stepDataTable = new DataTable(new[] {stepRow1, stepRow2});
      var table = new GherkinDataTable(stepDataTable);

      var result = table.GenerateInstance<NestedTestTypes.UserProfile>();

      result.Email.Should().Be(email);
      result.GivenName.Should().Be(givenName);
      result.Surname.Should().Be(surname);
      result.LanguagePreference.Should().Be(language);
      result.AccountType.Should().Be(accountType);
      result.DateOfBirth.Should().Be(dob);
      result.DateOfDeath.Should().Be(dod);
    }

    [Test]
    public void GenerateList_WhenMultipleRows_ReturnsMultipleRows()
    {
      const string heading = "shoes";
      const string value = "test";
      var stepRow1 = new TableRow(null, GenerateTableCells(heading));
      var stepRow2 = new TableRow(null, GenerateTableCells(value));
      var stepRow3 = new TableRow(null, GenerateTableCells(value));
      var stepDataTable = new DataTable(new [] { stepRow1, stepRow2, stepRow3 });
      var table = new GherkinDataTable(stepDataTable);

      var result = table.GenerateList<NestedTestTypes.GenerateInstanceSingleProperty>();

      result.Count.Should().Be(2);
    }

    [Test]
    public void GenerateList_WhenPassedOneRowOnBasicType_ReturnsSingleRow()
    {
      const string heading = "shoes";
      const string value = "test";
      var stepRow1 = new TableRow(null, GenerateTableCells(heading));
      var stepRow2 = new TableRow(null, GenerateTableCells(value));
      var stepDataTable = new DataTable(new [] { stepRow1, stepRow2 });
      var table = new GherkinDataTable(stepDataTable);

      var result = table.GenerateList<NestedTestTypes.GenerateInstanceSingleProperty>();

      result.Count.Should().Be(1);
    }

    private TableCell[] GenerateTableCells(params string[] cellText)
    {
      return cellText.Select(cell => new TableCell(null, cell)).ToArray();
    }

    private class NestedTestTypes
    {
      public class GenerateInstanceSingleProperty
      {
        public string Shoes { get; set; }
      }

      public class GenerateInstanceNameWithSpaces
      {
        public string NameWithSpaces { get; set; }
      }

      public class GenerateInstanceMultipleProperty
      {
        public string Shoes { get; set; }

        public string NotShoes { get; set; }
      }

      public class GenerateInstanceSingleNonStringProperty
      {
        public int Shoes { get; set; }
      }

      public class GenerateInstanceSingleNullStringProperty
      {
        public string Shoes { get; set; }
      }

      public class GenerateInstanceEnumProperty
      {
        public AccountType Shoes { get; set; }
      }

      public class UserProfile
      {
        public string Email { get; set; }
        public string GivenName{ get; set; }
        public string Surname { get; set; }
        public Language LanguagePreference { get; set; }
        public AccountType AccountType { get; set; }
        public DateTime DateOfBirth { get; set; }
        public DateTime? DateOfDeath { get; set; }
      }

      public enum Language
      {
        English,
        French
      }

      public enum AccountType
      {
        User,
        Moderator,
        Admin,
        Developer
      }
    }
  }
}
