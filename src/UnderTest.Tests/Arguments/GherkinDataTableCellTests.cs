using FluentAssertions;
using NUnit.Framework;
using UnderTest.Arguments;

namespace UnderTest.Tests.Arguments
{
  public class GherkinDataTableCellTests
  {
    [Test]
    public void Constructor_WhenPassedValue_ValuePropSet()
    {
      const string expected = "shoes";

      var result = new GherkinDataTableCell(expected);

      result.Value.Should().Be(expected);
    }
  }
}
