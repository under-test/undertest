using FluentAssertions;
using NUnit.Framework;
using UnderTest.Attributes;

namespace UnderTest.Tests.Attributes
{
  public class GivenAttributeTests
  {
    [Test]
    public void Constructor_WhenPassedValue_StepIsSet()
    {
      var expected = "shoes";

      var instance = new GivenAttribute(expected);

      instance.Step.Should().Be(expected);
    }
  }
}