using FluentAssertions;
using NUnit.Framework;
using UnderTest.Attributes;

namespace UnderTest.Tests.Attributes
{
  public class WhenAttributeTests
  {
    [Test]
    public void Constructor_WhenPassedValue_StepIsSet()
    {
      var expected = "shoes";

      var instance = new WhenAttribute(expected);

      instance.Step.Should().Be(expected);
    }
  }
}