using System;
using System.Collections.Generic;
using System.Diagnostics;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.Infrastructure;
using UnderTest.Reporting;

namespace UnderTest.Tests.Reporting
{
  public class UnderTestRunResultTests
  {
    [Test]
    public void Constructor_PassedNull_ThrowsNullArgException()
    {
      Action act = () => new UnderTestRunResult(null);

      act.Should().Throw<ArgumentNullException>();
    }


    [Test]
    public void Constructor_WhenCalled_PropertiesAreDefaulted()
    {
      var projectDetails = new TestProjectDetails()
        .SetProjectName("Test");

      var instance = new UnderTestRunResult(projectDetails);

      instance.UniqueRunId.Should().NotBeNullOrEmpty();
      instance.Features.Should().BeOfType<TestRunFeatureResultList>();
      instance.OverallResult.Should().Be(TestRunOverallResult.Inconclusive);
    }
  }
}
