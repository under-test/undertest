using FluentAssertions;
using NUnit.Framework;
using UnderTest.Reporting;

namespace UnderTest.Tests.Reporting
{
  public class TestRunFeatureResultTests
  {
    [Test]
    public void Constructor_WhenCalled_PropertiesAreNonNull()
    {
      var instance = new TestRunFeatureResult();

      instance.StrategyRuns.Should().NotBeNull();
    }
  }
}