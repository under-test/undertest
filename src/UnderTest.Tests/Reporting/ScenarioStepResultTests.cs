using System;
using FluentAssertions;
using Gherkin.Ast;
using Newtonsoft.Json;
using NUnit.Framework;
using UnderTest.Infrastructure;
using UnderTest.Reporting;

namespace UnderTest.Tests.Reporting
{
  public class ScenarioStepResultTests
  {
    [Test]
    public void Constructor_WhenCalled_PropertiesAreNotNull()
    {
      var instance = new ScenarioStepResult();

      instance.LogMessages.Should().NotBeNull();
      instance.Errors.Should().NotBeNull();
      instance.Warnings.Should().NotBeNull();
      instance.Screenshots.Should().NotBeNull();
      instance.BehaviorResults.Should().NotBeNull();
    }

    [Test]
    public void LoadStep_WhenPassedNull_ThrowsArgumentNullException()
    {
      var instance = new ScenarioStepResult();

      Action act = () => instance.LoadStep(null);

      act.Should().Throw<ArgumentNullException>();
    }

    [Test]
    public void LoadStep_PassedStep_PropertiesLoadedFromStep()
    {
      const string expectedKeyword = "Given";
      const string expectedText = "something happens";
      const int expectedLine = 1;
      const int expectedColumn = 2;


      var instance = new ScenarioStepResult();
      var step = new StepForProcessing(new Step(new Location(expectedLine, expectedColumn), expectedKeyword, expectedText, null));

      instance.LoadStep(step);

      instance.Step.Should().Be(expectedText);
      instance.Keyword.Should().Be(expectedKeyword);
      instance.Location.Line.Should().Be(expectedLine);
      instance.Location.Column.Should().Be(expectedColumn);
    }

    [Test]
    public void Class_Serialized_DeserializesSuccessfully()
    {
      var instance = new ScenarioStepResult();
      instance.BehaviorResults.Add("test", new TestObject { Shoes = 1 });

      var jsonSettings = new JsonSerializerSettings
      {
        TypeNameHandling = TypeNameHandling.All
      };
      var json = JsonConvert.SerializeObject(instance, jsonSettings);

      var result = JsonConvert.DeserializeObject<ScenarioStepResult>(json, jsonSettings);

      var testObject = (TestObject)result.BehaviorResults["test"];
      testObject.Shoes.Should().Be(1);
    }

    [Serializable]
    public class TestObject
    {
      public int Shoes { get; set; }
    }
  }
}
