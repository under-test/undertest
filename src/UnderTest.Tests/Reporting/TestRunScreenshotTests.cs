using FluentAssertions;
using NUnit.Framework;
using UnderTest.Reporting;

namespace UnderTest.Tests.Reporting
{
  public class TestRunScreenshotTests
  {
    [Test]
    public void Constructor_WhenCalled_CreatesInstance()
    {
      var instance = new TestRunScreenshot();

      instance.Should().NotBeNull();
    }
  }
}
