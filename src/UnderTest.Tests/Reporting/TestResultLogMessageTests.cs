using FluentAssertions;
using NUnit.Framework;
using UnderTest.Reporting;

namespace UnderTest.Tests.Reporting
{
  public class TestResultLogMessageTests
  {
    [Test]
    public void Constructor_WhenCalled_CreatesInstance()
    {
      var instance = new TestResultLogMessage();

      instance.Should().NotBeNull();
    }
  }
}
