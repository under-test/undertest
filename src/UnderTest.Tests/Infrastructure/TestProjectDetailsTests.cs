using FluentAssertions;
using NUnit.Framework;
using UnderTest.Infrastructure;

namespace UnderTest.Tests.Infrastructure
{
  public class TestProjectDetailsTests
  {
    [Test]
    public void Constructor_WhenCalled_DefaultsPropertiesToNull()
    {
      var instance = new TestProjectDetails();

      instance.ProjectName.Should().BeNull();
      instance.ProjectVersion.Should().BeNull();
    }

    [Test]
    public void SetProjectName_PassedName_SetsProjectName()
    {
      var expected = "Project";

      var instance = new TestProjectDetails();

      var result = instance.SetProjectName(expected);

      result.ProjectName.Should().Be(expected);
    }

    [Test]
    public void SetProjectVersion_PassedVersion_SetsProjectVersion()
    {
      var expected = "0.1.0";

      var instance = new TestProjectDetails();

      var result = instance.SetProjectVersion(expected);

      result.ProjectVersion.Should().Be(expected);
    }
  }
}
