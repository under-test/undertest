using System;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.Infrastructure;

namespace UnderTest.Tests.Infrastructure
{
  public class StepForProcessingTests
  {
    [Test]
    public void Constructor_WhenPassedNull_ThrowsArgumentNullException()
    {
      Action act = () => new StepForProcessing(null);

      act.Should().Throw<ArgumentNullException>();
    }
  }
}
