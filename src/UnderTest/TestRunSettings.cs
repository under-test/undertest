using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using JetBrains.Annotations;
using Serilog;
using Serilog.Sinks.SystemConsole.Themes;
using SimpleInjector;
using SimpleInjector.Lifestyles;
using UnderTest.Attributes;
using UnderTest.Configuration;
using UnderTest.Exceptions;
using UnderTest.Filtering;
using UnderTest.Infrastructure;
using UnderTest.Infrastructure.DI;
using UnderTest.Infrastructure.ExampleTableDataSources;
using UnderTest.Infrastructure.Preflight;
using UnderTest.Infrastructure.StepParameters;
using UnderTest.Locators;
using UnderTest.Strategies;

namespace UnderTest
{
  [PublicAPI]
  public class TestRunSettings : IUnderTestSettings, IDisposable
  {
    public TestRunSettings()
    {
      SetupReportFolderDefault();
      BuildLogger();
      SetupDefaultContainer();
    }

    public ReadOnlyCollection<Assembly> Assemblies { get; private set; } = new ReadOnlyCollection<Assembly>(new List<Assembly>());

    public string AssetsFolder
    {
      get
      {
        const string assetsFolderName = "assets";
        return Path.Combine(Directory.GetCurrentDirectory(), assetsFolderName);
      }
    }

    public string[] CommandLineArgs { get; internal set; }

    public UnderTestConfigSettings ConfigSettings => Container.GetInstance<UnderTestConfigSettings>();

    public Container Container { get; private set; }

    public ExecutionMode ExecutionMode { get; private set; } = ExecutionMode.Normal;

    public IFeatureHandlerLocator FeatureHandlerLocator => Container.GetInstance<IFeatureHandlerLocator>();

    public IGherkinLoader GherkinLoader  => Container.GetInstance<IGherkinLoader>();

    public ILogger Logger { get; private set; }

    public TestRunOutputSettings OutputSettings { get; private set; } = new TestRunOutputSettings();

    public ReadOnlyCollection<PostRunVerification> PostRunVerification { get; private set; } = new ReadOnlyCollection<PostRunVerification>(new List<PostRunVerification>());

    public IReportOutputWriter ReportOutputWriter  => Container.GetInstance<IReportOutputWriter>();

    public IStepLocator StepLocator  => Container.GetInstance<IStepLocator>();

    public ReadOnlyCollection<ITestStrategy> Strategies { get; private set; } = new ReadOnlyCollection<ITestStrategy>(new List<ITestStrategy>());

    public ReadOnlyCollection<TestFilter> TestFilters { get; private set; } = new ReadOnlyCollection<TestFilter>(new List<TestFilter>());

    public ReadOnlyCollection<TestRunEventHandlerBase> TestRunEventHandlers { get; private set; } = new ReadOnlyCollection<TestRunEventHandlerBase>(new List<TestRunEventHandlerBase>());

    public TypesCache TypesCache  => Container.GetInstance<TypesCache>();

    public string FeatureDirectory
    {
      get
      {
        if (!Assemblies.Any())
        {
          throw new NoFeatureAssembliesConfiguredException("You must call UnderTestSettings.AddAssembly before accessing `FeatureDirectory`");
        }

        var workingDirectory = Path.GetDirectoryName(Assemblies.First().Location) ?? throw new FileNotFoundException();
        var featureDirectory = Path.Combine(workingDirectory, "Features");
        if (!Directory.Exists(featureDirectory))
        {
          throw new DirectoryNotFoundException("The `Features` folder does not exist.");
        }

        return featureDirectory;
      }
    }

    public TestRunExecutionSettings ExecutionSettings { get; private set; } = new TestRunExecutionSettings();

    //public IList<StatusCheckerSettings> StatusCheckers { get; set; } = new List<StatusCheckerSettings>();

    public TestRunSettings AddAssembly([NotNull]Assembly assembly)
    {
      if (assembly == null)
      {
        throw new ArgumentNullException(nameof(assembly));
      }

      Assemblies = new ReadOnlyCollection<Assembly>(new List<Assembly>(Assemblies) {assembly});

      // collection registrations in the container
      Container.Collection.Register<IAmAConfigObjectMarker>(assembly);

      return this;
    }

    public TestRunSettings AddAssemblies([NotNull]IEnumerable<Assembly> assemblies)
    {
      if (assemblies == null)
      {
        throw new ArgumentNullException(nameof(assemblies));
      }

      var list = new List<Assembly>(Assemblies);
      list.AddRange(assemblies);
      Assemblies = new ReadOnlyCollection<Assembly>(list);

      return this;
    }

    public TestRunSettings AddPostRunVerification(PostRunVerification verification)
    {
      if (verification == null)
      {
        throw new ArgumentNullException(nameof(verification));
      }

      PostRunVerification = new ReadOnlyCollection<PostRunVerification>(new List<PostRunVerification>(PostRunVerification) { verification });

      return this;
    }

    public TestRunSettings AddStrategy([NotNull] ITestStrategy strategy)
    {
      if (strategy == null)
      {
        throw new ArgumentNullException(nameof(strategy));
      }

      Strategies = new ReadOnlyCollection<ITestStrategy>(new List<ITestStrategy>(Strategies) {strategy});

      return this;
    }

    public TestRunSettings AddTestFilter([NotNull] TestFilter filter)
    {
      if (filter == null)
      {
        throw new ArgumentNullException(nameof(filter));
      }

      TestFilters = new ReadOnlyCollection<TestFilter>(new List<TestFilter>(TestFilters) {filter});

      return this;
    }

    public TestRunSettings ConfigureContainer(Action<Container> customizeContainer)
    {
      customizeContainer(Container);

      return this;
    }

    public TestRunSettings ConfigureOutputSettings(Configure<TestRunOutputSettings> configure)
    {
      OutputSettings = configure.InvokeSafe(OutputSettings);

      BuildLogger();

      return this;
    }

    public bool Init()
    {
      try
      {
        return AddFeatureHandlersToContainer()
          .AddFeatureHandlerServicesToContainer()
          .AddTestRunEventHandlers()
          .BuildTypeCache()
          .HandleSettingDefaultsForUnsetValues()
          .RunPreflightChecks();
      }
      catch (DuplicateFeatureFileNameException e)
      {
        Console.WriteLine(e.Message);
        Log.Fatal(e.Message);
        return false;
      }
    }

    private TestRunSettings AddTestRunEventHandlers()
    {
      Container.RegisterInitializer<TestRunEventHandlerBase>(x => x.OnInit());

      Assemblies
        .SelectMany(x => x.GetExportedTypes())
        .Where(type => type.IsClass && !type.IsAbstract && type.IsSubclassOf(typeof(TestRunEventHandlerBase)) && type.GetCustomAttribute<TestRunEventHandlerAttribute>() != null)
        .ForEach(x =>
        {
          Container.Register(x, x, Lifestyle.Singleton);
          TestRunEventHandlers = new ReadOnlyCollection<TestRunEventHandlerBase>(new List<TestRunEventHandlerBase>(TestRunEventHandlers)
          {
            (TestRunEventHandlerBase)Container.GetInstance(x)
          });
        });

      return this;
    }

    public TestRunSettings SetFeatureHandlerLocator([NotNull] IFeatureHandlerLocator featureHandlerLocator)
    {
      Container.RegisterInstance(featureHandlerLocator ?? throw new ArgumentNullException(nameof(featureHandlerLocator)));

      return this;
    }

    public TestRunSettings SetExecutionMode(ExecutionMode executionMode)
    {
      ExecutionMode = executionMode;

      return this;
    }

    public TestRunSettings SetGherkinLoader([NotNull] IGherkinLoader gherkinLoader)
    {
      Container.RegisterInstance(gherkinLoader ?? throw new ArgumentNullException(nameof(gherkinLoader)));

      return this;
    }

    public TestRunSettings SetLogger([NotNull] ILogger logger)
    {
      Logger = logger;

      return this;
    }

    public TestRunSettings SetReportOutputWriter([NotNull] IReportOutputWriter reportOutputWriter)
    {
      Container.RegisterInstance(reportOutputWriter ?? throw new ArgumentNullException(nameof(reportOutputWriter)));

      return this;
    }

    public TestRunSettings SetStepLocator([NotNull] IStepLocator stepLocator)
    {
      if (stepLocator == null)
      {
        throw new ArgumentNullException(nameof(stepLocator));
      }

      Container.RegisterInstance(stepLocator ?? throw new ArgumentNullException(nameof(stepLocator)));

      return this;
    }

    public TestRunSettings WithExecutionSettings(Configure<TestRunExecutionSettings> executionSettings)
    {
      ExecutionSettings = executionSettings.InvokeSafe(ExecutionSettings);

      return this;
    }

    private TestRunSettings AddFeatureHandlerServicesToContainer()
    {
      Assemblies
        .SelectMany(x => x.GetExportedTypes()).Where(type => type.IsClass && !type.IsAbstract && type.IsSubclassOf(typeof(FeatureHandlerServiceBase)))
        .ForEach(x =>
          Container.Register(x, x, Lifestyle.Scoped));

      Container.RegisterInitializer<FeatureHandlerServiceBase>(x => x.OnInit());

      return this;
    }

    private TestRunSettings AddFeatureHandlersToContainer()
    {
      Assemblies
        .SelectMany(x => x.GetExportedTypes()).Where(type => type.IsClass && !type.IsAbstract && type.IsSubclassOf(typeof(FeatureHandler)))
        .ForEach(x =>
          Container.Register(x, x, Lifestyle.Scoped));

      Container.RegisterInitializer<FeatureHandler>(x =>
      {
        x.TestSettings = this;
      });

      return this;
    }

    private void BuildLogger()
    {
      Logger = new LoggerConfiguration()
        .MinimumLevel
          .Information()
        .ReadFrom
          .AppSettings()
        .WriteTo
          .Console(theme: AnsiConsoleTheme.Code)
        .WriteTo
          .File(Path.Combine(OutputSettings.ReportOutputFolder, "log.txt"))
        .CreateLogger();
    }

    private TestRunSettings BuildTypeCache()
    {
      TypesCache.BuildTypeCache(this);

      return this;
    }

    private TestRunSettings HandleSettingDefaultsForUnsetValues()
    {
      // do we need to default the file output path?
      if (Assemblies.Any() && string.IsNullOrWhiteSpace(OutputSettings.ReportOutputFolder))
      {
        SetupReportFolderDefault();
      }

      // set the default strategy if we need one
      if (!Strategies.Any())
      {
        AddStrategy(new DefaultGlobalStrategyTestStrategy());
      }

      return this;
    }

    private bool RunPreflightChecks()
    {
      try
      {
        Logger.Information("Running preflight checks");
        new CheckForPotentiallyMalformedFeatureHandlers()
          .Execute(this);
        new CheckForPotentiallyInvalidHandlersFeatureFilePaths()
          .Execute(this);
      }
      catch (Exception e)
      {
        Logger.Error($"Preflight check failed with the exception: {e.Message}");

        return false;
      }

      Logger.Information("Preflight checks complete");
      Logger.Information(string.Empty);

      return true;
    }

    private void SetupDefaultContainer()
    {
      Container = new Container();
      Container.Options.AllowOverridingRegistrations = true;
      Container.Options.PropertySelectionBehavior = new PropertyInjectionPropertySelectionBehavior();
      Container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();
      Container.Options.DefaultLifestyle = Lifestyle.Scoped;

      Container.Collection.Register<IExampleTableDataEnhancer>(new FileExampleDataTableEnhancer(this));
      Container.RegisterInstance<IGherkinLoader>(new FileSystemGherkinLoader(this));
      Container.RegisterInstance<IStepLocator>(new StepLocator(this));
      Container.RegisterInstance<IFeatureHandlerLocator>(new DefaultFeatureHandlerLocator(this));
      Container.RegisterInstance<IReportOutputWriter>(new ReportOutputWriter(this));
      Container.RegisterInstance<IExampleTableEnhancer>(new ExampleTableEnhancer(this));
      Container.RegisterInstance<IStepParameterExtractor>(new StepParameterExtractor(this));
      Container.RegisterInstance<IUnderTestSettings>(this);
      Container.RegisterInstance<UnderTestConfigSettings>(new UnderTestConfigSettings());
      Container.RegisterInstance<ILogger>(Logger);
      Container.RegisterInstance<TypesCache>(new TypesCache());
      Container.RegisterInstance<TestRunSettings>(this);
    }

    private void SetupReportFolderDefault()
    {
      var folder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "reports");
      folder = Path.Combine(folder, DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss"));
      OutputSettings.SetReportOutputFolder(folder);
    }

    public void Dispose()
    {
      foreach (var strategy in Strategies)
      {
        strategy?.Dispose();
      }

      Container.Dispose();
    }
  }
}
