using UnderTest.Infrastructure;

namespace UnderTest.Strategies
{
  public interface ITestStrategyEventHandlers: IHasLogger, IHasTestSettings
  {
    void BeforeTestRun(BeforeAfterTestRunContext context);
    void BeforeFeature(BeforeAfterFeatureContext context);

    void BeforeScenario(BeforeAfterScenarioContext context);

    void BeforeBackgroundKeywordGrouping(BeforeAfterKeywordGroupingContext context);

    void BeforeScenarioKeywordGrouping(BeforeAfterKeywordGroupingContext context);

    void BeforeStep(BeforeAfterStepContext context);

    void AfterFeature(BeforeAfterFeatureContext context);

    void AfterScenario(BeforeAfterScenarioContext context);

    void AfterScenarioKeywordGrouping(BeforeAfterKeywordGroupingContext context);

    void AfterBackgroundKeywordGrouping(BeforeAfterKeywordGroupingContext context);

    void AfterStep(BeforeAfterStepContext context);

    void AfterTestRun(BeforeAfterTestRunContext context);
  }
}
