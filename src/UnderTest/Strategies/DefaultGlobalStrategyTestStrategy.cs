using System.Collections.Generic;

namespace UnderTest.Strategies
{
  public class DefaultGlobalStrategyTestStrategy : ITestStrategy
  {
    public string Name { get; } = "Default Global Strategy";

    public IList<ITestStrategyEventHandlers> StrategyEventHandlers { get; } = new List<ITestStrategyEventHandlers>();

    public void Dispose()
    { }
  }
}
