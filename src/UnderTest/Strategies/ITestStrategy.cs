using System;
using System.Collections.Generic;

namespace UnderTest.Strategies
{
  public interface ITestStrategy : IDisposable
  {
    string Name { get; }

    IList<ITestStrategyEventHandlers> StrategyEventHandlers { get; }
  }
}
