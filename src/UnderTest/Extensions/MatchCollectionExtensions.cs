using System.Collections.Generic;
using JetBrains.Annotations;

// ReSharper disable once CheckNamespace
namespace System.Text.RegularExpressions
{
  [PublicAPI]
  public static class MatchCollectionExtensions
  {
    public static IList<Match> ToList(this MatchCollection instance)
    {
      var result = new List<Match>();
      if (instance == null)
      {
        return result;
      }

      foreach (Match match in instance)
      {
        result.Add(match);
      }

      return result;
    }
  }
}
