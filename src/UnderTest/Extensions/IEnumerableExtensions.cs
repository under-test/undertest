using System.Linq;
using Gherkin.Ast;
using UnderTest.Infrastructure;
using UnderTest.Reporting;

// ReSharper disable once CheckNamespace
namespace System.Collections.Generic
{
  public static class IEnumerableExtensions
  {
    public static void ForEach<T>(this IEnumerable<T> instance, Action<T> action)
    {
      foreach (var item in instance)
      {
        action(item);
      }
    }

    public static bool IncludesFutureVersionTag(this IEnumerable<Tag> instance, UnderTestConfigSettings configSettings)
    {
      return IncludesAnyTag(instance, configSettings.FutureVersionTagName);
    }

    public static bool IncludesIgnoreTag(this IEnumerable<Tag> instance, UnderTestConfigSettings configSettings)
    {
      return IncludesAnyTag(instance, configSettings.IgnoreTagName);
    }

    public static bool IncludesInconclusiveTag(this IEnumerable<Tag> instance, UnderTestConfigSettings configSettings)
    {
      return IncludesAnyTag(instance, configSettings.InconclusiveTagName);
    }

    public static bool IncludesWipTag(this IEnumerable<Tag> instance, UnderTestConfigSettings configSettings)
    {
      return IncludesAnyTag(instance, configSettings.WipTagName);
    }

    public static IList<StepForProcessing> ToStepsForProcessingList(this IEnumerable<Step> instance)
    {
      if (instance == null)
      {
        return new List<StepForProcessing>();
      }

      var previousAttributeKeyword = string.Empty;
      return instance.Select(x =>
      {
        if (IsAttributeKeyword(x.Keyword))
        {
          previousAttributeKeyword = x.Keyword;
        }

        var result = new StepForProcessing(x)
        {
          AttributeKeyword = previousAttributeKeyword
        };

        return result;
      }).ToList();
    }

    public static IList<ScenarioRunResult> ToWipTestResults(this IEnumerable<IHasLocation> instance)
    {
      return ToScenarioResultWithScenarioHandler(instance,
        (result, scenario, backgroundSteps) => result.AddScenarioAsFullyWip(scenario, backgroundSteps));
    }

    public static IList<ScenarioRunResult> ToIgnoredTestResults(this IEnumerable<IHasLocation> instance)
    {
      return ToScenarioResultWithScenarioHandler(instance,
        (result, scenario, backgroundSteps) => result.AddScenarioAsFullyIgnored(scenario, backgroundSteps));
    }

    private static bool IncludesAnyTag(IEnumerable<Tag> instance, string tagName)
    {
      return instance.Any(tag => tagName.Equals(tag.Name, StringComparison.OrdinalIgnoreCase));
    }

    private static bool IsAttributeKeyword(string keyword)
    {
      keyword = keyword.Trim().ToLowerInvariant();
      return "given".Equals(keyword) || "when".Equals(keyword)
                                     || "then".Equals(keyword);
    }

    private static IList<ScenarioRunResult> ToScenarioResultWithScenarioHandler(IEnumerable<IHasLocation> instance,
      Action<List<ScenarioRunResult>, Scenario, IEnumerable<StepForProcessing>> scenarioHandler)
    {
      var result = new List<ScenarioRunResult>();

      var backgroundSteps = new List<StepForProcessing>();
      foreach (var block in instance)
      {
        switch (block)
        {
          case Background background:
            backgroundSteps.AddRange(background.Steps.ToStepsForProcessingList());
            break;
          case Scenario scenario:
            scenarioHandler(result, scenario, backgroundSteps);
            break;
          case Rule rule:
            foreach (var ruleBlock in rule.Children)
            {
              switch (ruleBlock)
              {
                case Background background:
                  backgroundSteps.AddRange(background.Steps.ToStepsForProcessingList());
                  break;
                case Scenario scenario:
                  scenarioHandler(result, scenario, backgroundSteps);
                  break;
              }
            }

            break;
        }
      }

      return result;
    }

    public static void ForEachScenario(this IEnumerable<IHasLocation> instance, Action<Scenario> action)
    {
      void IterateChildren(IEnumerable parent)
      {
        foreach (var block in parent)
        {
          switch (block)
          {
            case Scenario scenario:
              action(scenario);
              break;
            case Rule rule:
              foreach (var ruleBlock in rule.Children)
              {
                switch (ruleBlock)
                {
                  case Scenario scenario:
                    action(scenario);
                    break;
                }
              }

              break;
          }
        }
      }
      
      IterateChildren(instance);
    }
  }
}
