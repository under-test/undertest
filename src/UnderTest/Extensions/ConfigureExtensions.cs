using JetBrains.Annotations;

// ReSharper disable once CheckNamespace
namespace UnderTest
{
  public static class ConfigureExtensions
  {
    public static T InvokeSafe<T>([CanBeNull] this Configure<T> configurator, T settings)
    {
      return (configurator ?? (x => x)).Invoke(settings);
    }
  }
}
