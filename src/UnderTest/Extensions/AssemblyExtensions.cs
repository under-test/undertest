using System.Diagnostics;
using System.Linq;
using JetBrains.Annotations;

// ReSharper disable once CheckNamespace
namespace System.Reflection
{
  public static class AssemblyExtensions
  {
    public static bool IsAssemblyDebugBuild(this Assembly assembly)
    {
      return assembly.GetCustomAttributes(false)
                     .OfType<DebuggableAttribute>()
                     .Any(da => da.IsJITTrackingEnabled);
    }

    [Pure]
    public static string GetSummaryText(this Assembly instance)
    {
      return $"{instance.GetInformationalVersion()}";
    }

    private static string GetInformationalVersion(this Assembly assembly)
    {
      return assembly.GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;
    }
  }
}
