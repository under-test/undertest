using System.Collections.Generic;
using UnderTest.Behaviors;

// extension methods go in the target type's namespace
// ReSharper disable once CheckNamespace
namespace UnderTest.Strategies
{
  public static class BeforeAfterStepContextExtensions
  {
    public static void TriggerAfterStepEvent(this BeforeAfterStepContext instance, ITestStrategy testStrategy, 
      IFeatureHandler handler, TestRunSettings testRunSettings)
    {
      var context = new StepExecutingContext
      {
        Feature = instance.Feature,
        Handler = handler as FeatureHandler,
        Scenario = instance.Scenario,
        Step = instance.Step
      };

      if (handler != null)
        testRunSettings.WrapMethodInBehaviors(handler.AfterStep, context, instance);
      
      testStrategy.StrategyEventHandlers.ForEach(x => 
        testRunSettings.WrapMethodInBehaviors(x.AfterStep, context, instance));
      
      testRunSettings.TestRunEventHandlers.ForEach(x => 
        testRunSettings.WrapMethodInBehaviors(x.AfterStep, context, instance));
    }

    public static void TriggerBeforeStepEvent(this BeforeAfterStepContext instance, ITestStrategy testStrategy, 
      IFeatureHandler handler, TestRunSettings testRunSettings)
    {
      var context = new StepExecutingContext
      {
        Feature = instance.Feature,
        Handler = handler as FeatureHandler,
        Scenario = instance.Scenario,
        Step = instance.Step
      };
      
      testRunSettings.TestRunEventHandlers.ForEach(x => 
        testRunSettings.WrapMethodInBehaviors(x.BeforeStep, context, instance));
      
      testStrategy.StrategyEventHandlers.ForEach(x => 
        testRunSettings.WrapMethodInBehaviors(x.BeforeStep, context, instance));

      if (handler != null)
        testRunSettings.WrapMethodInBehaviors(handler.BeforeStep, context, instance);
    }
  }
}
