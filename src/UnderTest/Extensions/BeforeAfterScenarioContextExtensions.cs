using System.Collections.Generic;
using UnderTest.Behaviors;
using UnderTest.Strategies;

// ReSharper disable once CheckNamespace
namespace UnderTest
{
  public static class BeforeAfterScenarioContextExtensions
  {
    public static void TriggerAfterScenarioEvent(this BeforeAfterScenarioContext instance, ITestStrategy testStrategy,
      IFeatureHandler handler, TestRunSettings testRunSettings)
    {
      var context = new StepExecutingContext
      {
        Feature = instance.Feature,
        Handler = handler as FeatureHandler,
        Scenario = instance.Scenario
      };

      if (handler != null)
        testRunSettings.WrapMethodInBehaviors(handler.AfterScenario, context, instance);

      testStrategy.StrategyEventHandlers.ForEach(x => 
        testRunSettings.WrapMethodInBehaviors(x.AfterScenario, context, instance));
      
      testRunSettings.TestRunEventHandlers.ForEach(x => 
        testRunSettings.WrapMethodInBehaviors(x.AfterScenario, context, instance));
    }

    public static void TriggerBeforeScenarioEvent(this BeforeAfterScenarioContext instance, ITestStrategy testStrategy,
      IFeatureHandler handler, TestRunSettings testRunSettings)
    {
      var context = new StepExecutingContext
      {
        Feature = instance.Feature,
        Handler = handler as FeatureHandler,
        Scenario = instance.Scenario
      };

      testRunSettings.TestRunEventHandlers.ForEach(x => 
        testRunSettings.WrapMethodInBehaviors(x.BeforeScenario, context, instance));
      
      testStrategy.StrategyEventHandlers.ForEach(x => 
        testRunSettings.WrapMethodInBehaviors(x.BeforeScenario, context, instance));
      
      if (handler != null)
        testRunSettings.WrapMethodInBehaviors(handler.BeforeScenario, context, instance);
    }
  }
}
