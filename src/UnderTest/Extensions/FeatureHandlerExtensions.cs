using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Gherkin.Ast;
using JetBrains.Annotations;
using UnderTest.Attributes;
using UnderTest.Exceptions;

namespace UnderTest
{
  [PublicAPI]
  public static class FeatureHandlerExtensions
  {
    public static async Task Executes<T>(this FeatureHandler instance, [NotNull]StepsToExecute<T> executor) where T: FeatureHandlerServiceBase
    {
      var service = instance.TestSettings.Container.GetInstance<T>();
      await executor.Invoke(service);
    }

    public static void ExecutesSync<T>(this FeatureHandler instance, [NotNull]StepsToExecuteSync<T> executor) where T: FeatureHandlerServiceBase
    {
      var service = instance.TestSettings.Container.GetInstance<T>();
      executor.Invoke(service);
    }
    
    internal static bool HasExpectedFailureAttribute(this IFeatureHandler instance)
    {
      var attributes = instance?.GetType().GetCustomAttributes(typeof(ExpectedFailureAttribute), true);
      return attributes != null && attributes.Any();
    }

    internal static (bool, MethodInfo) HasScenarioAttribute(this IFeatureHandler instance, Scenario scenario)
    {
      var methods = instance?.GetType().GetMethods().Where(x =>
        {
          return x
            .GetCustomAttributes(typeof(ScenarioAttribute), true)
            .Cast<ScenarioAttribute>()
            .Any(method => method.Name == scenario?.Name);
        })
        .ToList();

      return (methods.Any(), methods.FirstOrDefault());
    }

    internal static void ThrowIfInconclusive(this IFeatureHandler instance)
    {
      var attributes = instance?.GetType().GetCustomAttributes(typeof(InconclusiveAttribute), false);
      if (attributes != null && attributes.Any())
      {
        var message =
          $"FeatureHandler marked inconclusive by attribute with message \"{attributes.Cast<InconclusiveAttribute>().Select(x => x.Message).FirstOrDefault() ?? string.Empty}\"";
        throw new FeatureHandlerMarkedAsInconclusiveException(message) {Handler = instance};
      }
    }
  }
}
