using System.Collections.Generic;
using UnderTest.Behaviors;

// extensions go in the target type namespace
// ReSharper disable once CheckNamespace
namespace UnderTest.Strategies
{
  public static class BeforeAfterFeatureContextExtensions
  {
    public static void TriggerAfterFeatureEvent(this BeforeAfterFeatureContext instance, ITestStrategy testStrategy, IFeatureHandler handler, TestRunSettings testRunSettings)
    {
      var context = new StepExecutingContext
      {
        Feature = instance.Feature,
        Handler = handler as FeatureHandler
      };

      if (handler != null)
        testRunSettings.WrapMethodInBehaviors(handler.AfterFeature, context, instance);

      testStrategy.StrategyEventHandlers.ForEach(x => 
        testRunSettings.WrapMethodInBehaviors(x.AfterFeature, context, instance));
      
      testRunSettings.TestRunEventHandlers.ForEach(x => 
        testRunSettings.WrapMethodInBehaviors(x.AfterFeature, context, instance));
    }

    public static void TriggerBeforeFeatureEvent(this BeforeAfterFeatureContext instance, ITestStrategy testStrategy,
      IFeatureHandler handler, TestRunSettings testRunSettings)
    {
      var context = new StepExecutingContext
      {
        Feature = instance.Feature,
        Handler = handler as FeatureHandler
      };

      testRunSettings.TestRunEventHandlers.ForEach(x => 
        testRunSettings.WrapMethodInBehaviors(x.BeforeFeature, context, instance));
      
      testStrategy.StrategyEventHandlers.ForEach(x =>
        testRunSettings.WrapMethodInBehaviors(x.BeforeFeature, context, instance));

      if (handler == null) return;

      testRunSettings.WrapMethodInBehaviors(handler.BeforeFeature, context, instance);
    }
  }
}
