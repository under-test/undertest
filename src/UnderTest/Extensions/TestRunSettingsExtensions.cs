using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Gherkin.Ast;
using JetBrains.Annotations;
using UnderTest.Attributes;
using UnderTest.Behaviors;
using UnderTest.Filtering;

namespace UnderTest
{
  public static class TestRunSettingsExtensions
  {
    internal static List<Type> GetConfiguredFeatureHandlers(this TestRunSettings instance)
    {
      return instance
        .Assemblies
        .SelectMany(x => x.GetTypes())
        .Where(type => type.IsDefined(typeof(HandlesFeatureAttribute), false))
        .ToList();
    }

    [PublicAPI]
    public static TestRunSettings OnlyRunTheseTags(this TestRunSettings instance, params string[] tags)
    {
      instance.AddTestFilter(context =>
      {
        var contextTags = GetAllTagsForContext(context);

        return !ContainsTags(contextTags, tags);
      });

      return instance;
    }

    [PublicAPI]
    public static TestRunSettings DoNotRunTheseTags(this TestRunSettings instance, params string[] tagsThatIndicateIgnore)
    {
      instance.AddTestFilter(
        context =>
        {
          bool IsFeature() => context.Scenario == null && context.SpecificExample == null;
          bool IsScenario() => context.Scenario != null && context.SpecificExample == null;
          bool IsScenarioExample() => context.Scenario != null && context.SpecificExample != null;

          // so:
          //   if we have a feature with an ignore tag -> ignore the feature
          //   if we have a scenario with an ignore tag -> ignore the scenario
          //   if we have a scenario example table with an ignore tag -> ignore the example
          var tagsToEvaluate = new List<Tag>();
          if (IsFeature())
          {
            tagsToEvaluate.AddRange(context.FeatureTags);
          }
          else if (IsScenario())
          {
            tagsToEvaluate.AddRange(context.FeatureTags);
            tagsToEvaluate.AddRange(context.Scenario!.Tags);
          }
          else if (IsScenarioExample())
          {
            tagsToEvaluate.AddRange(context.FeatureTags);
            tagsToEvaluate.AddRange(context.Scenario!.Tags);
            tagsToEvaluate.AddRange(context.SpecificExample!.Tags);
          }
          
          if (!tagsToEvaluate.Any())
          {
            return false;
          }

          return ContainsTags(tagsToEvaluate, tagsThatIndicateIgnore);
        });

      return instance;
    }

    private static bool ContainsTags(IEnumerable<Tag> tagsToCheck, params string[] theTags)
    {
      if (tagsToCheck == null)
      {
        return false;
      }

      return (from tag in tagsToCheck from t in theTags where string.Equals(tag.Name.TrimStart('@'), t.TrimStart('@'), StringComparison.InvariantCultureIgnoreCase) select tag).Any();
    }

    internal static void WrapMethodInBehaviors<T>(this TestRunSettings testRunSettings, Action<T> method, 
      StepExecutingContext stepContext, T featureContext)
    {
      MethodInfo GetBeforeAfterFeatureContextMethodInfo<T>(Action<T> a) => a.Method;

      var behaviors = GetBeforeAfterFeatureContextMethodInfo(method).GetBehaviors(testRunSettings);
      behaviors.ForEach(x => x.OnStepExecuting(stepContext));
      method(featureContext);
      behaviors.ForEach(x => x.OnStepExecuted(stepContext));
    }

    private static List<Tag> GetAllTagsForContext(TestFilterContext context)
    {
      var result = new List<Tag>();
      
      // if this is for the feature, we want all tags for the feature and children.
      //    we do this because we are filtering the entire feature and child tags may opt us into/out-of the filter
      // if this is for a scenario, we want all feature and scenario tags.
      //    we do this because we are filtering the entire scenario and child tags may opt us into/out-of the filter
      // if this is for a specific scenario, we want all feature, scenario and example tags.
      if (context.Scenario == null && context.SpecificExample == null)
      {
        result.AddRange(context.AllFeatureTags);
        return result;
      }

      if (context.Scenario != null)
      {
        result.AddRange(context.Scenario.Tags);
        
        if (context.SpecificExample != null)
          result.AddRange(context.SpecificExample.Tags);
        else
          result.AddRange(context.Scenario.Examples.SelectMany(x =>x.Tags));
      }

      return result;
    }
  }
}
