using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using UnderTest;
using UnderTest.Arguments;
using UnderTest.Attributes;
using UnderTest.Behaviors;
using UnderTest.Exceptions;
using UnderTest.Infrastructure;
using UnderTest.Infrastructure.Binding;
using UnderTest.Locators;
using UnderTest.Reporting;

// ReSharper disable once CheckNamespace
namespace Gherkin.Ast
{
  public static class StepForProcessingExtensions
  {
    internal static void Execute(this StepForProcessing instance, TestRunSettings runSettings,
      ScenarioStepResult stepResult,
      IFeatureHandler handler, BeforeAfterStepContext stepContext, ScenarioRunResult scenarioRunResult)
    {
      var stopWatch = Stopwatch.StartNew();
      try
      {
        var method = runSettings.StepLocator.LocateStep(instance, handler);
        if (method == null)
        {
          var message = $"Unable to locate method for step '{instance.Keyword}{instance.Text}'";
          runSettings.Logger.Warning(message);
          throw new MissingStepBindingException(message)
          {
            StepText = instance.Text,
            StepKeyword = instance.Keyword
          };
        }

        var methodToCall = method.MethodToCall;
        methodToCall.ThrowIfInconclusive();

        scenarioRunResult.ExpectedFailure |= methodToCall.HasExpectedFailureAttribute();

        stepResult.HandlingMethod = methodToCall.Name;

        var containerInstance = runSettings.TypesCache.LocateInstanceForType(methodToCall.ReflectedType, runSettings);

        CallMethodAndPassInParameters(instance, methodToCall, containerInstance, method, stepResult, runSettings, handler, stepContext);

        stepResult.ScenarioStepResultType = ScenarioStepResultType.Pass;
      }
      finally
      {
        stepResult.Duration = stopWatch.Elapsed;
      }
    }

    private static void CallMethodAndPassInParameters(StepForProcessing instance, MethodInfo methodToCall,
      object containerInstance, StepLocatorMatch method, ScenarioStepResult result,
      TestRunSettings testRunSettings, IFeatureHandler featureHandler, BeforeAfterStepContext stepContext)
    {
      try
      {
        var stepBehaviors = methodToCall.GetBehaviors(testRunSettings);

        var stepExecutionContext = BuildStepExecutionContext(featureHandler, stepContext);
        CallOnStepExecutingBehaviors(stepBehaviors, stepExecutionContext);

        var parameters = methodToCall.GetParameters();
        if (parameters.Length == 0)
        {
          methodToCall
            .Invoke(containerInstance, null)
            .WaitIfTask();
        }
        else
        {
          var parametersToPass = new List<object>();

          if (method.StepParameters != null)
          {
            parametersToPass.AddRange(
              method.StepParameters
                .Select(x => instance.ExampleTableRow?.ApplyRowToStepText(x.ParameterText) ?? x.ParameterText));
          }

          if (instance.Argument != null)
          {
            switch (instance.Argument)
            {
              case DataTable table:
                result.DataTable = table;
                var gherkinDataTable = new GherkinDataTable(table, instance.ExampleTableRow);
                testRunSettings.Logger.Information($"Preparing to pass DataTable:\n{gherkinDataTable}");
                parametersToPass.Add(gherkinDataTable);
                break;
              case DocString docString:
                result.DocString = docString;
                var gherkinDocString = new GherkinDocString(docString.ContentType, docString.Content);
                testRunSettings.Logger.Information($"Preparing to pass DocString:\n{gherkinDocString}");
                parametersToPass.Add(gherkinDocString);
                break;
            }
          }

          ApplyParameterTransforms(methodToCall, parameters, parametersToPass, testRunSettings);
          result.StepParameters = parametersToPass;

          // todo - it would be ideal to check if we have the correct number of parameters here
          //        to do this, we need to handle out/ref/params/optional/default etc

          methodToCall
            .Invoke(containerInstance, BindingFlags.InvokeMethod | BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static,
              new UnderTestInvokeBinder(), parametersToPass.ToArray(), CultureInfo.InvariantCulture)
            .WaitIfTask();
        }

        CallOnStepExecutedBehaviors(stepBehaviors, stepExecutionContext);
      }
      catch (Exception e)
      {
        result.LogMessages.Add(new TestResultLogMessage
        {
          Type = "Error",
          Message = $"Failed to execute step {instance.Keyword}{instance.Text} with error {e.Message}",
          StackTrace = e.StackTrace
        });
        result.ScenarioStepResultType = ScenarioStepResultType.Failed;

        throw;
      }
    }

    private static void CallOnStepExecutedBehaviors(List<BehaviorAttribute> stepBehaviors, StepExecutingContext stepExecutionContext)
    {
      stepBehaviors.ForEach(x => x.OnStepExecuted(stepExecutionContext));
    }

    private static void CallOnStepExecutingBehaviors(List<BehaviorAttribute> stepBehaviors, StepExecutingContext stepExecutionContext)
    {
      stepBehaviors.ForEach(x => x.OnStepExecuting(stepExecutionContext));
    }

    private static StepExecutingContext BuildStepExecutionContext(IFeatureHandler featureHandler,
      BeforeAfterStepContext stepContext)
    {
      var stepExecutionContext = new StepExecutingContext
      {
        ScenarioStepResult = stepContext.ScenarioStepResult,
        Handler = featureHandler as FeatureHandler,
        Feature = stepContext.Feature,
        Scenario = stepContext.Scenario,
        Step = stepContext.Step
      };
      return stepExecutionContext;
    }

    private static void ApplyParameterTransforms(MethodInfo method, ParameterInfo[] parameters,
      IList<object> parametersToPass, TestRunSettings testRunSettings)
    {
      for (var index = 0; index < parameters.Length; index++)
      {
        if (parametersToPass.Count <= index)
        {
          throw new StepCallWithParameterCountMismatchException($"More parameters being passed({parametersToPass.Count}) than expected({parameters.Length})")
          {
            MethodName = method.Name,
            IntendedParameterCount = parametersToPass.Count,
            MethodCallSignature = method.ToString()
          };
        }

        var param = parameters[index];

        var paramTransform = param
          .GetCustomAttributes(typeof(StepParamAttribute), true);

        var attrs = paramTransform.ToList();
        if (!attrs.Any()) continue;

        // the attribute has AllowMultiple=false - so we can just call first.
        var attr = attrs
          .First()
          .ApplyPropertyInjection(testRunSettings.Container);

        parametersToPass[index] = ((StepParamAttribute)attr).Transform(parametersToPass[index]);
      }
    }
  }
}
