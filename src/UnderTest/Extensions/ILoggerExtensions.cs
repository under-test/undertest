using System;
using System.Collections.Generic;
using System.Linq;
using Serilog.Core;
using UnderTest;
using UnderTest.Exceptions;
using UnderTest.Infrastructure;
using UnderTest.Reporting;

// ReSharper disable once CheckNamespace
namespace Serilog
{
  // ReSharper disable once InconsistentNaming
  public static class ILoggerExtensions
  {
    public static void OutputTestFilterExceptionHelp(this ILogger instance, Exception e, TestRunSettings settings)
    {
      instance.Fatal("A TestFilter threw an exception.  This has caused UnderTest to stop processing.");
      instance.Fatal("This is often caused by not checking if TestFilterContext.Scenarios is null before using it in a test filter.");
      instance.Fatal(string.Empty);
      instance.Fatal("The exception was:");
      e?.InnerException?.OutputException(settings, "  ");
      instance.Fatal(string.Empty);
    }

    public static void OutputNoFeatureAssembliesConfiguredHelp(this ILogger instance)
    {
      // todo - include a link to some docs on how to get setup - https://gitlab.com/under-test/undertest/issues/103
      instance.Fatal("No assemblies configured for execution.  We can't test what we don't know about.");
      instance.Fatal(string.Empty);
      instance.Fatal("            var result = new UnderTestRunner()");
      instance.Fatal("               .WithProjectDetails(x => x");
      instance.Fatal(@"                 .SetProjectName(""UnderTest Example Test Suite""))");
      instance.Fatal("               .WithTestSettings(x => x");
      instance.Fatal("                 .AddAssemblies(typeof(Program).Assembly))");
      instance.Fatal("               .Execute();");
      instance.Fatal(string.Empty);
    }

    public static void OutputInvalidGherkinFailureHelp(this ILogger instance, UnderTestFailedToParseFeatureFilesException err)
    {
      instance.Fatal("Invalid gherkin located in the `Features` folder.");
      instance.Fatal("We cannot execute invalid gherkin. Details are:");
      instance.Fatal(string.Empty);

      if (err.ParseException == null) return;

      instance.Fatal($"The error was found in '{err.Filename}'");
      instance.Fatal(err.ParseException.Message);
      instance.Fatal(string.Empty);
    }

    public static void LogInvalidFeatures(this ILogger instance, IList<UnderTestFailedToParseFeatureFilesException> invalidFiles)
    {
      if (!invalidFiles.Any())
      {
        return;
      }

      instance.Fatal("Invalid gherkin located in the `Features` folder.");
      instance.Fatal("We cannot execute invalid gherkin. The invalid files are:");

      invalidFiles.ForEach(x =>
      {
        instance.Fatal($"  Unable to parse feature `{x.Filename}` - please use FeatureLint for more information.");
      });

      instance.Fatal(string.Empty);
    }

    public static void LogPreRunInformation(this ILogger instance,
      UnderTestRunResult result,
      TestRunSettings runSettings, FeaturesContainer features)
    {
      instance.Information(string.Empty);
      instance.Information($"UnderTest version {result.UnderTestVersion}");

      instance.Information(string.Empty);
      instance.Information($"Project details:");
      instance.Information($"----------------");
      instance.Information($"  Project name: {result.ProjectName}");

      instance.Information(string.Empty);
      instance.Information("Test settings:");
      instance.Information($"-------------");

      instance.Information("  Assemblies:");
      if (!runSettings.Assemblies.Any())
      {
        instance.Information("    (none)");
      }

      foreach (var assembly in runSettings.Assemblies)
      {
        instance.Information($"    {assembly.Location}");
      }

      instance.Information("  Strategies:");
      foreach (var strategy in runSettings.Strategies)
      {
        instance.Information($"    {strategy.Name}");
      }

      instance.Information($"  Step Locator: {runSettings.StepLocator.Name}");
      instance.Information($"  Container Locator: {runSettings.FeatureHandlerLocator.Name}\n");
      instance.Information($"  Execution Mode: {runSettings.ExecutionMode}\n");

      instance.Information($"Features: ");
      instance.Information($"---------");
      instance.Information($"  Testable: {features.Testable.Count}");
      instance.Information($"       Wip: {features.Wip.Count}");
      instance.Information($"   Ignored: {features.Ignored.Count}\n");
    }

    public static void LogTestResults(this ILogger instance, UnderTestRunResult result, TestRunSettings runSettings)
    {
      instance.Information(string.Empty);
      instance.Information($"Test Run Results:");
      instance.Information($"----------------");

      instance.Information($"  Overall result: {result.OverallResult.ToString()}");
      instance.Information($"  Features processed: {result.Features.Count}");
      instance.Information($"              Passed: {result.Features.Passed}");
      instance.Information($"              Failed: {result.Features.Failed}{SummarizeExpectedFailures(result)}");
      instance.Information($"        Inconclusive: {result.Features.Inconclusive}");
      instance.Information($"                 Wip: {result.Features.Wip}");
      instance.Information($"             Skipped: {result.Features.Skipped}");
      LogFeatureResultsByTypeVerbose(instance, result);
      instance.Information(string.Empty);
      instance.Information($"  Scenarios processed: {result.Scenarios.Total}");
      instance.Information($"               Passed: {result.Scenarios.Passed}");
      instance.Information($"               Failed: {result.Scenarios.Failed}");
      instance.Information($"         Inconclusive: {result.Scenarios.Inconclusive}");
      instance.Information($"                  Wip: {result.Scenarios.Wip}");
      instance.Information($"              Skipped: {result.Scenarios.Skipped}");
      instance.Information($"       Execution time: {result.ExecutionTime}");
      instance.Information($"           Start time: {result.ExecutionTime.StartTime} (UTC)");
      instance.Information($"             End time: {result.ExecutionTime.EndTime} (UTC)");

      instance.Information(string.Empty);
      instance.Information($"Results saved to: {runSettings.OutputSettings.ReportOutputFilePath}");
    }

    private static string SummarizeExpectedFailures(UnderTestRunResult result)
    {
      if (result.Features.ExpectedFailure == 0)
      {
        return string.Empty;
      }

      return $" (Expected {result.Features.ExpectedFailure})";
    }

    private static void LogFeatureResultsByTypeVerbose(ILogger instance, UnderTestRunResult result)
    {
      instance.Debug(string.Empty);
      instance.Debug($"  Feature breakdown-->");
      foreach (var feature in result.Features)
      {
        var strategyResult = feature.StrategyRuns.First();
        instance.Debug($"    -----------------------");
        instance.Debug($"    Feature: {feature.Name}:");
        OutputFeatureResultDetailForScenarioTestResult(instance, feature, $"            Scenarios Passed:", ScenarioRunResultType.Pass, strategyResult.ScenarioTestResults);
        OutputFeatureResultDetailForScenarioTestResult(instance, feature, $"            Scenarios Failed:", ScenarioRunResultType.Failed, strategyResult.ScenarioTestResults);
        OutputFeatureResultDetailForScenarioTestResult(instance, feature, $"      Scenarios Inconclusive:", ScenarioRunResultType.Inconclusive, strategyResult.ScenarioTestResults);
        OutputFeatureResultDetailForScenarioTestResult(instance, feature, $"               Scenarios Wip:", ScenarioRunResultType.Wip, strategyResult.ScenarioTestResults);
        OutputFeatureResultDetailForScenarioTestResult(instance, feature, $"           Scenarios Skipped:", ScenarioRunResultType.Ignored, strategyResult.ScenarioTestResults);
      }
      instance.Debug($"    -----------------------");
    }

    private static void OutputFeatureResultDetailForScenarioTestResult(ILogger instance, TestRunFeatureResult feature, string header, ScenarioRunResultType resultType, IList<ScenarioRunResult> allResults)
    {
      var results = allResults.Where(x => x.Result == resultType).ToList();
      if (!results.Any())
      {
        return;
      }

      instance.Debug(messageTemplate: $"{header} {results.Count()}");
      foreach (var passedScenario in results)
      {
        instance.Verbose($"               {passedScenario.ScenarioName}");
      }
    }

    public static void PrintHeader(this ILogger instance, string title)
    {
      instance.Information($"╔{"═".Repeat(title.Length + 2)}╗");
      instance.Information($"║ {title} ║");
      instance.Information($"╚{"═".Repeat(title.Length + 2)}╝");
    }
  }
}
