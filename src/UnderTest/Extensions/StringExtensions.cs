using System.Collections.Generic;
using System.IO;
using System.Linq;
using Gherkin.Ast;

namespace System
{
  public static class StringExtensions
  {
    public static string Repeat(this string instance, int repeatCount)
    {
      return string.Concat(Enumerable.Repeat(instance, repeatCount));
    }

    public static string NormalizeFilePath(this string instance)
    {
      if (instance == null)
      {
        throw new ArgumentNullException(nameof(instance));
      }

      return instance
        .ToLowerInvariant()
        .Replace(@"\", "/");
    }
  }
}
