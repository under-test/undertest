using System.Collections.Generic;
using JetBrains.Annotations;

// ReSharper disable once CheckNamespace
namespace System.Text.RegularExpressions
{
  [PublicAPI]
  public static class GroupCollectionExtensions
  {
    public static IList<Group> ToList(this GroupCollection instance)
    {
      var result = new List<Group>();
      if (instance == null)
      {
        return result;
      }

      foreach (Group @group in instance)
      {
        result.Add(@group);
      }

      return result;
    }
  }
}
