using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;
using JetBrains.Annotations;
using SimpleInjector;
using UnderTest.Attributes;

// ReSharper disable once CheckNamespace
namespace System
{
  [PublicAPI]
  public static class ObjectExtensions
  {
    public static T ApplyPropertyInjection<T>(this T instance, Container container)
    {
      instance
        .GetType()
        .GetProperties()
        .Where(x => 
          x.GetCustomAttributes<PropertyInjectedAttribute>(true).Any()
          ||
          x.GetCustomAttributes<InjectAttribute>(true).Any()
          )
        .Where(x => x.CanWrite)
        .ForEach(x => x.SetValue(instance, container.GetInstance(x.PropertyType)));

      return instance;
    }

    [Pure]
    public static T Clone<T>(this T obj)
    {
      using var stream = new MemoryStream();
      var serializer = new BinaryFormatter();
      serializer.Serialize(stream, obj);
      stream.Seek(offset: 0, loc: SeekOrigin.Begin);

      return (T)serializer.Deserialize(stream);
    }

    public static void ThrowIfNull(this object instance, string name)
    {
      if (instance == null)
      {
        throw new ArgumentNullException(name);
      }
    }

    public static void WaitIfTask(this object instance)
    {
      if (instance is Task task)
      {
        task.GetAwaiter().GetResult();
      }
    }
  }
}
