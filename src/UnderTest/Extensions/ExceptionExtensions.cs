using System.Reflection;
using Serilog;
using UnderTest;

// ReSharper disable once CheckNamespace
namespace System
{
  public static class ExceptionExtensions
  {
    public static void OutputException(this Exception exception, IUnderTestSettings testSettings, string prefix = null)
    {
      switch (exception)
      {
        case AggregateException ex:
          var exceptions = ex.Flatten().InnerExceptions;
          for (var index = 1; index < exceptions.Count; index++)
          {
            var e = exceptions[index];
            OutputException(e, testSettings, $"#{index + 1}/{exceptions.Count}: ");
          }
          break;
        case TargetInvocationException ex:
          OutputException(ex.InnerException, testSettings);
          break;
        case TypeInitializationException ex:
          OutputException(ex.InnerException, testSettings);
          break;
        default:
          testSettings.Logger.Error($"{prefix}{exception.Message}{exception.StackTrace}{Environment.NewLine}");
          break;
      }
    }
  }
}
