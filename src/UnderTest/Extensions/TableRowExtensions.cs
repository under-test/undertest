using JetBrains.Annotations;

// ReSharper disable once CheckNamespace
namespace Gherkin.Ast
{
  public static class TableRowExtensions
  {
    [Pure]
    public static string ToDisplayOfRowValues(this TableRow instance)
    {
      var result = string.Empty;
      if (instance == null)
      {
        return result;
      }

      foreach (var cell in instance.Cells)
      {
        if (!string.IsNullOrWhiteSpace(result))
        {
          result += " - ";
        }

        result += cell.Value;
      }

      return result;
    }
  }
}
