using System;
using System.Collections.Generic;

namespace UnderTest.Infrastructure.CrossPlatform
{
  public class CrossPlatformFilePathEqualityComparer : IEqualityComparer<string>
  {
    public bool Equals(string x, string y)
    {
      if (x == null) throw new ArgumentNullException(nameof(x));
      if (y == null) throw new ArgumentNullException(nameof(y));

      var path1 = x.NormalizeFilePath();
      var path2 = y.NormalizeFilePath();

      return path1.Equals(path2, StringComparison.CurrentCultureIgnoreCase);
    }

    public int GetHashCode(string obj)
    {
      return obj.NormalizeFilePath().GetHashCode();
    }
  }
}
