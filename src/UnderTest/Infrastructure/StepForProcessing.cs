using System;
using System.Collections.Generic;
using Gherkin.Ast;
using JetBrains.Annotations;

namespace UnderTest.Infrastructure
{
  [PublicAPI]
  [Serializable]
  public class StepForProcessing
  {
    public StepForProcessing(Step step)
    {
      if (step == null)
      {
        throw new ArgumentNullException(nameof(step));
      }

      Location = step.Location;
      Keyword = step.Keyword;
      AttributeKeyword = step.Keyword;
      Text = step.Text;
      Argument = step.Argument;
    }

    public Location Location { get; private set; }

    public string Keyword { get; private set; }

    public string AttributeKeyword { get; set; }

    public string Text { get; private set; }

    public StepArgument Argument { get; private set; }

    public ExampleTableRow ExampleTableRow { get; set; }
  }
}
