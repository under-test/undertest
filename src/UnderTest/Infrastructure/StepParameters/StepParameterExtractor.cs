using System;
using System.Text.RegularExpressions;

namespace UnderTest.Infrastructure.StepParameters
{
  public class StepParameterExtractor : IStepParameterExtractor
  {
    public StepParameterExtractor(TestRunSettings testRunSettings)
    {
      TestSettings = testRunSettings;
    }

    public IUnderTestSettings TestSettings { get; }

    public StepParameterList Extract(string input)
    {
      if (input == null)
      {
        throw new ArgumentNullException(nameof(input));
      }

      var pattern = @"<[^>]*>";
      var matches = Regex.Matches(input, pattern, RegexOptions.IgnoreCase);
      var result = new StepParameterList();
      foreach (Match match in matches)
      {
        result.Add(new StepParameter {ParameterText = match.Value});
      }

      return result;
    }
  }
}
