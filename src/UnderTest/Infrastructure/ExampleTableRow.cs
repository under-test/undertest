using System.Collections.Generic;
using System.Linq;
using Gherkin.Ast;
using JetBrains.Annotations;

namespace UnderTest.Infrastructure
{
  [PublicAPI]
  public class ExampleTableRow
  {
    public ExampleTableRow(IEnumerable<TableCell> tableHeaderCells, IEnumerable<TableCell> valuesRowCells)
    {
      HeaderCells = tableHeaderCells;
      Row = valuesRowCells;
    }

    public IEnumerable<TableCell> HeaderCells { get; }

    public IEnumerable<TableCell> Row { get; }

    public string ApplyRowToStepText(string inputString)
    {
      if (HeaderCells == null || Row == null)
      {
        return inputString;
      }

      var result = inputString;
      var col = 0;
      foreach (var variableCell in HeaderCells)
      {
        var valueCell = Row.ElementAt(col++);
        var header = variableCell.Value;
        var value = valueCell.Value;

        result = result.Replace("<" + header + ">", value);
      }

      return result;
    }

    public override string ToString()
    {
      if (HeaderCells == null || Row == null)
      {
        return string.Empty;
      }

      var first = true;
      var result = "| ";
      foreach (var tableCell in Row)
      {
        if (!first) result += " | ";
        else first = false;
        result += $"{tableCell.Value}";
      }

      result += " |";
      
      return result;
    }
  }
}
