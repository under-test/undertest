using System;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace UnderTest.Infrastructure.Binding
{
  public class UnderTestInvokeBinder : Binder
  {
    public override FieldInfo BindToField(BindingFlags bindingAttr, FieldInfo[] match, object value,
      CultureInfo culture)
    {
      if (match == null)
      {
        throw new ArgumentNullException(nameof(match));
      }

      return match.FirstOrDefault(field => ChangeType(value, field.FieldType, culture) != null);
    }

    public override MethodBase BindToMethod(BindingFlags bindingAttr, MethodBase[] match, ref object[] args,
      ParameterModifier[] modifiers, CultureInfo culture, string[] names, out object state)
    {
      var internalBinderState = new InternalBinderState();
      var arguments = new object[args.Length];
      args.CopyTo(arguments, 0);
      internalBinderState.Args = arguments;
      state = internalBinderState;

      if (match == null)
      {
        throw new ArgumentNullException(nameof(match));
      }

      var count = 0;
      foreach (var potentialMatch in match)
      {
        var parameters = potentialMatch.GetParameters();
        if (args.Length != parameters.Length)
        {
          continue;
        }

        for (var argIndex = 0; argIndex < args.Length; argIndex++)
        {
          if (names != null)
          {
            if (names.Length != args.Length)
            {
              throw new ArgumentException("names and args must have the same number of elements.");
            }

            for (var namesIndex = 0; namesIndex < names.Length; namesIndex++)
            {
              if (string.CompareOrdinal(parameters[argIndex].Name, names[namesIndex]) == 0)
              {
                args[argIndex] = internalBinderState.Args[namesIndex];
              }
            }
          }

          if (ChangeType(args[argIndex], parameters[argIndex].ParameterType, culture) != null)
          {
            count += 1;
          }
          else
          {
            break;
          }
        }

        // have we found the correct method?
        if (count == args.Length)
        {
          return potentialMatch;
        }
      }

      return null;
    }

    public override object ChangeType(object value, Type targetType, CultureInfo culture)
    {
      if (!CanConvertFrom(value.GetType(), targetType))
      {
        return null;
      }

      if (targetType.IsEnum)
      {
        return UnderTestEnumBinder.ToEnum(targetType, value.ToString());
      }

      if (targetType == typeof(Guid))
      {
        return Guid.Parse(value.ToString());
      }

      if (!targetType.IsGenericType || targetType.GetGenericTypeDefinition() != typeof(Nullable<>))
      {
        return Convert.ChangeType(value, targetType);
      }

      if (value is string strValue)
      {
        if (string.IsNullOrEmpty(value.ToString()))
        {
          return null;
        }

        if ("null".Equals(strValue.ToLower()))
        {
          return null;
        }
      }

      return Convert.ChangeType(value, targetType.GetGenericArguments()[0]);
    }

    public override void ReorderArgumentArray(ref object[] args, object state)
    {
      // Return the args that had been reordered by BindToMethod.
      ((InternalBinderState) state).Args.CopyTo(args, 0);
    }

    public override MethodBase SelectMethod(BindingFlags bindingAttr, MethodBase[] match, Type[] types,
      ParameterModifier[] modifiers)
    {
      if (match == null)
      {
        throw new ArgumentNullException(nameof(match));
      }

      var count = 0;
      foreach (var potentialMatch in match)
      {
        var parameters = potentialMatch.GetParameters();
        if (types.Length != parameters.Length)
        {
          continue;
        }

        for (var j = 0; j < types.Length; j++)
        {
          if (CanConvertFrom(types[j], parameters[j].ParameterType))
          {
            count += 1;
          }
          else
          {
            break;
          }
        }

        if (count == types.Length)
        {
          return potentialMatch;
        }
      }

      return null;
    }

    public override PropertyInfo SelectProperty(
      BindingFlags bindingAttr,
      PropertyInfo[] match,
      Type returnType,
      Type[] indexes,
      ParameterModifier[] modifiers
    )
    {
      if (match == null)
      {
        throw new ArgumentNullException(nameof(match));
      }

      var count = 0;
      foreach (var potentialMatch in match)
      {
        var parameters = potentialMatch.GetIndexParameters();
        if (indexes.Length != parameters.Length)
        {
          continue;
        }

        for (var j = 0; j < indexes.Length; j++)
        {
          if (CanConvertFrom(indexes[j], parameters[j].ParameterType))
          {
            count += 1;
          }
          else
          {
            break;
          }
        }

        if (count != indexes.Length)
        {
          continue;
        }

        if (CanConvertFrom(returnType, potentialMatch.PropertyType))
        {
          return potentialMatch;
        }
      }

      return null;
    }

    private bool CanConvertFrom(Type type1, Type type2)
    {
      // we support strings
      if (Type.GetTypeCode(type1) == TypeCode.String || Type.GetTypeCode(type2) == TypeCode.String)
      {
        return true;
      }

      // we support binding to target enums
      if (type2.IsEnum)
      {
        return true;
      }

      if (!type1.IsPrimitive || !type2.IsPrimitive)
      {
        return false;
      }

      var typeCode1 = Type.GetTypeCode(type1);
      var typeCode2 = Type.GetTypeCode(type2);

      if (typeCode1 == typeCode2)
      {
        return true;
      }

      switch (typeCode1)
      {
        case TypeCode.String:
          switch (typeCode2)
          {
            case TypeCode.Boolean:
            case TypeCode.Byte:
            case TypeCode.Char:
            case TypeCode.Decimal:
            case TypeCode.Double:
            case TypeCode.Int16:
            case TypeCode.Int32:
            case TypeCode.Int64:
            case TypeCode.Object:
            case TypeCode.SByte:
            case TypeCode.Single:
            case TypeCode.String:
            case TypeCode.UInt16:
            case TypeCode.UInt32:
            case TypeCode.UInt64:
              return true;
            default:
              return false;
          }
        case TypeCode.Char:
          switch (typeCode2)
          {
            case TypeCode.UInt16:
            case TypeCode.UInt32:
            case TypeCode.Int32:
            case TypeCode.UInt64:
            case TypeCode.Int64:
            case TypeCode.Single:
            case TypeCode.Double:
              return true;
            default:
              return false;
          }

        case TypeCode.Byte:
          switch (typeCode2)
          {
            case TypeCode.Char:
            case TypeCode.UInt16:
            case TypeCode.Int16:
            case TypeCode.UInt32:
            case TypeCode.Int32:
            case TypeCode.UInt64:
            case TypeCode.Int64:
            case TypeCode.Single:
            case TypeCode.Double:
              return true;
            default:
              return false;
          }

        case TypeCode.SByte:
          switch (typeCode2)
          {
            case TypeCode.Int16:
            case TypeCode.Int32:
            case TypeCode.Int64:
            case TypeCode.Single:
            case TypeCode.Double:
              return true;
            default:
              return false;
          }
        case TypeCode.UInt16:
          switch (typeCode2)
          {
            case TypeCode.UInt32:
            case TypeCode.Int32:
            case TypeCode.UInt64:
            case TypeCode.Int64:
            case TypeCode.Single:
            case TypeCode.Double:
              return true;
            default:
              return false;
          }

        case TypeCode.Int16:
          switch (typeCode2)
          {
            case TypeCode.Int32:
            case TypeCode.Int64:
            case TypeCode.Single:
            case TypeCode.Double:
              return true;
            default:
              return false;
          }

        case TypeCode.UInt32:
          switch (typeCode2)
          {
            case TypeCode.UInt64:
            case TypeCode.Int64:
            case TypeCode.Single:
            case TypeCode.Double:
              return true;
            default:
              return false;
          }

        case TypeCode.Int32:
          switch (typeCode2)
          {
            case TypeCode.Int64:
            case TypeCode.Single:
            case TypeCode.Double:
              return true;
            default:
              return false;
          }

        case TypeCode.UInt64:
          switch (typeCode2)
          {
            case TypeCode.Single:
            case TypeCode.Double:
              return true;
            default:
              return false;
          }

        case TypeCode.Int64:
          switch (typeCode2)
          {
            case TypeCode.Single:
            case TypeCode.Double:
              return true;
            default:
              return false;
          }

        case TypeCode.Single:
          switch (typeCode2)
          {
            case TypeCode.Double:
              return true;
            default:
              return false;
          }

        default:
          return false;
      }
    }

    private class InternalBinderState
    {
      public object[] Args;
    }
  }
}
