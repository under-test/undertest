﻿using System;
using System.Linq;
using UnderTest.Support;
using static System.Enum;

namespace UnderTest.Infrastructure.Binding
{
  public static class UnderTestEnumBinder
  {
    public static object ToEnum(Type targetType, string input)
    {
      input = input?.Trim();
      foreach (var name in GetNames(targetType))
      {
        var alias =
            targetType
              .GetField(name)
              .GetCustomAttributes(typeof(EnumAliasAttribute), true)
              .Cast<EnumAliasAttribute>()
              .FirstOrDefault(x
                => string.Equals(x.Alias, input, StringComparison.CurrentCultureIgnoreCase));

        if (alias == null)
        {
          continue;
        }

        return Parse(targetType, name);
      }

      return Parse(targetType, input, true);
    }
  }
}
