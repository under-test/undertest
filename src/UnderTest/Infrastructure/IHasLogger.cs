using JetBrains.Annotations;
using Serilog;

namespace UnderTest.Infrastructure
{
  [PublicAPI]
  public interface IHasLogger
  {
    ILogger Log { get; }
  }
}
