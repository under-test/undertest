using System.Collections.Generic;
using System.IO;
using JetBrains.Annotations;

namespace UnderTest.Infrastructure
{
  [PublicAPI]
  public class TestRunOutputSettings
  {
    private IList<string> assetsToCopyToReportFolder = new List<string>();

    public IEnumerable<string> AssetsToCopyToReportFolder => assetsToCopyToReportFolder;

    public string ReportOutputFileName { get; private set; } = "undertest-result.json";
    public string ReportOutputFolder { get; set; }

    public string ReportOutputFilePath => Path.Combine(ReportOutputFolder, ReportOutputFileName);
    
    public bool WarnAboutScenarioExecution { get; private set; } = true;

    public TestRunOutputSettings AddAssetGlobToCopyToReportFolder(string fileGlob)
    {
      assetsToCopyToReportFolder.Add(fileGlob);

      return this;
    }

    public TestRunOutputSettings SetReportOutputFolder(string folder)
    {
      ReportOutputFolder = folder;

      if (!Directory.Exists(folder))
      {
        Directory.CreateDirectory(folder);
      }

      return this;
    }

    public TestRunOutputSettings SetReportOutputFileName(string filePath)
    {
      ReportOutputFileName = filePath;

      return this;
    }

    public TestRunOutputSettings DisableWarnAboutScenarioExecution()
    {
      WarnAboutScenarioExecution = false;

      return this;
    }
    
    public TestRunOutputSettings EnableWarnAboutScenarioExecution()
    {
      WarnAboutScenarioExecution = true;

      return this;
    }
  }
}
