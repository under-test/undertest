using JetBrains.Annotations;
using UnderTest.Reporting;

namespace UnderTest.Infrastructure
{
  [PublicAPI]
  public interface IReportOutputWriter
  {
    IUnderTestSettings TestSettings { get; }

    void SaveResults(UnderTestRunResult results);
  }
}
