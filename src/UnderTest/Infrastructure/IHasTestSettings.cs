using JetBrains.Annotations;

namespace UnderTest.Infrastructure
{
  [PublicAPI]
  public interface IHasTestSettings
  {
    IUnderTestSettings TestSettings { get; }
  }
}
