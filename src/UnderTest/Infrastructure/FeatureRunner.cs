using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Gherkin.Ast;
using Serilog;
using UnderTest.Exceptions;
using UnderTest.Filtering;
using UnderTest.Infrastructure.ExampleTableDataSources;
using UnderTest.Reporting;
using UnderTest.Strategies;

namespace UnderTest.Infrastructure
{
  public class FeatureRunner : IHasLogger, IHasInternalTestSettings
  {
    public ILogger Log => TestSettings.Logger;

    public TestRunSettings TestSettings { get; set; }

    private TestRunFeatureStrategyResult strategyTestRunResult;

    private FeatureContext featureContext;

    private ITestStrategy testStrategy;

    private string StageNameAfterFeature = "AfterFeature";
    private string StageNameBeforeFeature = "BeforeFeature";
    
    public void Execute(FeatureContext featureContextP, TestRunFeatureStrategyResult strategyTestRunResultP,
      ITestStrategy testStrategyP)
    {
      strategyTestRunResult = strategyTestRunResultP;
      featureContext = featureContextP;
      testStrategy = testStrategyP;

      var handler = TestSettings.FeatureHandlerLocator.LocateContainerByFeatureContext(featureContext);
      if (handler == null)
      {
        Log.Warning(@$"  No handler found for feature {featureContext.Filename}. Common causes of this are:
    1. Feature handler not inheriting from FeatureHandler
    2. Missing [HandlesFeature(""PATH"")]
    3. Invalid path in [HandlesFeature(""PATH"")]
    4. Intentional use of a global FeatureHandler");
        Log.Information("We will attempt to look for global handlers.");
      }
      else
      {
        Log.Information($"  Handler located class name is {handler.GetType().Name}.");
      }
      
      var backgroundSteps = new List<StepForProcessing>();
      var beforeAfterFeatureContext = new BeforeAfterFeatureContext{ Feature = featureContext };
      try
      {
        if (CallAndFailFeatureOnException(
          () =>
          {
            handler.ThrowIfInconclusive();

            TestSettings.TestRunEventHandlers.ForEach(x => x.BeforeFeature(new BeforeAfterFeatureContext{ Feature = featureContext }));
            beforeAfterFeatureContext.TriggerBeforeFeatureEvent(testStrategyP, handler, TestSettings);
          },
          StageNameBeforeFeature, handler.HasExpectedFailureAttribute()))
        {
          ExecuteFeatureFromChildComponents(backgroundSteps, featureContext.Document.Feature, handler);
        }
      }
      finally
      {
        CallAndFailFeatureOnException(
          () =>
          {
            TestSettings.TestRunEventHandlers.ForEach(x => x.AfterFeature(new BeforeAfterFeatureContext{ Feature = featureContext }));
            beforeAfterFeatureContext.TriggerAfterFeatureEvent(testStrategy, handler, TestSettings);
          },
          StageNameAfterFeature, handler.HasExpectedFailureAttribute());
      }
    }

    private void CompileScenariosFromExamples(IFeatureHandler handler, Scenario scenario, List<StepForProcessing> backgroundSteps)
    {
      var exampleTableEnhancer = TestSettings.Container.GetInstance<IExampleTableEnhancer>();
      var exampleIndex = 1;

      // do we need to ignore this scenario?
      if (scenario.Tags.IncludesIgnoreTag(TestSettings.ConfigSettings))
      {
        Log.Warning($"'{scenario.Name}' marked as ignored.  This scenario will be skipped.");
        strategyTestRunResult.SetAndNoOpWithStrategyName(testStrategy.Name);
        strategyTestRunResult.ScenarioTestResults.AddScenarioAsFullyIgnored(scenario, backgroundSteps);
        return;
      }

      Log.Information($"  Processing scenario: {scenario.Name}");
      if (!string.IsNullOrWhiteSpace(scenario.Description))
      {
        Log.Information($"    Scenario description: {scenario.Description.Trim()}");
      }
      EnhanceExamplesIfNeededOrFailScenario(scenario, exampleTableEnhancer, backgroundSteps)
        .ForEach(exampleTable =>
        {
          Log.Information($"  Processing example table: {exampleTable.Name}");

          if (exampleTable.Tags.IncludesIgnoreTag(TestSettings.ConfigSettings))
          {
            Log.Warning($"  Example table marked as `@ignore` for scenario `{scenario.Name}` on table named `{exampleTable.Name}`.  Skipping over these {exampleTable.TableBody.Count()} examples.");
            strategyTestRunResult.ScenarioTestResults.AddExampleScenariosAsIgnored(scenario, backgroundSteps, exampleTable);
            return;
          }
          
          if (exampleTable.Tags.IncludesFutureVersionTag(TestSettings.ConfigSettings))
          {
            Log.Warning($"  Example table marked as `@future-version` for scenario `{scenario.Name}` on table named `{exampleTable.Name}`.  Skipping over these {exampleTable.TableBody.Count()} examples.");
            strategyTestRunResult.ScenarioTestResults.AddExampleScenariosAsSkipped(scenario, backgroundSteps, exampleTable);
            return;
          }

          if (featureContext.ShouldBeFiltered(TestSettings.TestFilters, new TestFilterContext
          {
            Scenario = scenario,
            FeatureContext = featureContext,
            TestStrategy = testStrategy,
            SpecificExample = exampleTable
          }))
          {
            Log.Warning($"  Example table `{exampleTable.Name}` marked with tag(or not) causing it be filtered for scenario `{scenario.Name}`.  Skipping over these {exampleTable.TableBody.Count()} examples.");
            strategyTestRunResult.ScenarioTestResults.AddExampleScenariosAsSkipped(scenario, backgroundSteps, exampleTable);
            return;
          }

          if (exampleTable.TableHeader == null)
          {
            Log.Warning($"  Invalid or missing table header for scenario '{scenario.Name}'.  Failing example.");
            strategyTestRunResult.SetAndNoOpWithStrategyName(testStrategy.Name);
            strategyTestRunResult.ScenarioTestResults.AddScenarioAsFullyFailed(scenario, backgroundSteps);
            return;
          }

          exampleTable.TableBody.ForEach(valuesRow =>
          {
            Log.Information(
              $"  Processing example {exampleIndex}:\n  {scenario.Name} {valuesRow.ToDisplayOfRowValues()} ");

            ExecuteFeatureScenario(handler, backgroundSteps, scenario, new ExampleTableRow(exampleTable.TableHeader.Cells, valuesRow.Cells));

            exampleIndex++;
          });
        });
    }

    private IList<Examples> EnhanceExamplesIfNeededOrFailScenario(Scenario scenario, IExampleTableEnhancer exampleTableEnhancer,
                                                                    IEnumerable<StepForProcessing> backgroundSteps)
    {
      try
      {
        return exampleTableEnhancer.EnhanceExamples(scenario, featureContext);
      }
      catch (Exception e)
      {
        var message = $"  Error processing example table enhancement with exception: {e.Message}.  Skipping example table completely.";
        Log.Error(message);

        strategyTestRunResult.SetAndNoOpWithStrategyName(testStrategy.Name);
        strategyTestRunResult.ScenarioTestResults.AddScenarioAsFullyFailed(scenario, backgroundSteps);
        return new List<Examples>();
      }
    }

    private void ExecuteFeatureFromChildComponents(List<StepForProcessing> parentBackgroundSteps, IHasChildren parent,
      IFeatureHandler handler)
    {
      var backgroundSteps = new List<StepForProcessing>(parentBackgroundSteps);
      foreach (var child in parent.Children)
      {
        switch (child)
        {
          case Background background:
            backgroundSteps.AddRange(new List<StepForProcessing>(background.Steps.ToStepsForProcessingList()));
            break;
          case Rule rule:
            ExecuteFeatureFromChildComponents(backgroundSteps, rule, handler);
            break;
          default:
            ExecuteScenario(handler, (Scenario) child, backgroundSteps);
            break;
        }
      }
    }

    private void ExecuteScenario(IFeatureHandler handler, Scenario scenario, List<StepForProcessing> backgroundSteps)
    {
      // do we need to ignore this scenario?
      if (scenario.Tags.IncludesIgnoreTag(TestSettings.ConfigSettings))
      {
        Log.Warning($"'{scenario.Name}' marked as ignored.  This scenario will be skipped.");
        strategyTestRunResult.SetAndNoOpWithStrategyName(testStrategy.Name);
        strategyTestRunResult.ScenarioTestResults.AddScenarioAsFullyIgnored(scenario, backgroundSteps);
        return;
      }

      if (scenario.Tags.Any(tag => TestSettings.ConfigSettings.WipTagName.Equals(tag.Name, StringComparison.InvariantCultureIgnoreCase)))
      {
        Log.Information($"Scenario is wip: {scenario.Name} The scenarios will not be run.");

        strategyTestRunResult.SetAndNoOpWithStrategyName(testStrategy.Name);
        strategyTestRunResult.ScenarioTestResults.AddScenarioAsFullyWip(scenario, backgroundSteps);
        return;
      }

      if (featureContext.ShouldBeFiltered(TestSettings.TestFilters, new TestFilterContext
      {
        Scenario = scenario,
        FeatureContext = featureContext,
        TestStrategy = testStrategy
      }))
      {
        Log.Information($"  Filtering scenario: {scenario.Name} Scenarios will not be run.");

        strategyTestRunResult.SetAndNoOpWithStrategyName(testStrategy.Name);
        strategyTestRunResult.ScenarioTestResults.AddScenarioAsFullySkipped(scenario, backgroundSteps);
        return;
      }

      if (!scenario.IsScenarioOutline() && scenario.Examples.Any())
      {
        // this is an edge we want to catch in undertest
        // gherkin-wise this is an allowable situation.  This is a scenario with examples.
        // we have chosen to throw an exception in this case and fail the test.
        // if you would like to have an option what happens here, create an issue https://gitlab.com/under-test/undertest/issues
        Log.Information(
          $"Filtering feature(Scenario with examples found)(Not a Scenario Outline): {featureContext.Filename} No scenarios will be run.");

        strategyTestRunResult.SetAndNoOpWithStrategyName(testStrategy.Name);
        strategyTestRunResult.ScenarioTestResults.AddScenarioAsFullyFailed(scenario, backgroundSteps);
      }
      else if (!scenario.Examples.Any())
      {
        ExecuteFeatureScenario(handler, backgroundSteps, scenario, null);
      }
      else
      {
        CompileScenariosFromExamples(handler, scenario, backgroundSteps);
      }
    }

    private void ExecuteFeatureScenario(IFeatureHandler handler, List<StepForProcessing> backgroundSteps,
      Scenario scenario,
      ExampleTableRow exampleTableRow)
    {
      // todo - move to DI container
      var scenarioRunner = new ScenarioRunner
      {
        TestSettings = TestSettings,
        FeatureContext = featureContext,
        Handler = handler,
        BackgroundSteps = backgroundSteps,
        Scenario = scenario,
        ExampleTableRow = exampleTableRow,
        StrategyTestRunResult = strategyTestRunResult,
        TestStrategy = testStrategy
      };

      if (TestSettings.ExecutionSettings.ScenarioTimeout == TimeSpan.Zero)
      {
        scenarioRunner.Execute();
        return;
      }

      try
      {
        var task = Task.Run(() => scenarioRunner.Execute());
        if (!task.Wait(TestSettings.ExecutionSettings.ScenarioTimeout))
          throw new ScenarioExecutionTimeoutException(
            $"Timed out the execution of scenario `{scenario.Name}` within {TestSettings.ExecutionSettings.ScenarioTimeout.TotalSeconds}sec")
          {
            ScenarioName = scenario.Name,
            ExampleTableRow = exampleTableRow
          };
      }
      catch (ScenarioExecutionTimeoutException e)
      {
        Log.Error($"'{scenario.Name}' timed out with message: " + e.Message);
        strategyTestRunResult.ScenarioTestResults.AddScenarioAsFullyFailed(scenario, backgroundSteps);
      }
    }

    private bool CallAndFailFeatureOnException(Action act, string contextLocation,
      bool expectedFailure)
    {
      try
      {
        act();

        if (contextLocation == StageNameAfterFeature
            &&
            strategyTestRunResult
              .ScenarioTestResults
              .All(x => x.StepResults.All(y => y.ScenarioStepResultType != ScenarioStepResultType.Failed))
            && expectedFailure)
        {
          throw new ExpectedExceptionButPassedException("Expected scenario to fail but passed.  You should remove the [ExpectedException] from the feature or scenario.");
        }

        return true;
      }
      catch (FeatureHandlerMarkedAsInconclusiveException e)
      {
        Log.Warning($"  `{e.Handler.GetType().Name}` is marked inconclusive. {e.Message}.  Feature will be skipped.");
        
        strategyTestRunResult.ScenarioTestResults.Add(new ScenarioRunResult
        {
          Description = e.Message,
          Errors = { new StepNotice{ Message = e.Message }},
          Result = ScenarioRunResultType.Inconclusive,
          ExpectedFailure = expectedFailure
        });
      }
      catch (Exception e)
      {
        e.OutputException(TestSettings, $"    Failure in {contextLocation}: ");

        if (contextLocation == StageNameAfterFeature)
        {
          // mark the previously passed steps as failed
          foreach (var testResult in strategyTestRunResult.ScenarioTestResults)
          {
            testResult.Result = ScenarioRunResultType.Failed;
            testResult.Errors.Add(new StepNotice {NoticeCode = "Error", Message = $"{contextLocation}-{e.Message}"});
          }
        }
        else if (contextLocation == StageNameBeforeFeature)
        {
          // we need to add all scenarios to the 
          featureContext.Document.Feature.Children.ForEachScenario(x => 
            strategyTestRunResult.ScenarioTestResults.Add(new ScenarioRunResult
            {
              Description = $"Failure in {contextLocation}",
              Errors = { new StepNotice{ Message = e.Message }},
              Result = ScenarioRunResultType.Failed,
              ExpectedFailure = expectedFailure,
              ScenarioName = x.Name
            }));
        }
        
        Log.Error("This will cause previously passing scenario(s) to now be marked as failure.");
      }

      return false;
    }
  }
}
