using System.Linq;
using UnderTest.Attributes;

namespace UnderTest.Infrastructure.Preflight
{
  public class CheckForPotentiallyMalformedFeatureHandlers: IPreflightCheck
  {
    public void Execute(TestRunSettings settings)
    {
      WarnForFeatureHandlersWithoutHandlesFeatureAttribute(settings);

      WarnForHandlesFeatureAttributeWithoutFeatureHandlers(settings);
    }

    private static void WarnForFeatureHandlersWithoutHandlesFeatureAttribute(TestRunSettings settings)
    {
      var featureHandlerWithoutAttribute = settings
        .Assemblies
        .SelectMany(x => x.GetTypes())
        .Where(type => typeof(IFeatureHandler).IsAssignableFrom(type))
        .Where(type => !type.IsAbstract)
        .Where(type => !type.IsDefined(typeof(HandlesFeatureAttribute), false))
        .ToList();

      featureHandlerWithoutAttribute.ForEach(x =>
        settings.Logger.Warning($"IFeatureHandler implementation found without HandlesFeatureAttribute {x.Name}"));
    }

    private static void WarnForHandlesFeatureAttributeWithoutFeatureHandlers(TestRunSettings settings)
    {
      var attributesWithoutBaseClass = settings
        .GetConfiguredFeatureHandlers()
        .Where(type => !typeof(IFeatureHandler).IsAssignableFrom(type))
        .ToList();

      attributesWithoutBaseClass.ForEach(x =>
        settings.Logger.Warning($"HandlesFeatureAttribute found on a class without inheriting from FeatureHandler {x.Name}"));
    }
  }
}
