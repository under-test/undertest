namespace UnderTest.Infrastructure.Preflight
{
  public interface IPreflightCheck
  {
    void Execute(TestRunSettings settings);
  }
}