using System.Collections.Generic;
using Gherkin.Ast;

namespace UnderTest.Infrastructure.ExampleTableDataSources
{
  public interface IExampleTableEnhancer : IHasTestSettings
  {
    IList<Examples> EnhanceExamples(Scenario scenario, FeatureContext featureContext);
  }
}
