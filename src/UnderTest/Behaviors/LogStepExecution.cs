﻿using JetBrains.Annotations;

namespace UnderTest.Behaviors
{
  [PublicAPI]
  public class LogStepExecution: BehaviorAttribute
  {
    public override void OnStepExecuting(StepExecutingContext context)
    {
      base.OnStepExecuting(context);

      context.Handler.Log.Information($"    About to execute `{context.Step.Keyword}{context.Step.Text}`");
    }

    public override void OnStepExecuted(StepExecutingContext context)
    {
      base.OnStepExecuted(context);

      context.Handler.Log.Information($"    Executed `{context.Step.Keyword}{context.Step.Text}`");
    }
  }
}
