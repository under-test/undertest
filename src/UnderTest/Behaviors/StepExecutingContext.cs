﻿using JetBrains.Annotations;

namespace UnderTest.Behaviors
{
  [UsedImplicitly]
  public class StepExecutingContext : BeforeAfterStepContext
  {
    public FeatureHandler Handler { get; set; }
  }
}
