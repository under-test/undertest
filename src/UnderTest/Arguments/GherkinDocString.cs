using JetBrains.Annotations;

namespace UnderTest.Arguments
{
  [PublicAPI]
  public class GherkinDocString
  {
    public GherkinDocString(string contentType, string content)
    {
      ContentType = contentType;
      Content = content;
    }

    public string ContentType { get; }

    public string Content { get; }

    public override string ToString() => $"\"\"\"\nContent\n\"\"\"";
  }
}
