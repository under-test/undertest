using System.Collections.Generic;
using System.Linq;

namespace UnderTest.Arguments
{
  public class GherkinDataTableHeader
  {
    public GherkinDataTableHeader(IList<GherkinDataTableCell> cells)
    {
      Cells = cells;
    }

    public IList<GherkinDataTableCell> Cells { get; }

    public override string ToString() => string.Join("|", Cells);
  }
}
