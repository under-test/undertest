﻿using System.Linq;
using Gherkin.Ast;
using JetBrains.Annotations;
using UnderTest.Infrastructure;

namespace UnderTest
{
  [PublicAPI]
  public class ScenarioExecutionContext
  {
    public ScenarioExecutionContext(FeatureContext feature, Scenario scenario, ExampleTableRow exampleTableRow)
    {
      Feature = feature;

      var tags = Feature.CombineTagsWithScenario(scenario);

      Scenario = new Scenario(tags.ToArray(), scenario.Location, scenario.Keyword, scenario.Name, scenario.Description, scenario.Steps?.ToArray(), scenario.Examples?.ToArray());

      ExampleTableRow = exampleTableRow;
    }

    public ExampleTableRow ExampleTableRow { get; set; }
    
    public FeatureContext Feature { get; set; }

    public bool HasExampleTableRow => ExampleTableRow != null;

    public Scenario Scenario { get; set; }
  }
}
