using System;
using System.Linq;
using UnderTest.Arguments;

namespace UnderTest.Attributes.StepParams
{
  public abstract class GherkinDataTableStepParamAttribute : StepParamAttribute
  {
    public override object Transform(object input)
    {
      if (!(input is GherkinDataTable dataTable))
      {
        throw new ArgumentException("Argument type of 'GherkinDataTable' expected", nameof(input));
      }

      for (var rowIndex = 0; rowIndex < dataTable.Rows.Count(); rowIndex++)
      {
        for (var colIndex = 0; colIndex < dataTable.Rows[rowIndex].Cells.Count; colIndex++)
        {
          dataTable.Rows[rowIndex].Cells[colIndex].Value = TransformCellValue(
            dataTable.Header.Cells[colIndex].Value,
            dataTable.Rows[rowIndex].Cells[colIndex].Value,
            rowIndex,
            colIndex);
        }
      }

      return dataTable;
    }

    public abstract string TransformCellValue(string heading, string cellText, int rowIndex, int colIndex);
  }
}
