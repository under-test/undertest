using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UnderTest.Exceptions;

namespace UnderTest.Attributes.StepParams
{
  [PublicAPI]
  public class CollectionStepParam : StepParamAttribute
  {
    public string ItemSeparator { get; set; } = ",";

    public Type ListCollectionType { get; set; } = typeof(string);

    public bool TrimElements { get; set; } = true;

    public override object Transform(object input)
    {
      dynamic result = Activator.CreateInstance(typeof(List<>).MakeGenericType(ListCollectionType));

      var inputStr = input as string;
      if (string.IsNullOrWhiteSpace(inputStr))
      {
        return result;
      }

      var elements = inputStr.Split(new [] { ItemSeparator }, StringSplitOptions.RemoveEmptyEntries);
      foreach (var element in elements)
      {
        var elementValue = element;
        if (TrimElements)
        {
          elementValue = elementValue.Trim();
        }

        try
        {
          result.Add((dynamic)Convert.ChangeType(elementValue, ListCollectionType));
        }
        catch (Exception e)
        {
          throw new InvalidCollectionStepParamValueException(
            $"Failed to convert value('{elementValue}') within the gherkin to the target type {ListCollectionType.Name}", e);
        }
      }

      return result;
    }
  }
}
