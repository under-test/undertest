using System;

namespace UnderTest.Attributes
{
  [AttributeUsage(AttributeTargets.Parameter)]
  public abstract class StepParamAttribute : Attribute
  {
    public abstract object Transform(object input);
  }
}
