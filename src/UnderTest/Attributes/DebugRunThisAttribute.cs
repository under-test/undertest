using System;
using JetBrains.Annotations;

namespace UnderTest.Attributes
{
  [PublicAPI]
  [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
  public class DebugRunThisAttribute : Attribute, IAmADebugAttribute
  { }
}
