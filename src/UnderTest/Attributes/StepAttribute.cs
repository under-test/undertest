using System;
using JetBrains.Annotations;

namespace UnderTest.Attributes
{
  [MeansImplicitUse]
  [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
  public class StepAttribute : Attribute
  {
    public StepAttribute(string step)
    {
      Step = step ?? throw new ArgumentNullException(nameof(step));
    }

    public string Step { get; }
  }
}
