using JetBrains.Annotations;

namespace UnderTest.Attributes
{
  [MeansImplicitUse]
  public class ThenAttribute : StepAttribute
  {
    public ThenAttribute(string step) : base(step)
    { }
  }
}
