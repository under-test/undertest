﻿using System;
using JetBrains.Annotations;

namespace UnderTest.Attributes
{
  [PublicAPI]
  [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
  public class ExpectedFailureAttribute: Attribute
  {
    public string Message { get; set; }
  }
}
