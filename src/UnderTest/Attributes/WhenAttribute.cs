using JetBrains.Annotations;

namespace UnderTest.Attributes
{
  [MeansImplicitUse]
  public class WhenAttribute : StepAttribute
  {
    public WhenAttribute(string step) : base(step)
    { }
  }
}
