using System.Linq;
using Gherkin.Ast;
using JetBrains.Annotations;

namespace UnderTest
{
  [PublicAPI]
  public class BeforeAfterScenarioContext
  {
    public BeforeAfterScenarioContext(FeatureContext feature, Scenario scenario)
    {
      Feature = feature;

      var tags = Feature.CombineTagsWithScenario(scenario);

      Scenario = new Scenario(tags.ToArray(), scenario.Location, scenario.Keyword, scenario.Name, scenario.Description, scenario.Steps?.ToArray(), scenario.Examples?.ToArray());
    }

    public FeatureContext Feature { get; set; }

    public Scenario Scenario { get; set; }
  }
}
