using JetBrains.Annotations;

namespace UnderTest
{
  [PublicAPI]
  public class BeforeAfterFeatureContext
  {
    public FeatureContext Feature { get; set; }
  }
}
