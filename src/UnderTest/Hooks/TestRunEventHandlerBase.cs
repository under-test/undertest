﻿using JetBrains.Annotations;
using Serilog;
using UnderTest.Attributes;
using UnderTest.Strategies;

namespace UnderTest
{
  [PublicAPI]
  public abstract class TestRunEventHandlerBase: ITestStrategyEventHandlers
  {
    public ILogger Log => TestSettings.Logger;

    [PropertyInjected]
    public IUnderTestSettings TestSettings { get; internal set; }

    public virtual void OnInit()
    { }

    public virtual void BeforeTestRun(BeforeAfterTestRunContext context)
    { }

    public virtual void BeforeFeature(BeforeAfterFeatureContext context)
    { }

    public virtual void BeforeScenario(BeforeAfterScenarioContext context)
    { }

    public virtual void BeforeBackgroundKeywordGrouping(BeforeAfterKeywordGroupingContext context)
    { }

    public virtual void BeforeScenarioKeywordGrouping(BeforeAfterKeywordGroupingContext context)
    { }

    public virtual  void BeforeStep(BeforeAfterStepContext context)
    { }

    public virtual void AfterFeature(BeforeAfterFeatureContext context)
    { }

    public virtual void AfterScenario(BeforeAfterScenarioContext context)
    { }

    public virtual void AfterScenarioKeywordGrouping(BeforeAfterKeywordGroupingContext context)
    { }

    public virtual void AfterBackgroundKeywordGrouping(BeforeAfterKeywordGroupingContext context)
    { }

    public virtual void AfterStep(BeforeAfterStepContext context)
    { }

    public virtual void AfterTestRun(BeforeAfterTestRunContext context)
    { }
  }
}
