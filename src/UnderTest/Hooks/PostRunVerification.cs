using UnderTest.Reporting;

namespace UnderTest
{
  public delegate UnderTestRunResult PostRunVerification(UnderTestRunResult result);
}
