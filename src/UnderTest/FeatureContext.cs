using System;
using System.Collections.Generic;
using System.Linq;
using Gherkin.Ast;
using Newtonsoft.Json;
using UnderTest.Exceptions;
using UnderTest.Filtering;

namespace UnderTest
{
  [Serializable]
  public class FeatureContext
  {
    [JsonIgnore]
    public GherkinDocument Document { get; internal set; }

    public string Filename { get; internal set; }

    [JsonIgnore]
    public IUnderTestSettings TestSettings { get; set; }

    public int ExecutionOrder
    {
      get
      {
        var handler = TestSettings.FeatureHandlerLocator.LocateContainerByFeatureContext(this);
        return handler?.FeatureExecutionOrder ?? 0;
      }
    }

    internal bool ShouldBeFiltered(IEnumerable<TestFilter> filters, TestFilterContext context)
    {
      try
      {
        var testFilters = filters.ToList();
        return testFilters.Any() && testFilters.Any(x => x(context));
      }
      catch (Exception e)
      {
        throw new UnderTestFilterFailureException("Filter threw an exception?  Are you ensuring all properties you are accessing are not null", e);
      }
    }
    
    internal IEnumerable<Tag> CombineTagsWithScenario(Scenario scenario)
    {
      var result = new List<Tag>();
      if (Document?.Feature?.Tags != null)
      {
        result.AddRange(Document.Feature.Tags.ToList());
      }

      if (scenario?.Tags != null)
      {
        result.AddRange(scenario.Tags);
      }
      
      return result
              .GroupBy(p => p.Name)
              .Select(g => g.First())
              .ToList();
    }
  }
}
