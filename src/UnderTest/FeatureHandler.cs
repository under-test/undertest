using System;
using System.Linq;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Serilog;
using UnderTest.Attributes;

namespace UnderTest
{
  [PublicAPI]
  public class FeatureHandler<T> : FeatureHandler where T: FeatureHandlerServiceBase
  {
    public async Task ExecutesAsync([NotNull]StepsToExecute<T> executor)
    {
      var service = TestSettings.Container.GetInstance<T>();
      await executor.Invoke(service);
    }

    public Task ExecutesAsync([NotNull]Func<Task<T>> whatToExecute)
    {
      return whatToExecute();
    }

    public void ExecutesSync([NotNull]StepsToExecuteSync<T> executor)
    {
      var service = TestSettings.Container.GetInstance<T>();
       executor.Invoke(service);
    }
  }

  [PublicAPI]
  public class FeatureHandler: IFeatureHandler
  {
    public ILogger Log => TestSettings.Logger;

    public IUnderTestSettings TestSettings { get; set; }

    private int? featureExecutionOrder;

    public void BeforeTestRun(BeforeAfterTestRunContext context)
    {
      Log.Verbose($"Before test run");
    }

    public virtual void BeforeFeature(BeforeAfterFeatureContext context)
    {
      Log.Verbose($"Before feature {context.Feature.Document.Feature.Name}");
    }

    public virtual void BeforeScenario(BeforeAfterScenarioContext context)
    {
      Log.Verbose($"Before scenario {context.Scenario.Name}");
    }

    public virtual void BeforeBackgroundKeywordGrouping(BeforeAfterKeywordGroupingContext context)
    {
      Log.Verbose($"Before background keyword grouping {context.Scenario.Name} block {context.KeywordGrouping.Keyword.ToString()}");
    }

    public virtual void BeforeScenarioKeywordGrouping(BeforeAfterKeywordGroupingContext context)
    {
      Log.Verbose($"Before scenario keyword grouping {context.Scenario.Name} block {context.KeywordGrouping.Keyword.ToString()}");
    }

    public virtual void BeforeStep(BeforeAfterStepContext context)
    {
      Log.Verbose($"Before step {context.Step.Keyword}{context.Step.Text}");
    }

    public virtual void AfterFeature(BeforeAfterFeatureContext context)
    {
      Log.Verbose(($"After feature {context.Feature.Document.Feature.Name}"));
    }

    public virtual void AfterScenario(BeforeAfterScenarioContext context)
    {
      Log.Verbose($"After scenario {context.Scenario.Name}");
    }

    public virtual void AfterScenarioKeywordGrouping(BeforeAfterKeywordGroupingContext context)
    {
      Log.Verbose($"After scenario keyword grouping {context.Scenario.Name} block {context.KeywordGrouping.Keyword.ToString()}");
    }

    public virtual void AfterBackgroundKeywordGrouping(BeforeAfterKeywordGroupingContext context)
    {
      Log.Verbose($"After background keyword grouping {context.Scenario.Name} block {context.KeywordGrouping.Keyword.ToString()}");
    }

    public virtual void AfterStep(BeforeAfterStepContext context)
    {
      Log.Verbose($"After step {context.Step.Keyword}{context.Step.Text}");
    }

    public void AfterTestRun(BeforeAfterTestRunContext context)
    {
      Log.Verbose($"After test run");
    }

    public int FeatureExecutionOrder
    {
      get
      {
        if (featureExecutionOrder == null)
        {
          featureExecutionOrder = GetType()
            .GetCustomAttributes(typeof(FeatureExecutionOrderAttribute), true)
            .Cast<FeatureExecutionOrderAttribute>()
            .Select(attr => attr.Order)
            .FirstOrDefault();
        }

        return featureExecutionOrder.Value;
      }
    }

    public Task ExecutesAsync([NotNull]Func<Task> whatToExecute)
    {
      return whatToExecute();
    }

    public void ExecutesSync([NotNull]Action whatToExecute)
    {
      whatToExecute();
    }
    
    public void Noop(string message = null)
    { }
  }
}
