using System.Collections.Generic;
using JetBrains.Annotations;
using UnderTest.Infrastructure;

namespace UnderTest
{
  [PublicAPI]
  public class ScenarioKeywordGrouping
  {
    public GherkinStepKeywordType Keyword { get; set; }

    public IList<StepForProcessing> Steps { get; set; } = new List<StepForProcessing>();
  }
}
