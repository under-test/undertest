using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Gherkin.Ast;
using JetBrains.Annotations;
using Serilog;
using UnderTest.Attributes;
using UnderTest.Exceptions;
using UnderTest.Infrastructure;
using UnderTest.Infrastructure.StepParameters;

namespace UnderTest.Locators
{
  [PublicAPI]
  public class StepLocator : IStepLocator, IHasLogger
  {
    public ILogger Log => TestSettings.Logger;

    public StepLocator([NotNull] TestRunSettings testRunRunSettings)
    {
      TestSettings = testRunRunSettings ?? throw new ArgumentNullException(nameof(testRunRunSettings));
    }

    public string Name { get; } = "Default Step Locator";

    public TestRunSettings TestSettings { get; }

    // Context on step location.
    // Our process and priority is going to be (error if there are multiple):
    // 1. if we have a handler check if for matches
    //    a. first we will check for exact text matches
    //    b. then we will check for regex based matches
    // 2. check if we have a global handler types that have a matching binding
    //    a. first we will check for exact text matches
    //    b. then we will check for regex based matches
    public virtual StepLocatorMatch LocateStep([NotNull] StepForProcessing step, IFeatureHandler handler)
    {
      if (step == null)
      {
        throw new ArgumentNullException(nameof(step));
      }

      var globalSteps = TestSettings.TypesCache.GlobalHandlerTypes;
      if (globalSteps == null)
      {
        throw new ArgumentException("The StepMethods cache is not initialized");
      }

      var stepTypes = new List<Type> {GetAttributeTypeFromStepKeyword(step), typeof(StepAttribute)};
      if (handler != null)
      {
        var featureHandlerMatch = LocateStepOnTypes(step, stepTypes, new List<Type>{ handler.GetType() });
        if (featureHandlerMatch != null)
        {
          return featureHandlerMatch;
        }
      }

      return LocateStepOnTypes(step, stepTypes, globalSteps);
    }

    private StepLocatorMatch LocateStepOnTypes(StepForProcessing step, IList<Type> typeForStepKeyword, IEnumerable<Type> types)
    {
      var parameters = new StepParameterList();
      var stepParameterExtractor = TestSettings.Container.GetInstance<IStepParameterExtractor>();
      var methods = types
        .SelectMany(type => type.GetMethods())
        .Where(methodInfo => methodInfo.GetCustomAttributes(typeof(StepAttribute), true).Any())
        .Where(x =>
        {
          var attributes = new List<StepAttribute>();
          foreach (var stepAttribute in typeForStepKeyword)
          {
            attributes.AddRange(x.GetCustomAttributes(stepAttribute, true)
              .Cast<StepAttribute>()
              .ToList());
          }

          return attributes.Any(attr =>
          {
            var attType = attr.GetType();
            if (attType != GetAttributeTypeFromStepKeyword(step) && attType != typeof(StepAttribute))
            {
              return false;
            }

            var stepText = attr.Step.ToLowerInvariant().Trim();
            if (stepText == step.Text.ToLowerInvariant().Trim())
            {
              parameters.AddRange(stepParameterExtractor.Extract(attr.Step));
              return true;
            }

            var lambdaMatches = Regex.Matches(step.Text, $"^{attr.Step}$", RegexOptions.IgnoreCase);
            if (lambdaMatches.Count > 0)
            {
              parameters.AddRange(stepParameterExtractor.FromRegExMatches(lambdaMatches));
              return true;
            }

            if (step.ExampleTableRow == null)
            {
              return false;
            }

            if (stepText == step.ExampleTableRow.ApplyRowToStepText(step.Text).ToLowerInvariant().Trim())
            {
              parameters.AddRange(stepParameterExtractor.Extract(attr.Step));
              return true;
            }

            lambdaMatches = Regex.Matches(step.ExampleTableRow.ApplyRowToStepText(step.Text).Trim(),
              $"^{attr.Step}$", RegexOptions.IgnoreCase);
            if (lambdaMatches.Count == 0)
            {
              return false;
            }

            parameters.AddRange(stepParameterExtractor.FromRegExMatches(lambdaMatches));
            return true;
          });
        })

        .ToList();

      if (!methods.Any())
      {
        return null;
      }

      // if there are multiple matches - we are not going to guess at this point.
      // if you are interested in having this be more intelligent - create an issue to discuss
      if (methods.Count > 1)
      {
        throw new MultipleHandlersMatchStepsException($"Multiple steps match the step `{step.Text}`");
      }

      return new StepLocatorMatch
      {
        MethodToCall = methods.First(),
        StepParameters = parameters
      };
    }

    private Type GetAttributeTypeFromStepKeyword(StepForProcessing step)
    {
      switch(step.AttributeKeyword.ToLowerInvariant().Trim()){
        case "given":
          return typeof(GivenAttribute);
        case "when":
          return typeof(WhenAttribute);
        case "then":
          return typeof(ThenAttribute);
        default:
          throw new Exception($"Unknown keyword {step.Keyword}");
      }
    }
  }
}
