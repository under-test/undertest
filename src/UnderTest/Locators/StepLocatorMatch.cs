using System.Reflection;
using UnderTest.Infrastructure.StepParameters;

namespace UnderTest.Locators
{
  public class StepLocatorMatch
  {
    public MethodInfo MethodToCall { get; set; }

    public StepParameterList StepParameters { get; set; }
  }
}
