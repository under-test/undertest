using System;

namespace UnderTest.Locators
{
  public class DefaultFeatureHandlerLocator : IFeatureHandlerLocator
  {
    public DefaultFeatureHandlerLocator(TestRunSettings testRunSettings)
    {
      TestSettings = testRunSettings;
    }

    public string Name { get; } = "Default FeatureHandler Locator";

    public TestRunSettings TestSettings { get; }

    public IFeatureHandler LocateContainerByFeatureContext(FeatureContext feature)
    {
      TestSettings
        .TypesCache
        .FeatureHandlers.TryGetValue(feature.Filename.NormalizeFilePath(), out var handlerType);

      if (handlerType == null)
      {
        return null;
      }

      var instance = TestSettings.Container.GetInstance(handlerType) as IFeatureHandler;

      return instance;
    }
  }
}
