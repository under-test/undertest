using System;
using System.Runtime.Serialization;
using System.Security.Permissions;
using JetBrains.Annotations;

namespace UnderTest.Exceptions
{
  [PublicAPI]
  [Serializable]
  public class WorkInProgressScenarioException : UnderTestException
  {
    public string StepKeyword { get; set; }

    public string StepText { get; set; }

    public WorkInProgressScenarioException(string message)
      : base(message)
    { }

    public WorkInProgressScenarioException(string message, Exception inner)
      : base(message, inner)
    { }

    protected WorkInProgressScenarioException(SerializationInfo info, StreamingContext context)
      : base(info, context)
    {
      StepKeyword = info.GetString("StepKeyword");
      StepText = info.GetString("StepText");
    }

    [PublicAPI]
    [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
    public override void GetObjectData(SerializationInfo info, StreamingContext context)
    {
      if (info == null)
      {
        throw new ArgumentNullException(nameof(info));
      }

      info.AddValue("StepKeyword", StepKeyword);
      info.AddValue("StepText", StepText);

      base.GetObjectData(info, context);
    }
  }
}
