using System;
using JetBrains.Annotations;

namespace UnderTest.Exceptions
{
  [PublicAPI]
  [Serializable]
  public class MultipleExampleSourcesNotAllowedException : UnderTestException
  {
    public MultipleExampleSourcesNotAllowedException(string message)
      : base(message)
    { }

    public MultipleExampleSourcesNotAllowedException(string message, Exception inner)
      : base(message, inner)
    { }
  }
}
