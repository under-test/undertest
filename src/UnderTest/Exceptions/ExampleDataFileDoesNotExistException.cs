using System;
using System.Runtime.Serialization;
using System.Security.Permissions;
using JetBrains.Annotations;

namespace UnderTest.Exceptions
{
  [PublicAPI]
  [Serializable]
  public class ExampleDataFileDoesNotExistException : UnderTestException
  {
    public ExampleDataFileDoesNotExistException(string message)
      : base(message)
    { }

    public ExampleDataFileDoesNotExistException(string message, Exception inner)
      : base(message, inner)
    { }

    protected ExampleDataFileDoesNotExistException(SerializationInfo info, StreamingContext context)
      : base(info, context)
    {
      Filename = info.GetString("Filename");
    }

    public string Filename { get; set; }

    [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
    public override void GetObjectData(SerializationInfo info, StreamingContext context)
    {
      if (info == null)
      {
        throw new ArgumentNullException(nameof(info));
      }

      info.AddValue("Filename", Filename);

      base.GetObjectData(info, context);
    }
  }
}
