using System;
using System.Runtime.Serialization;
using System.Security.Permissions;
using JetBrains.Annotations;
using UnderTest.Infrastructure;

namespace UnderTest.Exceptions
{
  [PublicAPI]
  [Serializable]
  public class ScenarioExecutionTimeoutException : UnderTestException
  {
    public ScenarioExecutionTimeoutException(string message)
      : base(message)
    { }

    public ScenarioExecutionTimeoutException(string message, Exception inner)
      : base(message, inner)
    { }

    protected ScenarioExecutionTimeoutException(SerializationInfo info, StreamingContext context)
      : base(info, context)
    {
      ScenarioName = info.GetString(nameof(ScenarioName));
      ExampleTableRow = info.GetValue(nameof(ExampleTableRow), typeof(ExampleTableRow)) as ExampleTableRow;
    }

    public string ScenarioName { get; set; }
    public ExampleTableRow ExampleTableRow { get; set; }

    [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
    public override void GetObjectData(SerializationInfo info, StreamingContext context)
    {
      if (info == null)
      {
        throw new ArgumentNullException(nameof(info));
      }

      info.AddValue(nameof(ScenarioName), ScenarioName);
      info.AddValue(nameof(ExampleTableRow), ExampleTableRow);

      base.GetObjectData(info, context);
    }
  }
}
