using System;
using System.Runtime.Serialization;
using Gherkin;
using JetBrains.Annotations;

namespace UnderTest.Exceptions
{
  [Serializable]
  [PublicAPI]
  public class UnderTestFailedToParseFeatureFilesException : UnderTestException
  {
    public UnderTestFailedToParseFeatureFilesException(string message)
      : base(message)
    { }

    public UnderTestFailedToParseFeatureFilesException(string message, Exception inner)
      : base(message, inner)
    { }

    public UnderTestFailedToParseFeatureFilesException(SerializationInfo info, StreamingContext context)
      : base(info, context)
    { }

    public string Filename { get; set; }

    public CompositeParserException ParseException { get; set; }
  }
}
