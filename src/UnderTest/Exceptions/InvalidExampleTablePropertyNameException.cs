using System;
using System.Runtime.Serialization;
using System.Security.Permissions;
using JetBrains.Annotations;

namespace UnderTest.Exceptions
{
  [PublicAPI]
  [Serializable]
  public class InvalidExampleTablePropertyNameException : UnderTestException
  {
    public string HeaderName { get; set; }

    public InvalidExampleTablePropertyNameException(string message)
      : base(message)
    { }

    public InvalidExampleTablePropertyNameException(string message, Exception inner)
      : base(message, inner)
    { }

    protected InvalidExampleTablePropertyNameException(SerializationInfo info, StreamingContext context)
      : base(info, context)
    {
      HeaderName = info.GetString("HeaderName");
    }

    [PublicAPI]
    [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
    public override void GetObjectData(SerializationInfo info, StreamingContext context)
    {
      if (info == null)
      {
        throw new ArgumentNullException(nameof(info));
      }

      info.AddValue("HeaderName", HeaderName);

      base.GetObjectData(info, context);
    }
  }
}
