using System;
using System.Runtime.Serialization;
using JetBrains.Annotations;

namespace UnderTest.Exceptions
{
  [Serializable]
  [PublicAPI]
  public class UnderTestFilterFailureException : UnderTestException
  {
    public UnderTestFilterFailureException(string message)
      : base(message)
    { }

    public UnderTestFilterFailureException(string message, Exception inner)
      : base(message, inner)
    { }

    public UnderTestFilterFailureException(SerializationInfo info, StreamingContext context)
      : base(info, context)
    { }
  }
}
