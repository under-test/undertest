﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;
using JetBrains.Annotations;

namespace UnderTest.Exceptions
{
  [PublicAPI]
  [Serializable]
  public class ExpectedExceptionButPassedException : UnderTestException
  {
    public ExpectedExceptionButPassedException(string message)
      : base(message)
    { }

    public ExpectedExceptionButPassedException(string message, Exception inner)
      : base(message, inner)
    { }

    public string ScenarioName;

    protected ExpectedExceptionButPassedException(SerializationInfo info, StreamingContext context)
      : base(info, context)
    {
      ScenarioName = (string)info.GetValue("Errors", typeof(string));
    }

    [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
    public override void GetObjectData(SerializationInfo info, StreamingContext context)
    {
      if (info == null)
      {
        throw new ArgumentNullException(nameof(info));
      }

      info.AddValue("ScenarioName", ScenarioName);

      base.GetObjectData(info, context);
    }
  }
}
