namespace UnderTest
{
  public enum UnderTestExitCodes
  {
    Pass = 0,

    Inconclusive = -1,

    Failure = -2
  }
}
