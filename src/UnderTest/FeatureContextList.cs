using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

namespace UnderTest
{
  [PublicAPI]
  public class FeatureContextList : List<FeatureContext>
  {
    public FeatureContextList(IUnderTestSettings settings)
    {
      TestSettings = settings;
    }

    public FeatureContextList(IEnumerable<FeatureContext> features, IUnderTestSettings settings) : this(settings)
    {
      AddRange(features);
    }

    public IUnderTestSettings TestSettings { get; }

    internal FeaturesContainer BuildFeaturesContainer()
    {
      // priority here is:
      //   1. Wip/invalid removed first
      //   2. Then ignored are removed
      //   3. The remaining features are `Testable`
      var wip = new FeatureContextList(
        this
          .Where(x => x.Document?.Feature?.Tags.IncludesWipTag(TestSettings.ConfigSettings) ?? true)
          .ToList(), TestSettings);

      var notWip = this
        .Where(x => !x.Document?.Feature?.Tags.IncludesWipTag(TestSettings.ConfigSettings) ?? false)
        .ToList();

      var runnable = new FeatureContextList(notWip.Where(x =>
          !x.Document.Feature.Tags.IncludesIgnoreTag(TestSettings.ConfigSettings))
        .ToList(), TestSettings);

      var ignored = new FeatureContextList(
        notWip.Where(x => x.Document?.Feature?.Tags.IncludesInconclusiveTag(TestSettings.ConfigSettings) ?? false),
         TestSettings
        );

      return new FeaturesContainer
      {
        Wip = wip,
        Ignored = ignored,
        Testable = runnable
      };
    }
  }
}
