using System;
using JetBrains.Annotations;

namespace UnderTest.Reporting
{
  [PublicAPI]
  [Serializable]
  public class TestResultLogMessage
  {
    public string Type { get; set; }

    public string Message { get; set; }

    public string StackTrace { get; set; }
  }
}
