using System;
using JetBrains.Annotations;

namespace UnderTest.Reporting
{
  [PublicAPI]
  [Serializable]
  public class StepNotice
  {
    public string NoticeCode { get; set; }

    public string Message { get; set; }
  }
}
