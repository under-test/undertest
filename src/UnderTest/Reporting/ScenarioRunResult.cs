using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using Newtonsoft.Json;
using UnderTest.Reporting.Serialization;

namespace UnderTest.Reporting
{
  [PublicAPI]
  [Serializable]
  public class ScenarioRunResult
  {
    public string Description { get; set; }

    public string Keyword { get; set; }

    public string ScenarioName { get; set; }

    [JsonConverter(typeof(EnumObjectConverter<ScenarioRunResultType>))]
    public ScenarioRunResultType Result { get; set; }

    public IList<ScenarioStepResult> StepResults { get; set; } = new List<ScenarioStepResult>();

    public IList<StepNotice> Errors { get; set; } = new List<StepNotice>();
    public bool ExpectedFailure { get; set; }
  }
}
