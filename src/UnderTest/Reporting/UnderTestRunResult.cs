using System;
using System.Linq;
using System.Reflection;
using JetBrains.Annotations;
using Newtonsoft.Json;
using UnderTest.Infrastructure;
using UnderTest.Reporting.Serialization;

namespace UnderTest.Reporting
{
  [PublicAPI]
  [Serializable]
  public class UnderTestRunResult
  {
    public UnderTestRunResult(TestProjectDetails projectDetails)
    {
      if (projectDetails == null)
      {
        throw new ArgumentNullException(nameof(projectDetails));
      }

      UniqueRunId = Guid.NewGuid().ToString();
      ProjectName = projectDetails.ProjectName;
      ProjectVersion = projectDetails.ProjectVersion;
      UnderTestVersion = typeof(UnderTestRunner).Assembly.GetSummaryText();
      ExecutionTime = new TestRunExecutionTime();
    }

    public string UniqueRunId { get; set; }

    public string ProjectName { get; set; }

    public string ProjectVersion { get; set; }

    public TestRunFeatureResultList Features { get; set; } = new();

    public TestRunScenarioResultList Scenarios { get; set; } = new();

    public string UnderTestVersion { get; set; }

    [JsonConverter(typeof(EnumObjectConverter<TestRunOverallResult>))]
    public TestRunOverallResult OverallResult { get; set; } = TestRunOverallResult.Inconclusive;

    public TestRunExecutionTime ExecutionTime { get; set; }

    public void FinishRun()
    {
      ExecutionTime.EndTime = DateTime.UtcNow;
      UpdateFeatureResultCounts();
      UpdateScenarioResultCounts();
    }

    public bool HasAnyFailedScenarios() => FeaturesWhereAnyScenarioHaveThisResult(ScenarioRunResultType.Failed) > 0;

    // OK - lets discuss how we report feature results.
    // When the following is true, in this order, the count will be incremented by one.
    //   * Failed:       any step within a feature's scenarios have reported as failed.
    //   * Inconclusive: any step within a feature's scenarios have reported as inconclusive
    //   * Wip:          any step within a feature's scenarios have reported as work in progress(wip).
    //   * Passed:       all steps within a feature's scenarios have reported as passed.
    //   * Skipped:      all step within a feature's scenarios have reported as skipped.
    //
    // Please create an issue at https://gitlab.com/under-test/undertest/issues/new if you have feedback on this.
    private void UpdateFeatureResultCounts()
    {
      Features.Passed = FeaturesWhereAtLeastOneScenarioIsOfResultTypeAndAllOtherScenariosHaveSameResultOrSkipped(ScenarioRunResultType.Pass);
      Features.Failed = FeaturesWhereAnyScenarioHaveThisResult(ScenarioRunResultType.Failed);
      Features.ExpectedFailure = FeaturesWhereScenarioHaveFailureAndExpectedFailure();
      Features.Inconclusive = FeaturesWhereAnyScenarioHaveThisResult(ScenarioRunResultType.Inconclusive);
      Features.Skipped = FeaturesWhereAtLeastOneScenarioIsOfResultTypeAndAllOtherScenariosHaveSameResultOrSkipped(ScenarioRunResultType.Ignored);
      Features.Wip = FeaturesWhereAnyScenarioHaveThisResult(ScenarioRunResultType.Wip);

      if (Features.Failed > 0)
      {
        OverallResult = TestRunOverallResult.Failure;
      }
      else if (Features.Inconclusive > 0 && Features.Count > 0)
      {
        OverallResult = TestRunOverallResult.Inconclusive;
      }
      else
      {
        OverallResult = TestRunOverallResult.Pass;
      }
    }

    private int FeaturesWhereScenarioHaveFailureAndExpectedFailure() =>
      Features.Count(x => x.StrategyRuns
        .Any(y => y.ScenarioTestResults
          .Any(result => result.Result ==  ScenarioRunResultType.Failed && result.ExpectedFailure)));

    private void UpdateScenarioResultCounts()
    {
      Scenarios.Total = Features.Sum(x => x.StrategyRuns.Sum(y => y.ScenarioTestResults.Count));
      Scenarios.Passed = CountScenariosWithResultType(ScenarioRunResultType.Pass);
      Scenarios.Failed = CountScenariosWithResultType(ScenarioRunResultType.Failed);
      Scenarios.Inconclusive = CountScenariosWithResultType(ScenarioRunResultType.Inconclusive);
      Scenarios.Skipped = CountScenariosWithResultType(ScenarioRunResultType.Ignored);
      Scenarios.Wip = CountScenariosWithResultType(ScenarioRunResultType.Wip);
    }

    private int FeaturesWhereAtLeastOneScenarioIsOfResultTypeAndAllOtherScenariosHaveSameResultOrSkipped(ScenarioRunResultType resultType)
    {
      return Features.Count(x => x.StrategyRuns
                                  .All(y => y.ScenarioTestResults
                                             .All(result => result.Result == resultType || result.Result == ScenarioRunResultType.Ignored)
                                             &&
                                             y.ScenarioTestResults.Any(result => result.Result == resultType)
                                  ));
    }

    private int FeaturesWhereAnyScenarioHaveThisResult(ScenarioRunResultType resultType)
    {
      return Features.Count(x => x.StrategyRuns
        .Any(y => y.ScenarioTestResults
          .Any(result => result.Result ==  resultType)));
    }

    private int CountScenariosWithResultType(ScenarioRunResultType resultType)
    {
      return Features.Sum(x => x.StrategyRuns
                     .Sum(y => y.ScenarioTestResults
                     .Count(result => result.Result ==  resultType)));
    }
  }
}
