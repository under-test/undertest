using System;
using System.Collections.Generic;
using System.Reflection;
using Gherkin.Ast;
using JetBrains.Annotations;
using Newtonsoft.Json;
using UnderTest.Attributes;
using UnderTest.Infrastructure;
using UnderTest.Reporting.Serialization;

namespace UnderTest.Reporting
{
  [PublicAPI]
  [Serializable]
  public class ScenarioStepResult
  {
    public string Keyword { get; set; }

    public string Step { get; set; }

    public Location Location { get; set; }

    public string HandlerName { get; set; }

    public string HandlingMethod { get; set; }

    [JsonConverter(typeof(EnumObjectConverter<ScenarioStepResultType>))]
    public ScenarioStepResultType ScenarioStepResultType { get; set; }

    public TimeSpan Duration { get; set; } = TimeSpan.Zero;

    public IList<TestResultLogMessage> LogMessages { get; set; } = new List<TestResultLogMessage>();

    public IList<TestRunScreenshot> Screenshots { get; set; } = new List<TestRunScreenshot>();

    public IList<StepNotice> Warnings { get; set; } = new List<StepNotice>();

    public IList<StepNotice> Errors { get; set; } = new List<StepNotice>();

    public IDictionary<string, object> BehaviorResults { get; set; } = new Dictionary<string, object>();

    [CanBeNull]
    public DataTable DataTable { get; set; }

    [CanBeNull]
    public DocString DocString { get; set; }

    [CanBeNull]
    public List<object> StepParameters { get; set; }

    public ScenarioStepResult LoadStep(StepForProcessing step)
    {
      if (step == null)
      {
        throw new ArgumentNullException(nameof(step));
      }

      Keyword = step.Keyword;
      Step = step.Text;
      Location = step.Location;

      return this;
    }
  }
}
