using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

namespace UnderTest.Reporting
{
  [PublicAPI]
  [Serializable]
  public class TestRunFeatureResultList : List<TestRunFeatureResult>
  {
    public TestRunFeatureResult EnsureFeatureInTestResultAndReturn([NotNull]FeatureContext featureContext)
    {
      if (featureContext == null)
      {
        throw new ArgumentNullException(nameof(featureContext));
      }

      if (featureContext.Document == null)
      {
        throw new ArgumentException("The document within the FeatureContext must be not null.");
      }

      var result = this.FirstOrDefault(x => x.FeatureFileName == featureContext.Filename);
      if (result != null)
      {
        return result;
      }

      var featureResult = new TestRunFeatureResult
      {
        FeatureFileName = featureContext.Filename,
        Name = featureContext.Document.Feature.Name,
        FeatureDescription = featureContext.Document.Feature.Description
      };

      Add(featureResult);

      return featureResult;
    }

    public int Passed { get; set; }

    public int Failed { get; set; }

    public int Inconclusive { get; set; }

    public int Skipped { get; set; }

    public int Wip { get; set; }
    public int ExpectedFailure { get; set; }
  }
}
