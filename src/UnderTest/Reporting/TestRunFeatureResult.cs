using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace UnderTest.Reporting
{
  [PublicAPI]
  [Serializable]
  public class TestRunFeatureResult
  {
    public string Name { get; set; }

    public string FeatureDescription { get; set; }

    public string FeatureFileName { get; set; }

    public IList<TestRunFeatureStrategyResult> StrategyRuns { get; set; } = new List<TestRunFeatureStrategyResult>();
  }
}
