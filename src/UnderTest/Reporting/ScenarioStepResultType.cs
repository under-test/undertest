using JetBrains.Annotations;

namespace UnderTest.Reporting
{
  [PublicAPI]
  public enum ScenarioStepResultType
  {
    Inconclusive = 0,
    Failed = 1,
    Pass = 2,
    Skipped = 3,
    Wip = 4
  }
}
