using JetBrains.Annotations;

namespace UnderTest.Reporting
{
  [PublicAPI]
  public enum TestRunOverallResult
  {
    Inconclusive = 0,

    Pass = 1,

    Failure = 2
  }
}
