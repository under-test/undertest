using System.Collections.Generic;
using JetBrains.Annotations;
using UnderTest.Infrastructure;
using UnderTest.Strategies;

namespace UnderTest
{
  [PublicAPI]
  public interface IFeatureHandler : ITestStrategyEventHandlers, IHasTestSettings, IHasLogger
  {
    int FeatureExecutionOrder { get; }
  }
}
