﻿using System;
using FluentAssertions;
using NUnit.Framework;

namespace UnderTest.Support.UnitTests
{
  public class EnumAliasAttributeTests
  {
    [Test]
    public void Constructor_PassedNull_ThrowsArgumentNullException()
    {
      Action act = () => new EnumAliasAttribute(null);

      act.Should().Throw<ArgumentNullException>();
    }

    [Test]
    public void Constructor_PassedAlias_AliasSet()
    {
      var expected = "shoes";

      var instance = new EnumAliasAttribute(expected);

      instance.Alias.Should().Be(expected);
    }
  }
}
